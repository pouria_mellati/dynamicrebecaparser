parser grammar DynamicRebecaParser;

import CoreRebecaParser;

/* Added message sending statement, foreach statement for collections. */
statement returns [Statement s]
	:
		recipient = expression BANG {MsgSendStatement mss = new MsgSendStatement(); $s = mss; mss.setLineNumber($BANG.getLine()); mss.setCharacter($BANG.getCharPositionInLine());}
		msgName = IDENTIFIER (LPAREN (params = expressionList {mss.getParameters().addAll($params.el);})? RPAREN)? SEMI
		{mss.setRecipient($recipient.e); mss.setMessageName($msgName.text);}
	|	fd = fieldDeclaration {$s = $fd.fd;} SEMI
    |	b = block {$s = $b.bs;}
    |   IF {$s = new ConditionalStatement(); $s.setLineNumber($IF.getLine());$s.setCharacter($IF.getCharPositionInLine());}
    	LPAREN e = expression RPAREN st = statement
    	{((ConditionalStatement)$s).setCondition($e.e); ((ConditionalStatement)$s).setStatement($st.s);}
		(ELSE est = statement {((ConditionalStatement)$s).setElseStatement($est.s);})?
    |   WHILE {$s = new WhileStatement(); $s.setLineNumber($WHILE.getLine());$s.setCharacter($WHILE.getCharPositionInLine());} 
    	LPAREN e = expression RPAREN st = statement {((WhileStatement)$s).setCondition($e.e); ((WhileStatement)$s).setStatement($st.s);}
    |   FOR {$s = new ForStatement(); $s.setLineNumber($FOR.getLine());$s.setCharacter($FOR.getCharPositionInLine());} 
    	LPAREN (fi = forInit {((ForStatement)$s).setForInitializer($fi.fi);})? SEMI 
    	(e = expression {((ForStatement)$s).setCondition($e.e);})? SEMI 
    	(el = expressionList {((ForStatement)$s).getForIncrement().addAll($el.el); })? 
    	RPAREN st = statement {((ForStatement)$s).setStatement($st.s);}
    |	FOR {$s = new ForeachStatement(); $s.setLineNumber($FOR.getLine()); $s.setCharacter($FOR.getCharPositionInLine());}
    	LPAREN t = type varName = IDENTIFIER COLON range = expression RPAREN st = statement
    	{ForeachStatement fe = (ForeachStatement)$s;
    	fe.setVarType($t.t); fe.setVarName($varName.text); fe.setRange($range.e); fe.setStatement($st.s);}
    |   SWITCH LPAREN e = expression RPAREN LBRACE sb = switchBlock RBRACE
    	{$s = $sb.ss; ((SwitchStatement)$s).setExpression($e.e); $s.setLineNumber($SWITCH.getLine()); $s.setCharacter($SWITCH.getCharPositionInLine());}
    |   RETURN e = expression? SEMI
    	{$s = new ReturnStatement(); ((ReturnStatement)$s).setExpression($e.e); $s.setLineNumber($RETURN.getLine());$s.setCharacter($RETURN.getCharPositionInLine());}
    |   BREAK SEMI
    	{$s = new BreakStatement(); $s.setLineNumber($BREAK.getLine());$s.setCharacter($BREAK.getCharPositionInLine());}
    |   CONTINUE SEMI
    	{$s = new BreakStatement(); $s.setLineNumber($CONTINUE.getLine());$s.setCharacter($CONTINUE.getCharPositionInLine());}
    |   SEMI {$s = new Statement();}
    |   se = statementExpression {$s = $se.se;} SEMI
	;

/* Add generics to the defaults. */
type returns [Type t]
	:
		{$t = new PrimitiveType();}
		typeName = IDENTIFIER
		{$t = new OrdinaryPrimitiveType(); ((OrdinaryPrimitiveType)$t).setName($typeName.text);
	 	 $t.setLineNumber($typeName.getLine());$t.setCharacter($typeName.getCharPositionInLine());}
		(ds = dimensions {PrimitiveType newpt = (PrimitiveType)$t; $t = new ArrayType(); ((ArrayType)$t).setPrimitiveType(newpt); ((ArrayType)$t).getDimensions().addAll($ds.ds);})?
	|
		{$t = new ParametrizedType();}
		pt = parametrizedType {$t = $pt.t;}
	;

parametrizedType returns [ParametrizedType t]
	:
		{$t = new ParametrizedType();}
		parametrizedTypeName = LIST typeParams = typeParametersDecl
		{ParametrizedType pt = (ParametrizedType)$t; pt.setName($parametrizedTypeName.text); pt.getParameters().addAll($typeParams.params);}
	;

typeParametersDecl returns [List<Type> params]
	:
		{$params = new LinkedList<Type>();}
		LT (firstTypeParam = type {$params.add($firstTypeParam.t);})? (COMMA typeParam = type {$params.add($typeParam.t);})* GT
	;

/* Add list literals to the defaults. */
literal returns [Literal l]
    :   INTLITERAL {$l = new Literal();$l.setLiteralValue($INTLITERAL.text);
    		$l.setType(TypesUtilities.INT_TYPE);
    		$l.setLineNumber($INTLITERAL.getLine());$l.setCharacter($INTLITERAL.getCharPositionInLine());}
    |   FLOATLITERAL 
    	{$l = new Literal();$l.setLiteralValue($FLOATLITERAL.text);
    	$l.setType(TypesUtilities.FLOAT_TYPE);
    	$l.setLineNumber($FLOATLITERAL.getLine());$l.setCharacter($FLOATLITERAL.getCharPositionInLine());}
    |   DOUBLELITERAL 
    	{$l = new Literal();$l.setLiteralValue($DOUBLELITERAL.text);
    	$l.setType(TypesUtilities.DOUBLE_TYPE);
    	$l.setLineNumber($DOUBLELITERAL.getLine());$l.setCharacter($DOUBLELITERAL.getCharPositionInLine());}
    |   CHARLITERAL 
    	{$l = new Literal();$l.setLiteralValue($CHARLITERAL.text);
    	$l.setType(TypesUtilities.CHAR_TYPE);
    	$l.setLineNumber($CHARLITERAL.getLine());$l.setCharacter($CHARLITERAL.getCharPositionInLine());}
    |   STRINGLITERAL 
    	{$l = new Literal();$l.setLiteralValue($STRINGLITERAL.text);
    	$l.setType(TypesUtilities.STRING_TYPE);
    	$l.setLineNumber($STRINGLITERAL.getLine());$l.setCharacter($STRINGLITERAL.getCharPositionInLine());}
    |   TRUE 
    	{$l = new Literal();$l.setLiteralValue("true");
    	$l.setType(TypesUtilities.BOOLEAN_TYPE);
    	$l.setLineNumber($TRUE.getLine());$l.setCharacter($TRUE.getCharPositionInLine());}
    |   FALSE 
    	{$l = new Literal();$l.setLiteralValue("false");
    	$l.setType(TypesUtilities.BOOLEAN_TYPE);
    	$l.setLineNumber($FALSE.getLine());$l.setCharacter($FALSE.getCharPositionInLine());}
    |   NULL 
    	{$l = new Literal();$l.setLiteralValue("null");
    	$l.setType(TypesUtilities.NULL_TYPE);
    	$l.setLineNumber($NULL.getLine());$l.setCharacter($NULL.getCharPositionInLine());}
    |	
    	{CollectionLiteral cl = new CollectionLiteral(); $l = cl;}
    	collType = parametrizedType {cl.setType($collType.t);}
    	LPAREN (elems = expressionList {cl.getElemExprs().addAll($elems.el);})? RPAREN
    ;

/* Add NEWing rule to defaults. */
unaryExpressionNotPlusMinus returns [Expression e]
    :	NEW t = type {NewExpression ne = new NewExpression(); $e = ne; ne.setType($t.t); $e.setLineNumber($NEW.getLine()); $e.setCharacter($NEW.getCharPositionInLine());}
    	LPAREN (params = expressionList {ne.getParamExprs().addAll($params.el);})? RPAREN   
    |	TILDA e1 = unaryExpression {$e = new UnaryExpression(); ((UnaryExpression)$e).setOperator($TILDA.text); ((UnaryExpression)$e).setExpression($e1.e); $e.setLineNumber($e1.e.getLineNumber()); $e.setCharacter($e1.e.getCharacter());}
    |	BANG e1 = unaryExpression {$e = new UnaryExpression(); ((UnaryExpression)$e).setOperator($BANG.text); ((UnaryExpression)$e).setExpression($e1.e); $e.setLineNumber($e1.e.getLineNumber()); $e.setCharacter($e1.e.getCharacter());}
    |   (
	    	ec = castExpression {$e = $ec.e;}
    	|	LPAREN ep = expression {$e = $ep.e;} RPAREN
	    |   p = primary  {$e = $p.tp;}
	    |   l = literal {$e = $l.l;}
	    |	QUES LPAREN el = expressionList RPAREN 
	    	{$e = new NonDetExpression(); ((NonDetExpression)$e).getChoices().addAll($el.el);
	    	$e.setLineNumber($QUES.getLine()); $e.setCharacter($QUES.getCharPositionInLine());}
	    )
        (DOT p2 = primary
        	{
        	DotPrimary de = new DotPrimary(); de.setLineNumber($DOT.getLine()); de.setCharacter($DOT.getCharPositionInLine());
        	if ($e instanceof DotPrimary) {
        		DotPrimary temp = (DotPrimary)$e;
        		while(temp.getRight() instanceof DotPrimary)
        			temp = (DotPrimary)temp.getRight();
        		de.setLeft(temp.getRight());
        		temp.setRight(de);
        		de.setRight($p2.tp);
        	} else {
        		de.setLeft($e); de.setRight($p2.tp);
	        	$e = de;
        	}
        	}
        )*
        (   PLUSPLUS {PlusSubExpression pse = new PlusSubExpression(); pse.setValue($e); pse.setOperator("++");$e=pse;
        		pse.setLineNumber($PLUSPLUS.getLine()); pse.setCharacter($PLUSPLUS.getCharPositionInLine());}
        |   SUBSUB {PlusSubExpression pse = new PlusSubExpression(); pse.setValue($e); pse.setOperator("--");$e=pse;
        		pse.setLineNumber($SUBSUB.getLine()); pse.setCharacter($SUBSUB.getCharPositionInLine());}
        )?

    ;

mainDeclaration returns [MainDeclaration md]
	: 	
		{$md = new MainBlock();}
		MAIN {$md.setLineNumber($MAIN.getLine());$md.setCharacter($MAIN.getCharPositionInLine());}
		b = block {((MainBlock)$md).setBlock($b.bs);}
	;
