parser grammar ProbabilisticTimedRebecaParser;

import CoreRebecaParser;

statement returns [Statement s]
	:
		fd = fieldDeclaration {$s = $fd.fd;} SEMI
    |	b = block {$s = $b.bs;}
    |   IF {$s = new ConditionalStatement(); $s.setLineNumber($IF.getLine());$s.setCharacter($IF.getCharPositionInLine());}
    	LPAREN e = expression RPAREN st = statement
    	{((ConditionalStatement)$s).setCondition($e.e); ((ConditionalStatement)$s).setStatement($st.s);}
		(ELSE est = statement {((ConditionalStatement)$s).setElseStatement($est.s);})?
    |   WHILE {$s = new WhileStatement(); $s.setLineNumber($WHILE.getLine());$s.setCharacter($WHILE.getCharPositionInLine());} 
    	LPAREN e = expression RPAREN st = statement {((WhileStatement)$s).setCondition($e.e); ((WhileStatement)$s).setStatement($st.s);}
    |   FOR {$s = new ForStatement(); $s.setLineNumber($FOR.getLine());$s.setCharacter($FOR.getCharPositionInLine());} 
    	LPAREN (fi = forInit {((ForStatement)$s).setForInitializer($fi.fi);})? SEMI 
    	(e = expression {((ForStatement)$s).setCondition($e.e);})? SEMI 
    	(el = expressionList {((ForStatement)$s).getForIncrement().addAll($el.el); })? 
    	RPAREN st = statement {((ForStatement)$s).setStatement($st.s);}
    |   SWITCH LPAREN e = expression RPAREN LBRACE sb = switchBlock RBRACE
    	{$s = $sb.ss; ((SwitchStatement)$s).setExpression($e.e); $s.setLineNumber($SWITCH.getLine()); $s.setCharacter($SWITCH.getCharPositionInLine());}
    |   RETURN e = expression? SEMI
    	{$s = new ReturnStatement(); ((ReturnStatement)$s).setExpression($e.e); $s.setLineNumber($RETURN.getLine());$s.setCharacter($RETURN.getCharPositionInLine());}
    |   BREAK SEMI
    	{$s = new BreakStatement(); $s.setLineNumber($BREAK.getLine());$s.setCharacter($BREAK.getCharPositionInLine());}
    |   CONTINUE SEMI
    	{$s = new BreakStatement(); $s.setLineNumber($CONTINUE.getLine());$s.setCharacter($CONTINUE.getCharPositionInLine());}
    |   SEMI {$s = new Statement();}
    |   se = statementExpression {$s = $se.se;} SEMI
    |   PALT {$s = new PAltStatement(); $s.setLineNumber($PALT.getLine());$s.setCharacter($PALT.getCharPositionInLine());}
		LBRACE
    		 (pasg = pAltStatementGroup {((PAltStatement)$s).getPAltStatementGroups().add($pasg.pasg);} )+
    	RBRACE
	;

pAltStatementGroup returns [PAltStatementGroup pasg]
	:
		PROB LPAREN e = expression RPAREN COLON LBRACE
		{$pasg = new PAltStatementGroup(); $pasg.setExpression($e.e);}  
    		(st = statement {$pasg.getStatements().add($st.s);})*
    	RBRACE
	;

