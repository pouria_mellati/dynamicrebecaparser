parser grammar TimedRebecaParser;

import CoreRebecaParser;

primary returns [TermPrimary tp]
    :   
    id1 = IDENTIFIER {$tp = new TermPrimary(); $tp.setName($id1.text);
					  $tp.setLineNumber($id1.getLine()); $tp.setCharacter($id1.getCharPositionInLine());}
    (	lp = LPAREN 
    	{ParentSuffixPrimary psp = new ParentSuffixPrimary(); 
    	 psp.setLineNumber($lp.getLine()); psp.setCharacter($lp.getCharPositionInLine());
    	 $tp.setParentSuffixPrimary(psp);}
		(el = expressionList {$tp.getParentSuffixPrimary().getArguments().addAll($el.el);})?
		RPAREN
    	(AFTER LPAREN ef = expression RPAREN {$tp.getParentSuffixPrimary().setAfterExpression($ef.e);})?
    	(DEADLINE LPAREN ed = expression RPAREN {$tp.getParentSuffixPrimary().setDeadlineExpression($ed.e);})?
    )?
	(LBRACKET e2 = expression RBRACKET {$tp.getIndices().add($e2.e);})*
    ;
