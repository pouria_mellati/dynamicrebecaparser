grammar DynamicRebecaComplete;

import DynamicRebecaLexer, DynamicRebecaParser;

@lexer::header {
	package org.rebecalang.compiler.modelcompiler.dynamicrebeca.compiler;
}
    
@parser::header {
	package org.rebecalang.compiler.modelcompiler.dynamicrebeca.compiler;
	import org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel.*;
	import org.rebecalang.compiler.modelcompiler.dynamicrebeca.extratypes.*;
	import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.*;
	
	import java.util.*;
	import org.antlr.runtime.BitSet;
	import org.rebecalang.compiler.utils.TypesUtilities;
}
