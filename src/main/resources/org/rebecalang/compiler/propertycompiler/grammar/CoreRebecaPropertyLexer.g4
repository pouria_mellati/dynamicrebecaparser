lexer grammar CoreRebecaPropertyLexer;

import CoreRebecaExpressionLexer;

DEFINE
	:	'define'
	;
LTL
	:	'LTL'
	;

CTL
	:	'CTL'
	;

THEN
	:	'->'
	;

PROPERTY
	:	'property'
	;
	