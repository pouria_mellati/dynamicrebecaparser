// Generated from src/main/resources/org/rebecalang/compiler/propertycompiler/grammar/CoreRebecaPropertyComplete.g4 by ANTLR 4.0

	package org.rebecalang.compiler.propertycompiler.corerebeca.compiler;
	import org.rebecalang.compiler.propertycompiler.corerebeca.objectmodel.*;
	import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.*;
	import java.util.*;
	import org.antlr.runtime.BitSet;
	import org.rebecalang.compiler.utils.TypesUtilities;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;

public class CoreRebecaPropertyCompleteBaseListener implements CoreRebecaPropertyCompleteListener {
	@Override public void enterAssignmentOperator(CoreRebecaPropertyCompleteParser.AssignmentOperatorContext ctx) { }
	@Override public void exitAssignmentOperator(CoreRebecaPropertyCompleteParser.AssignmentOperatorContext ctx) { }

	@Override public void enterExpression(CoreRebecaPropertyCompleteParser.ExpressionContext ctx) { }
	@Override public void exitExpression(CoreRebecaPropertyCompleteParser.ExpressionContext ctx) { }

	@Override public void enterRelationalOp(CoreRebecaPropertyCompleteParser.RelationalOpContext ctx) { }
	@Override public void exitRelationalOp(CoreRebecaPropertyCompleteParser.RelationalOpContext ctx) { }

	@Override public void enterShiftOp(CoreRebecaPropertyCompleteParser.ShiftOpContext ctx) { }
	@Override public void exitShiftOp(CoreRebecaPropertyCompleteParser.ShiftOpContext ctx) { }

	@Override public void enterUnaryExpression(CoreRebecaPropertyCompleteParser.UnaryExpressionContext ctx) { }
	@Override public void exitUnaryExpression(CoreRebecaPropertyCompleteParser.UnaryExpressionContext ctx) { }

	@Override public void enterExpressionList(CoreRebecaPropertyCompleteParser.ExpressionListContext ctx) { }
	@Override public void exitExpressionList(CoreRebecaPropertyCompleteParser.ExpressionListContext ctx) { }

	@Override public void enterUnaryExpressionNotPlusMinus(CoreRebecaPropertyCompleteParser.UnaryExpressionNotPlusMinusContext ctx) { }
	@Override public void exitUnaryExpressionNotPlusMinus(CoreRebecaPropertyCompleteParser.UnaryExpressionNotPlusMinusContext ctx) { }

	@Override public void enterType(CoreRebecaPropertyCompleteParser.TypeContext ctx) { }
	@Override public void exitType(CoreRebecaPropertyCompleteParser.TypeContext ctx) { }

	@Override public void enterConditionalExpression(CoreRebecaPropertyCompleteParser.ConditionalExpressionContext ctx) { }
	@Override public void exitConditionalExpression(CoreRebecaPropertyCompleteParser.ConditionalExpressionContext ctx) { }

	@Override public void enterAndExpression(CoreRebecaPropertyCompleteParser.AndExpressionContext ctx) { }
	@Override public void exitAndExpression(CoreRebecaPropertyCompleteParser.AndExpressionContext ctx) { }

	@Override public void enterRelationalExpression(CoreRebecaPropertyCompleteParser.RelationalExpressionContext ctx) { }
	@Override public void exitRelationalExpression(CoreRebecaPropertyCompleteParser.RelationalExpressionContext ctx) { }

	@Override public void enterPrimary(CoreRebecaPropertyCompleteParser.PrimaryContext ctx) { }
	@Override public void exitPrimary(CoreRebecaPropertyCompleteParser.PrimaryContext ctx) { }

	@Override public void enterDimensions(CoreRebecaPropertyCompleteParser.DimensionsContext ctx) { }
	@Override public void exitDimensions(CoreRebecaPropertyCompleteParser.DimensionsContext ctx) { }

	@Override public void enterConditionalOrExpression(CoreRebecaPropertyCompleteParser.ConditionalOrExpressionContext ctx) { }
	@Override public void exitConditionalOrExpression(CoreRebecaPropertyCompleteParser.ConditionalOrExpressionContext ctx) { }

	@Override public void enterShiftExpression(CoreRebecaPropertyCompleteParser.ShiftExpressionContext ctx) { }
	@Override public void exitShiftExpression(CoreRebecaPropertyCompleteParser.ShiftExpressionContext ctx) { }

	@Override public void enterConditionalAndExpression(CoreRebecaPropertyCompleteParser.ConditionalAndExpressionContext ctx) { }
	@Override public void exitConditionalAndExpression(CoreRebecaPropertyCompleteParser.ConditionalAndExpressionContext ctx) { }

	@Override public void enterAdditiveExpression(CoreRebecaPropertyCompleteParser.AdditiveExpressionContext ctx) { }
	@Override public void exitAdditiveExpression(CoreRebecaPropertyCompleteParser.AdditiveExpressionContext ctx) { }

	@Override public void enterExclusiveOrExpression(CoreRebecaPropertyCompleteParser.ExclusiveOrExpressionContext ctx) { }
	@Override public void exitExclusiveOrExpression(CoreRebecaPropertyCompleteParser.ExclusiveOrExpressionContext ctx) { }

	@Override public void enterInclusiveOrExpression(CoreRebecaPropertyCompleteParser.InclusiveOrExpressionContext ctx) { }
	@Override public void exitInclusiveOrExpression(CoreRebecaPropertyCompleteParser.InclusiveOrExpressionContext ctx) { }

	@Override public void enterInstanceOfExpression(CoreRebecaPropertyCompleteParser.InstanceOfExpressionContext ctx) { }
	@Override public void exitInstanceOfExpression(CoreRebecaPropertyCompleteParser.InstanceOfExpressionContext ctx) { }

	@Override public void enterEqualityExpression(CoreRebecaPropertyCompleteParser.EqualityExpressionContext ctx) { }
	@Override public void exitEqualityExpression(CoreRebecaPropertyCompleteParser.EqualityExpressionContext ctx) { }

	@Override public void enterCastExpression(CoreRebecaPropertyCompleteParser.CastExpressionContext ctx) { }
	@Override public void exitCastExpression(CoreRebecaPropertyCompleteParser.CastExpressionContext ctx) { }

	@Override public void enterPropertyModel(CoreRebecaPropertyCompleteParser.PropertyModelContext ctx) { }
	@Override public void exitPropertyModel(CoreRebecaPropertyCompleteParser.PropertyModelContext ctx) { }

	@Override public void enterMultiplicativeExpression(CoreRebecaPropertyCompleteParser.MultiplicativeExpressionContext ctx) { }
	@Override public void exitMultiplicativeExpression(CoreRebecaPropertyCompleteParser.MultiplicativeExpressionContext ctx) { }

	@Override public void enterLiteral(CoreRebecaPropertyCompleteParser.LiteralContext ctx) { }
	@Override public void exitLiteral(CoreRebecaPropertyCompleteParser.LiteralContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	@Override public void visitTerminal(TerminalNode node) { }
	@Override public void visitErrorNode(ErrorNode node) { }
}