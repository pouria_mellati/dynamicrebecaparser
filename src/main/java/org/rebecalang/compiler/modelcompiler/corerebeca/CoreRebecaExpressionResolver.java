package org.rebecalang.compiler.modelcompiler.corerebeca;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.rebecalang.compiler.modelcompiler.ScopeHandler;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.ArrayType;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.BinaryExpression;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.CastExpression;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.DotPrimary;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.Expression;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.Literal;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.NonDetExpression;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.PlusSubExpression;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.PrimaryExpression;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.PrimitiveType;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.TermPrimary;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.TernaryExpression;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.Type;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.UnaryExpression;
import org.rebecalang.compiler.utils.CodeCompilationException;
import org.rebecalang.compiler.utils.CompilerFeature;
import org.rebecalang.compiler.utils.CompilerInternalErrorRuntimeException;
import org.rebecalang.compiler.utils.ExceptionContainer;
import org.rebecalang.compiler.utils.ExpressionResolver;
import org.rebecalang.compiler.utils.Pair;
import org.rebecalang.compiler.utils.TypesUtilities;

public class CoreRebecaExpressionResolver extends ExpressionResolver {

	private Type containerType;

	List<NonDetExpression> nondetsInExpression = new LinkedList<NonDetExpression>();

	public void setContainerType(Type containerType) {
		this.containerType = containerType;
	}

	public Pair<Type, Object> evaluate(Expression expression,
			ScopeHandler scopeHandler, Set<CompilerFeature> compilerFeatures,
			ExceptionContainer container) {
		nondetsInExpression.clear();
		Pair<Type, Object> returnValue = subEvaluator(expression, scopeHandler,
				compilerFeatures, container);
		return returnValue;
	}

	public Pair<Type, Object> subEvaluator(Expression expression,
			ScopeHandler scopeHandler, Set<CompilerFeature> compilerFeatures,
			ExceptionContainer container) {
		Pair<Type, Object> returnValue = new Pair<Type, Object>();

		if (expression instanceof TernaryExpression) {
			TernaryExpression tExpression = (TernaryExpression) expression;
			Pair<Type, Object> lType = subEvaluator(tExpression.getLeft(),
					scopeHandler, compilerFeatures, container);
			Pair<Type, Object> rType = subEvaluator(tExpression.getRight(),
					scopeHandler, compilerFeatures, container);
			Pair<Type, Object> cType = subEvaluator(tExpression.getCondition(),
					scopeHandler, compilerFeatures, container);
			// The ternary operator condition should be boolean expression
			if (!TypesUtilities.getInstance().canTypeUpCastTo(cType.getFirst(),
					TypesUtilities.BOOLEAN_TYPE)) {
				CodeCompilationException cce = TypesUtilities
						.getTypeMismatchException(cType.getFirst(),
								TypesUtilities.BOOLEAN_TYPE);
				cce.setLine(tExpression.getCondition().getLineNumber());
				cce.setColumn(tExpression.getCondition().getCharacter());
				container.addException(cce);
			}
			// The type of left expression of ternary operator should be the
			// same as the type of the right expression
			if (!TypesUtilities.getInstance().canTypeUpCastTo(lType.getFirst(),
					rType.getFirst())) {
				CodeCompilationException cce = TypesUtilities
						.getTypeMismatchException(rType.getFirst(),
								lType.getFirst());
				cce.setLine(tExpression.getLeft().getLineNumber());
				cce.setColumn(tExpression.getLeft().getCharacter());
				container.addException(cce);
			}
			tExpression.setType(lType.getFirst());
			returnValue.setFirst(tExpression.getType());
			if (cType.getSecond() != null) {
				if (lType.getSecond() != null && rType.getSecond() != null)
					if (((Boolean) cType.getSecond()).booleanValue()) {
						returnValue.setSecond(lType.getSecond());
					} else {
						returnValue.setSecond(rType.getSecond());
					}
			}
		} else if (expression instanceof BinaryExpression) {
			BinaryExpression bExpression = (BinaryExpression) expression;
			Pair<Type, Object> lType = subEvaluator(bExpression.getLeft(),
					scopeHandler, compilerFeatures, container);
			Pair<Type, Object> rType = subEvaluator(bExpression.getRight(),
					scopeHandler, compilerFeatures, container);
			try {
				bExpression.setType(getResultType(bExpression.getOperator(),
						lType.getFirst(), rType.getFirst()));
				returnValue.setFirst(bExpression.getType());
				returnValue.setSecond(evaluateConstantTerm(
						bExpression.getOperator(), bExpression.getType(),
						lType.getSecond(), rType.getSecond()));
				Set<String> assignmentOperators = new HashSet<String>();
				assignmentOperators.addAll(Arrays.asList("=", "+=", "-=", "*=",
						"/=", "%=", "~=", "^=", "&=", "|=", "<<=", ">>="));
				if (assignmentOperators.contains(bExpression.getOperator())
						&& !isInLValueStyle(bExpression.getLeft())) {
					container
							.getExceptions()
							.add(new CodeCompilationException(
									"The left-hand side of an assignment must be a variable",
									bExpression.getLineNumber(), bExpression
											.getCharacter()));
				}
			} catch (CodeCompilationException cce) {
				// Two types of two sides of binary operation are incompatible
				CodeCompilationException cce2 = createEvaluateExceptionMessage2(
						bExpression.getLineNumber(),
						bExpression.getCharacter(), bExpression.getOperator(),
						lType.getFirst(), rType.getFirst());
				if (cce2 != null)
					container.getExceptions().add(cce2);
				bExpression.setType(lType.getFirst());
				returnValue.setFirst(bExpression.getType());
			}
		} else if (expression instanceof UnaryExpression) {
			UnaryExpression uExpression = (UnaryExpression) expression;
			Pair<Type, Object> type = subEvaluator(uExpression.getExpression(),
					scopeHandler, compilerFeatures, container);
			String operator = uExpression.getOperator();
			if (operator.equals("++") || operator.equals("--")) {
				if (!TypesUtilities.getInstance().canTypeUpCastTo(
						type.getFirst(), TypesUtilities.INT_TYPE)) {
					CodeCompilationException cce = TypesUtilities
							.getTypeMismatchException(type.getFirst(),
									TypesUtilities.INT_TYPE);
					cce.setColumn(uExpression.getExpression().getCharacter());
					cce.setLine(uExpression.getExpression().getLineNumber());
					container.addException(cce);
				}
				if (!isInLValueStyle(uExpression.getExpression())) {
					container.getExceptions().add(
							new CodeCompilationException(
									"Invalid argument to operation ++/--",
									uExpression.getLineNumber(), uExpression
											.getCharacter()));
				}
			} else if (operator.equals("-")) {
				if (!TypesUtilities.getInstance().canTypeUpCastTo(
						type.getFirst(), TypesUtilities.DOUBLE_TYPE)) {
					CodeCompilationException cce = TypesUtilities
							.getTypeMismatchException(type.getFirst(),
									TypesUtilities.INT_TYPE);
					cce.setColumn(uExpression.getExpression().getCharacter());
					cce.setLine(uExpression.getExpression().getLineNumber());
					container.addException(cce);
				} else {
					if (type.getSecond() != null)
						returnValue.setSecond(evaluateConstantTerm("-",
								type.getFirst(), 0, type.getSecond()));
				}
			} else if (operator.equals("!")) {
				if (!TypesUtilities.getInstance().canTypeUpCastTo(
						type.getFirst(), TypesUtilities.BOOLEAN_TYPE)) {
					CodeCompilationException cce = TypesUtilities
							.getTypeMismatchException(type.getFirst(),
									TypesUtilities.BOOLEAN_TYPE);
					cce.setColumn(uExpression.getExpression().getCharacter());
					cce.setLine(uExpression.getExpression().getLineNumber());
					container.addException(cce);
				} else {
					if (type.getSecond() != null)
						returnValue.setSecond(evaluateConstantTerm("!",
								type.getFirst(), type.getSecond(), null));
				}
			}
			uExpression.setType(type.getFirst());
			returnValue.setFirst(uExpression.getType());
		} else if (expression instanceof CastExpression) {
			CastExpression cExpression = (CastExpression) expression;
			Pair<Type, Object> expressionType = subEvaluator(
					cExpression.getExpression(), scopeHandler,
					compilerFeatures, container);
			try {
				Type castType = TypesUtilities.getInstance().getType(
						cExpression.getType());
				if (!TypesUtilities.getInstance().canTypeCastTo(
						expressionType.getFirst(), castType)) {
					CodeCompilationException cce = TypesUtilities
							.getTypeMismatchException(
									expressionType.getFirst(), castType);
					cce.setColumn(cExpression.getExpression().getCharacter());
					cce.setLine(cExpression.getExpression().getLineNumber());
					container.addException(cce);
				}
				cExpression.setType(castType);
				returnValue.setFirst(cExpression.getType());
				returnValue.setSecond(evaluateConstantTerm(
						"("
								+ TypesUtilities.getTypeName(returnValue
										.getFirst()) + ")",
						returnValue.getFirst(), expressionType.getSecond(),
						null));
			} catch (CodeCompilationException cce) {
				cExpression.setType(TypesUtilities.UNKNOWN_TYPE);
				cce.setColumn(cExpression.getCharacter());
				cce.setLine(cExpression.getLineNumber());
				container.addException(cce);
			}
		} else if (expression instanceof NonDetExpression) {
			NonDetExpression ndExpression = (NonDetExpression) expression;
			// The type of the first element of the non-det expression is set as
			// the first candidate for the result type
			Type type = null;
//			subEvaluator(ndExpression.getChoices().get(0),
//					scopeHandler, compilerFeatures, container).getFirst();
			for (Expression ndTermExpression : ndExpression.getChoices()) {
				Pair<Type, Object> ndTerm = subEvaluator(ndTermExpression,
						scopeHandler, compilerFeatures, container);
				try {
					// The type is set to the biggest type in comparison to the
					// new choices
					type = (type == null ? ndTerm.getFirst() : TypesUtilities
							.getInstance().getTheBiggerType(type,
									ndTerm.getFirst()));
					if (ndTerm.getSecond() == null) {
						CodeCompilationException cce = new CodeCompilationException(
								"Non-deterministic terms must be constant expressions",
								0, 0);
						throw cce;
					}
				} catch (CodeCompilationException cce) {
					cce.setColumn(ndTermExpression.getCharacter());
					cce.setLine(ndTermExpression.getLineNumber());
					container.addException(cce);
				}
			}
			ndExpression.setType(type);
			returnValue.setFirst(ndExpression.getType());
			if (ndExpression.getChoices().size() < 2) {
				CodeCompilationException cce = new CodeCompilationException(
						"Non-deterministic expression must have more than two choices",
						ndExpression.getLineNumber(), ndExpression
								.getCharacter());
				container.addException(cce);
			}
			if (containerType != TypesUtilities.MSGSRV_TYPE) {
				CodeCompilationException cce = new CodeCompilationException(
						"Only message servers are allowed to have non-deterministic expression",
						ndExpression.getLineNumber(), ndExpression
								.getCharacter());
				container.addException(cce);
			}
		} else if (expression instanceof Literal) {
			Literal lExpression = (Literal) expression;
			if (lExpression.getType() == TypesUtilities.INT_TYPE) {
				try {
					int value = Integer.parseInt(lExpression.getLiteralValue());
					if (Byte.MIN_VALUE <= value && value <= Byte.MAX_VALUE) {
						lExpression.setType(TypesUtilities.BYTE_TYPE);
						returnValue.setSecond((byte) value);
					} else if (Short.MIN_VALUE <= value
							&& value <= Short.MAX_VALUE) {
						lExpression.setType(TypesUtilities.SHORT_TYPE);
						returnValue.setSecond((short) value);
					} else if (Integer.MIN_VALUE <= value
							&& value <= Integer.MAX_VALUE) {
						lExpression.setType(TypesUtilities.INT_TYPE);
						returnValue.setSecond(value);
					}
				} catch (NumberFormatException nfe) {
					container.getExceptions().add(
							getOutofRangeException(
									lExpression.getLiteralValue(),
									TypesUtilities.INT_TYPE,
									lExpression.getLineNumber(),
									lExpression.getCharacter()));
				}
			} else if (lExpression.getType() == TypesUtilities.DOUBLE_TYPE) {
				try {
					double value = Double.parseDouble(lExpression
							.getLiteralValue());
					returnValue.setSecond(value);
				} catch (NumberFormatException nfe) {
					container.getExceptions().add(
							getOutofRangeException(
									lExpression.getLiteralValue(),
									TypesUtilities.DOUBLE_TYPE,
									lExpression.getLineNumber(),
									lExpression.getCharacter()));
				}
			} else if (lExpression.getType() == TypesUtilities.FLOAT_TYPE) {
				try {
					float value = Float.parseFloat(lExpression
							.getLiteralValue());
					returnValue.setSecond(value);
				} catch (NumberFormatException nfe) {
					container.getExceptions().add(
							getOutofRangeException(
									lExpression.getLiteralValue(),
									TypesUtilities.FLOAT_TYPE,
									lExpression.getLineNumber(),
									lExpression.getCharacter()));
				}
			} else if (lExpression.getType() == TypesUtilities.BOOLEAN_TYPE) {
				try {
					boolean value = Boolean.parseBoolean(lExpression
							.getLiteralValue());
					returnValue.setSecond(value);
				} catch (NumberFormatException nfe) {
					container.getExceptions().add(
							getOutofRangeException(
									lExpression.getLiteralValue(),
									TypesUtilities.BOOLEAN_TYPE,
									lExpression.getLineNumber(),
									lExpression.getCharacter()));
				}
			}
			returnValue.setFirst(lExpression.getType());
		} else if (expression instanceof PlusSubExpression) {
			PlusSubExpression pspExpression = (PlusSubExpression) expression;
			Type type = subEvaluator(pspExpression.getValue(), scopeHandler,
					compilerFeatures, container).getFirst();
			if (!TypesUtilities.getInstance().canTypeUpCastTo(type,
					TypesUtilities.INT_TYPE)) {
				CodeCompilationException cce = TypesUtilities
						.getTypeMismatchException(type, TypesUtilities.INT_TYPE);
				cce.setColumn(pspExpression.getValue().getCharacter());
				cce.setLine(pspExpression.getValue().getLineNumber());
				container.addException(cce);
			}
			if (!isInLValueStyle(pspExpression.getValue())) {
				container.getExceptions().add(
						new CodeCompilationException(
								"Invalid argument to operation ++/--",
								pspExpression.getLineNumber(), pspExpression
										.getCharacter()));
			}
			pspExpression.setType(type);
			returnValue.setFirst(pspExpression.getType());
		} else if (expression instanceof PrimaryExpression) {
			PrimaryExpression pExpression = (PrimaryExpression) expression;
			returnValue = evaluatePrimaryExpression(pExpression, scopeHandler,
					compilerFeatures, container);
		}
		return returnValue;
	}

	protected Type getResultType(String operator, Type lType, Type rType)
			throws CodeCompilationException {
		Type retValue = TypesUtilities.UNKNOWN_TYPE;

		// Assignment operators which are applicable for all values
		if (operator.equals("=")) {
			if (!TypesUtilities.getInstance().canTypeUpCastTo(rType, lType)) {
				CodeCompilationException cce = createEvaluateExceptionMessage2(
						0, 0, operator, rType, lType);
				if (cce == null)
					return retValue;
				throw cce;
			}
			retValue = lType;
		}

		// Arithmetic operators which are applicable for both integer and real
		// values
		Set<String> arithmeticFreeOperators = new HashSet<String>();
		arithmeticFreeOperators.addAll(Arrays.asList("+", "-", "*", "/"));
		if (arithmeticFreeOperators.contains(operator)) {
			Type biggerType = TypesUtilities.getInstance().getTheBiggerType(
					lType, rType);
			if (!TypesUtilities.getInstance().canTypeUpCastTo(biggerType,
					TypesUtilities.DOUBLE_TYPE)) {
				CodeCompilationException cce = createEvaluateExceptionMessage2(
						0, 0, operator, rType, lType);
				if (cce == null)
					return retValue;
				throw cce;
			} else {
				if (TypesUtilities.getInstance().canTypeUpCastTo(biggerType,
						TypesUtilities.INT_TYPE)) {
					retValue = TypesUtilities.INT_TYPE;
				} else {
					retValue = biggerType;
				}
			}
		}

		Set<String> arithmeticFreeAssignmentOperators = new HashSet<String>();
		arithmeticFreeAssignmentOperators.addAll(Arrays.asList("+=", "-=",
				"*=", "/="));
		if (arithmeticFreeAssignmentOperators.contains(operator)) {
			if (!TypesUtilities.getInstance().canTypeUpCastTo(rType, lType)
					|| !TypesUtilities.getInstance().canTypeUpCastTo(lType,
							TypesUtilities.DOUBLE_TYPE)) {
				CodeCompilationException cce = createEvaluateExceptionMessage2(
						0, 0, operator, rType, lType);
				if (cce == null)
					return retValue;
				throw cce;
			}
			retValue = lType;
		}

		// Arithmetic operators which are applicable for only integer values
		Set<String> arithmeticIntegerOperators = new HashSet<String>();
		arithmeticIntegerOperators.addAll(Arrays.asList("%", ">>", "<<"));
		if (arithmeticIntegerOperators.contains(operator)) {
			Type biggerType = TypesUtilities.getInstance().getTheBiggerType(
					lType, rType);
			if (!TypesUtilities.getInstance().canTypeUpCastTo(biggerType,
					TypesUtilities.INT_TYPE)) {
				CodeCompilationException cce = createEvaluateExceptionMessage2(
						0, 0, operator, rType, lType);
				if (cce == null)
					return retValue;
				throw cce;
			}
			retValue = biggerType;
		}

		Set<String> arithmeticIntegerAssignmentOperators = new HashSet<String>();
		arithmeticIntegerAssignmentOperators.addAll(Arrays.asList(">>=", "<<=",
				"%="));
		if (arithmeticIntegerAssignmentOperators.contains(operator)) {
			if (!TypesUtilities.getInstance().canTypeUpCastTo(rType, lType)
					|| !TypesUtilities.getInstance().canTypeUpCastTo(lType,
							TypesUtilities.INT_TYPE)) {
				CodeCompilationException cce = createEvaluateExceptionMessage2(
						0, 0, operator, rType, lType);
				if (cce == null)
					return retValue;
				throw cce;
			}
			retValue = lType;
		}

		// Bitwise operators which are applicable for both integer and boolean
		// values
		Set<String> bitwiseOperators = new HashSet<String>();
		bitwiseOperators.addAll(Arrays.asList("|", "&", "^", "~"));
		if (bitwiseOperators.contains(operator)) {
			Type biggerType = TypesUtilities.getInstance().getTheBiggerType(
					lType, rType);
			if (!TypesUtilities.getInstance().canTypeUpCastTo(biggerType,
					TypesUtilities.INT_TYPE)
					&& !TypesUtilities.getInstance().canTypeUpCastTo(
							biggerType, TypesUtilities.BOOLEAN_TYPE)) {
				CodeCompilationException cce = createEvaluateExceptionMessage2(
						0, 0, operator, rType, lType);
				if (cce == null)
					return retValue;
				throw cce;
			}
			retValue = biggerType;
		}

		Set<String> bitwiseAssignmentOperators = new HashSet<String>();
		bitwiseAssignmentOperators
				.addAll(Arrays.asList("|=", "&=", "^=", "~="));
		if (bitwiseAssignmentOperators.contains(operator)) {
			if (!TypesUtilities.getInstance().canTypeUpCastTo(rType, lType)
					|| (!TypesUtilities.getInstance().canTypeUpCastTo(lType,
							TypesUtilities.INT_TYPE) && !TypesUtilities
							.getInstance().canTypeUpCastTo(lType,
									TypesUtilities.BOOLEAN_TYPE))) {
				CodeCompilationException cce = createEvaluateExceptionMessage2(
						0, 0, operator, rType, lType);
				if (cce == null)
					return retValue;
				throw cce;
			}
			retValue = lType;
		}

		// relational operators which are applicable for both integer and real
		// values
		Set<String> relationalOperators = new HashSet<String>();
		relationalOperators.addAll(Arrays.asList("<", ">", "<=", ">="));
		if (relationalOperators.contains(operator)) {
			Type biggerType = TypesUtilities.getInstance().getTheBiggerType(
					lType, rType);
			if (!TypesUtilities.getInstance().canTypeUpCastTo(biggerType,
					TypesUtilities.DOUBLE_TYPE)) {
				CodeCompilationException cce = createEvaluateExceptionMessage2(
						0, 0, operator, rType, lType);
				if (cce == null)
					return retValue;
				throw cce;
			}
			retValue = TypesUtilities.BOOLEAN_TYPE;
		}

		// relational operators which are applicable for integer, real, boolean,
		// and reactive classes values
		Set<String> relationalEQOperators = new HashSet<String>();
		relationalEQOperators.addAll(Arrays.asList("==", "!="));
		if (relationalEQOperators.contains(operator)) {
			Type biggerType = TypesUtilities.getInstance().getTheBiggerType(
					lType, rType);
			if (!TypesUtilities.getInstance().canTypeUpCastTo(biggerType,
					TypesUtilities.DOUBLE_TYPE)
					&& !TypesUtilities.getInstance().canTypeUpCastTo(
							biggerType, TypesUtilities.BOOLEAN_TYPE)
					&& !TypesUtilities.getInstance().canTypeUpCastTo(
							biggerType, TypesUtilities.REACTIVE_CLASS_TYPE)) {
				CodeCompilationException cce = createEvaluateExceptionMessage2(
						0, 0, operator, rType, lType);
				if (cce == null)
					return retValue;
				throw cce;
			}
			retValue = TypesUtilities.BOOLEAN_TYPE;
		}

		// logical operators which are applicable for integer, real, boolean,
		// and reactive classes values
		Set<String> logicalOperators = new HashSet<String>();
		logicalOperators.addAll(Arrays.asList("&&", "||"));
		if (logicalOperators.contains(operator)) {
			if (!TypesUtilities.getInstance().canTypeUpCastTo(lType,
					TypesUtilities.BOOLEAN_TYPE)
					|| !TypesUtilities.getInstance().canTypeUpCastTo(rType,
							TypesUtilities.BOOLEAN_TYPE)) {
				CodeCompilationException cce = createEvaluateExceptionMessage2(
						0, 0, operator, rType, lType);
				if (cce == null)
					return retValue;
				throw cce;
			}
			retValue = TypesUtilities.BOOLEAN_TYPE;
		}
		return retValue;
	}

	protected boolean isInLValueStyle(Expression expression) {
		if (!(expression instanceof PrimaryExpression))
			return false;
		PrimaryExpression pExpression = (PrimaryExpression) expression;
		while (pExpression instanceof DotPrimary) {
			pExpression = ((DotPrimary) pExpression).getRight();
		}
		return ((TermPrimary) pExpression).getParentSuffixPrimary() == null;
	}

	protected Pair<Type, Object> evaluatePrimaryExpression(
			PrimaryExpression pExpression, ScopeHandler scopeHandler,
			Set<CompilerFeature> compilerFeatures, ExceptionContainer container) {
		Pair<Type, Object> returnValue = new Pair<Type, Object>();
		// TODO should be modified in case of supporting "record"
		if (pExpression instanceof DotPrimary) {
			DotPrimary dotPrimary = (DotPrimary) pExpression;
			Expression firstTerm = dotPrimary.getLeft();
			Type termType = evaluate(firstTerm, scopeHandler, compilerFeatures,
					container).getFirst();
			firstTerm.setType(termType);
			PrimaryExpression pe = dotPrimary.getRight();
			while (pe instanceof DotPrimary) {
				termType = evaluatePrimaryTermExpression(termType,
						(TermPrimary) ((DotPrimary) pe).getLeft(),
						scopeHandler, compilerFeatures, container).getFirst();
				((DotPrimary) pe).getLeft().setType(termType);
				pe = ((DotPrimary) pe).getRight();
			}
			termType = evaluatePrimaryTermExpression(termType,
					(TermPrimary) pe, scopeHandler, compilerFeatures, container)
					.getFirst();
			pe.setType(termType);
			pExpression.setType(termType);
		} else if (pExpression instanceof TermPrimary) {
			returnValue = evaluatePrimaryTermExpression(TypesUtilities.NO_TYPE,
					(TermPrimary) pExpression, scopeHandler, compilerFeatures,
					container);
			pExpression.setType(returnValue.getFirst());
		} else {
			throw new CompilerInternalErrorRuntimeException(
					"Unknown primary type \""
							+ pExpression.getClass().getName() + "\"");
		}
		returnValue.setFirst(pExpression.getType());
		return returnValue;
	}

	protected Pair<Type, Object> evaluatePrimaryTermExpression(Type baseType,
			TermPrimary termPrimary, ScopeHandler scopeHandler,
			Set<CompilerFeature> compilerFeatures, ExceptionContainer container) {
		Pair<Type, Object> returnValue = new Pair<Type, Object>(
				TypesUtilities.UNKNOWN_TYPE, null);
		String termName = termPrimary.getName();
		try {
			if (termPrimary.getParentSuffixPrimary() == null) {
				// The term specifies access to a variable
				if (baseType == TypesUtilities.NO_TYPE) {
					Type foundType = scopeHandler
							.retreiveVariableTypeFromScope(termName);
					if (containerType == TypesUtilities.CONSTRUCTOR_TYPE) {
						if (TypesUtilities.getInstance().canTypeCastTo(
								foundType, TypesUtilities.REACTIVE_CLASS_TYPE)) {
							if (!termName.equals("self")) {
								CodeCompilationException cce = new CodeCompilationException(
										"Access denied to rebec reference \""
												+ termName
												+ "\"; Construcotr is only allowed to access to \"self\"",
										0, 0);
								throw cce;
							}
						}
					}
					returnValue.setFirst(foundType);
					Object value = scopeHandler
							.retrieveCompileTimeEvaluatableVariable(termName);
					returnValue.setSecond(value);
				} else {
					// TODO should be modified in case of supporting "record"
					container.getExceptions().add(
							new CodeCompilationException(
									"Invalid access to state variable "
											+ termName + "", termPrimary
											.getLineNumber(), termPrimary
											.getCharacter()));
					returnValue.setFirst(TypesUtilities.UNKNOWN_TYPE);
					return returnValue;
				}
			} else {
				// Term specifies method call (synch method or message server)
				for (Expression expression : termPrimary
						.getParentSuffixPrimary().getArguments()) {
					expression.setType(evaluate(expression, scopeHandler,
							compilerFeatures, container).getFirst());
				}
				if (baseType == TypesUtilities.NO_TYPE) {
					returnValue.setFirst(scopeHandler
							.retreiveMethodReturnTypeFromScope(termName,
									termPrimary.getParentSuffixPrimary()
											.getArguments()));
				} else {
					for (Expression argumentExpresion : termPrimary
							.getParentSuffixPrimary().getArguments()) {
						if (argumentExpresion.getType() == TypesUtilities.UNKNOWN_TYPE)
							return returnValue;
					}
					returnValue.setFirst(scopeHandler
							.retreiveMethodReturnTypeFromScope(TypesUtilities
									.getTypeName(baseType), termName,
									termPrimary.getParentSuffixPrimary()
											.getArguments()));
				}

			}
			if (!termPrimary.getIndices().isEmpty()) {
				if (!(returnValue.getFirst() instanceof ArrayType)) {
					ArrayType arrayType = new ArrayType();
					arrayType.setPrimitiveType((PrimitiveType) returnValue
							.getFirst());
					arrayType.getDimensions().add(0);
					throw TypesUtilities.getTypeMismatchException(
							returnValue.getFirst(), arrayType);
				}
				ArrayType foundTypeInArray = (ArrayType) returnValue.getFirst();
				if (termPrimary.getIndices().size() > foundTypeInArray
						.getDimensions().size()) {
					ArrayType arrayType = new ArrayType();
					arrayType.setPrimitiveType(foundTypeInArray
							.getPrimitiveType());
					for (int cnt = 0; cnt < termPrimary.getIndices().size(); cnt++)
						arrayType.getDimensions().add(0);
					throw TypesUtilities.getTypeMismatchException(
							returnValue.getFirst(), arrayType);
				}

				for (Expression expression : termPrimary.getIndices()) {
					Type type = evaluate(expression, scopeHandler,
							compilerFeatures, container).getFirst();
					if (!TypesUtilities.getInstance().canTypeCastTo(type,
							TypesUtilities.INT_TYPE)) {
						throw TypesUtilities.getTypeMismatchException(type,
								TypesUtilities.INT_TYPE);
					}
				}
				if (termPrimary.getIndices().size() == foundTypeInArray
						.getDimensions().size()) {
					returnValue.setFirst(foundTypeInArray.getPrimitiveType());
				} else {
					ArrayType arrayType = new ArrayType();
					arrayType.setPrimitiveType(foundTypeInArray
							.getPrimitiveType());
					for (int cnt = 0; cnt < foundTypeInArray.getDimensions()
							.size() - termPrimary.getIndices().size(); cnt++)
						arrayType.getDimensions().add(
								foundTypeInArray.getDimensions().get(cnt));
					returnValue.setFirst(arrayType);
				}

			}
		} catch (CodeCompilationException cce) {
			cce.setColumn(termPrimary.getCharacter());
			cce.setLine(termPrimary.getLineNumber());
			container.addException(cce);
		}

		return returnValue;
	}

	protected Object evaluateConstantTerm(String operator, Type type,
			Object left, Object right) {

		ScriptEngineManager mgr = new ScriptEngineManager();
		ScriptEngine engine = mgr.getEngineByName("JavaScript");
		try {
			if (right != null && left != null) {
				double value = ((Double) engine.eval(left + operator + right));
				if (type == TypesUtilities.INT_TYPE)
					return (int) value;
				if (type == TypesUtilities.FLOAT_TYPE)
					return (float) value;
				if (type == TypesUtilities.DOUBLE_TYPE)
					return value;
			} else if (left != null) {
				double value = ((Double) engine.eval(left.toString()));
				if (operator.equals("(byte)"))
					return (byte) value;
				if (operator.equals("(short)"))
					return (short) value;
				if (operator.equals("(int)"))
					return (int) value;
				if (operator.equals("(float)"))
					return (float) value;
				if (operator.equals("(double)"))
					return (double) value;
			}
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	protected static CodeCompilationException getOutofRangeException(
			String value, Type type, int line, int column) {
		return new CodeCompilationException("The literal " + value
				+ " of type " + TypesUtilities.getTypeName(type)
				+ " is out of range ", line, column);
	}

	protected static CodeCompilationException createEvaluateExceptionMessage2(
			int lineNumber, int column, String operator, Type... types) {
		String typesString = "";
		for (Type type : types) {
			if (type == null || type == TypesUtilities.UNKNOWN_TYPE)
				return null;
			typesString += ", "
					+ (type == null ? "unknown" : TypesUtilities
							.getTypeName(type));
		}
		if (types.length > 0)
			typesString = typesString.substring(2);

		return new CodeCompilationException("The operator " + operator
				+ " is undefined for the argument type(s) " + typesString,
				lineNumber, column);
	}

}
