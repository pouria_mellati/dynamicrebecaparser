// Generated from CoreRebecaComplete.g4 by ANTLR 4.0

	package org.rebecalang.compiler.modelcompiler.corerebeca.compiler;
	import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.*;
	import java.util.*;
	import org.antlr.runtime.BitSet;
	import org.rebecalang.compiler.utils.TypesUtilities;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;

public class CoreRebecaCompleteBaseListener implements CoreRebecaCompleteListener {
	@Override public void enterVariableDeclarator(CoreRebecaCompleteParser.VariableDeclaratorContext ctx) { }
	@Override public void exitVariableDeclarator(CoreRebecaCompleteParser.VariableDeclaratorContext ctx) { }

	@Override public void enterExpression(CoreRebecaCompleteParser.ExpressionContext ctx) { }
	@Override public void exitExpression(CoreRebecaCompleteParser.ExpressionContext ctx) { }

	@Override public void enterRelationalOp(CoreRebecaCompleteParser.RelationalOpContext ctx) { }
	@Override public void exitRelationalOp(CoreRebecaCompleteParser.RelationalOpContext ctx) { }

	@Override public void enterExpressionList(CoreRebecaCompleteParser.ExpressionListContext ctx) { }
	@Override public void exitExpressionList(CoreRebecaCompleteParser.ExpressionListContext ctx) { }

	@Override public void enterUnaryExpression(CoreRebecaCompleteParser.UnaryExpressionContext ctx) { }
	@Override public void exitUnaryExpression(CoreRebecaCompleteParser.UnaryExpressionContext ctx) { }

	@Override public void enterType(CoreRebecaCompleteParser.TypeContext ctx) { }
	@Override public void exitType(CoreRebecaCompleteParser.TypeContext ctx) { }

	@Override public void enterPrimary(CoreRebecaCompleteParser.PrimaryContext ctx) { }
	@Override public void exitPrimary(CoreRebecaCompleteParser.PrimaryContext ctx) { }

	@Override public void enterShiftExpression(CoreRebecaCompleteParser.ShiftExpressionContext ctx) { }
	@Override public void exitShiftExpression(CoreRebecaCompleteParser.ShiftExpressionContext ctx) { }

	@Override public void enterImportDeclaration(CoreRebecaCompleteParser.ImportDeclarationContext ctx) { }
	@Override public void exitImportDeclaration(CoreRebecaCompleteParser.ImportDeclarationContext ctx) { }

	@Override public void enterRebecaCode(CoreRebecaCompleteParser.RebecaCodeContext ctx) { }
	@Override public void exitRebecaCode(CoreRebecaCompleteParser.RebecaCodeContext ctx) { }

	@Override public void enterFormalParametersDeclaration(CoreRebecaCompleteParser.FormalParametersDeclarationContext ctx) { }
	@Override public void exitFormalParametersDeclaration(CoreRebecaCompleteParser.FormalParametersDeclarationContext ctx) { }

	@Override public void enterPackageDeclaration(CoreRebecaCompleteParser.PackageDeclarationContext ctx) { }
	@Override public void exitPackageDeclaration(CoreRebecaCompleteParser.PackageDeclarationContext ctx) { }

	@Override public void enterConditionalAndExpression(CoreRebecaCompleteParser.ConditionalAndExpressionContext ctx) { }
	@Override public void exitConditionalAndExpression(CoreRebecaCompleteParser.ConditionalAndExpressionContext ctx) { }

	@Override public void enterVariableDeclarators(CoreRebecaCompleteParser.VariableDeclaratorsContext ctx) { }
	@Override public void exitVariableDeclarators(CoreRebecaCompleteParser.VariableDeclaratorsContext ctx) { }

	@Override public void enterAdditiveExpression(CoreRebecaCompleteParser.AdditiveExpressionContext ctx) { }
	@Override public void exitAdditiveExpression(CoreRebecaCompleteParser.AdditiveExpressionContext ctx) { }

	@Override public void enterSwitchBlock(CoreRebecaCompleteParser.SwitchBlockContext ctx) { }
	@Override public void exitSwitchBlock(CoreRebecaCompleteParser.SwitchBlockContext ctx) { }

	@Override public void enterStatement(CoreRebecaCompleteParser.StatementContext ctx) { }
	@Override public void exitStatement(CoreRebecaCompleteParser.StatementContext ctx) { }

	@Override public void enterExclusiveOrExpression(CoreRebecaCompleteParser.ExclusiveOrExpressionContext ctx) { }
	@Override public void exitExclusiveOrExpression(CoreRebecaCompleteParser.ExclusiveOrExpressionContext ctx) { }

	@Override public void enterInstanceOfExpression(CoreRebecaCompleteParser.InstanceOfExpressionContext ctx) { }
	@Override public void exitInstanceOfExpression(CoreRebecaCompleteParser.InstanceOfExpressionContext ctx) { }

	@Override public void enterMsgsrvDeclaration(CoreRebecaCompleteParser.MsgsrvDeclarationContext ctx) { }
	@Override public void exitMsgsrvDeclaration(CoreRebecaCompleteParser.MsgsrvDeclarationContext ctx) { }

	@Override public void enterMultiplicativeExpression(CoreRebecaCompleteParser.MultiplicativeExpressionContext ctx) { }
	@Override public void exitMultiplicativeExpression(CoreRebecaCompleteParser.MultiplicativeExpressionContext ctx) { }

	@Override public void enterAssignmentOperator(CoreRebecaCompleteParser.AssignmentOperatorContext ctx) { }
	@Override public void exitAssignmentOperator(CoreRebecaCompleteParser.AssignmentOperatorContext ctx) { }

	@Override public void enterShiftOp(CoreRebecaCompleteParser.ShiftOpContext ctx) { }
	@Override public void exitShiftOp(CoreRebecaCompleteParser.ShiftOpContext ctx) { }

	@Override public void enterStatementExpression(CoreRebecaCompleteParser.StatementExpressionContext ctx) { }
	@Override public void exitStatementExpression(CoreRebecaCompleteParser.StatementExpressionContext ctx) { }

	@Override public void enterUnaryExpressionNotPlusMinus(CoreRebecaCompleteParser.UnaryExpressionNotPlusMinusContext ctx) { }
	@Override public void exitUnaryExpressionNotPlusMinus(CoreRebecaCompleteParser.UnaryExpressionNotPlusMinusContext ctx) { }

	@Override public void enterBlock(CoreRebecaCompleteParser.BlockContext ctx) { }
	@Override public void exitBlock(CoreRebecaCompleteParser.BlockContext ctx) { }

	@Override public void enterVariableInitializer(CoreRebecaCompleteParser.VariableInitializerContext ctx) { }
	@Override public void exitVariableInitializer(CoreRebecaCompleteParser.VariableInitializerContext ctx) { }

	@Override public void enterFormalParameterDeclaration(CoreRebecaCompleteParser.FormalParameterDeclarationContext ctx) { }
	@Override public void exitFormalParameterDeclaration(CoreRebecaCompleteParser.FormalParameterDeclarationContext ctx) { }

	@Override public void enterMainRebecDefinition(CoreRebecaCompleteParser.MainRebecDefinitionContext ctx) { }
	@Override public void exitMainRebecDefinition(CoreRebecaCompleteParser.MainRebecDefinitionContext ctx) { }

	@Override public void enterConditionalExpression(CoreRebecaCompleteParser.ConditionalExpressionContext ctx) { }
	@Override public void exitConditionalExpression(CoreRebecaCompleteParser.ConditionalExpressionContext ctx) { }

	@Override public void enterAndExpression(CoreRebecaCompleteParser.AndExpressionContext ctx) { }
	@Override public void exitAndExpression(CoreRebecaCompleteParser.AndExpressionContext ctx) { }

	@Override public void enterRebecaModel(CoreRebecaCompleteParser.RebecaModelContext ctx) { }
	@Override public void exitRebecaModel(CoreRebecaCompleteParser.RebecaModelContext ctx) { }

	@Override public void enterFieldDeclaration(CoreRebecaCompleteParser.FieldDeclarationContext ctx) { }
	@Override public void exitFieldDeclaration(CoreRebecaCompleteParser.FieldDeclarationContext ctx) { }

	@Override public void enterRelationalExpression(CoreRebecaCompleteParser.RelationalExpressionContext ctx) { }
	@Override public void exitRelationalExpression(CoreRebecaCompleteParser.RelationalExpressionContext ctx) { }

	@Override public void enterDimensions(CoreRebecaCompleteParser.DimensionsContext ctx) { }
	@Override public void exitDimensions(CoreRebecaCompleteParser.DimensionsContext ctx) { }

	@Override public void enterConditionalOrExpression(CoreRebecaCompleteParser.ConditionalOrExpressionContext ctx) { }
	@Override public void exitConditionalOrExpression(CoreRebecaCompleteParser.ConditionalOrExpressionContext ctx) { }

	@Override public void enterSynchMethodDeclaration(CoreRebecaCompleteParser.SynchMethodDeclarationContext ctx) { }
	@Override public void exitSynchMethodDeclaration(CoreRebecaCompleteParser.SynchMethodDeclarationContext ctx) { }

	@Override public void enterEnvironmentVariables(CoreRebecaCompleteParser.EnvironmentVariablesContext ctx) { }
	@Override public void exitEnvironmentVariables(CoreRebecaCompleteParser.EnvironmentVariablesContext ctx) { }

	@Override public void enterMethodDeclaration(CoreRebecaCompleteParser.MethodDeclarationContext ctx) { }
	@Override public void exitMethodDeclaration(CoreRebecaCompleteParser.MethodDeclarationContext ctx) { }

	@Override public void enterConstructorDeclaration(CoreRebecaCompleteParser.ConstructorDeclarationContext ctx) { }
	@Override public void exitConstructorDeclaration(CoreRebecaCompleteParser.ConstructorDeclarationContext ctx) { }

	@Override public void enterReactiveClassDeclaration(CoreRebecaCompleteParser.ReactiveClassDeclarationContext ctx) { }
	@Override public void exitReactiveClassDeclaration(CoreRebecaCompleteParser.ReactiveClassDeclarationContext ctx) { }

	@Override public void enterRecordDeclaration(CoreRebecaCompleteParser.RecordDeclarationContext ctx) { }
	@Override public void exitRecordDeclaration(CoreRebecaCompleteParser.RecordDeclarationContext ctx) { }

	@Override public void enterInclusiveOrExpression(CoreRebecaCompleteParser.InclusiveOrExpressionContext ctx) { }
	@Override public void exitInclusiveOrExpression(CoreRebecaCompleteParser.InclusiveOrExpressionContext ctx) { }

	@Override public void enterMainDeclaration(CoreRebecaCompleteParser.MainDeclarationContext ctx) { }
	@Override public void exitMainDeclaration(CoreRebecaCompleteParser.MainDeclarationContext ctx) { }

	@Override public void enterEqualityExpression(CoreRebecaCompleteParser.EqualityExpressionContext ctx) { }
	@Override public void exitEqualityExpression(CoreRebecaCompleteParser.EqualityExpressionContext ctx) { }

	@Override public void enterArrayInitializer(CoreRebecaCompleteParser.ArrayInitializerContext ctx) { }
	@Override public void exitArrayInitializer(CoreRebecaCompleteParser.ArrayInitializerContext ctx) { }

	@Override public void enterFormalParameters(CoreRebecaCompleteParser.FormalParametersContext ctx) { }
	@Override public void exitFormalParameters(CoreRebecaCompleteParser.FormalParametersContext ctx) { }

	@Override public void enterCastExpression(CoreRebecaCompleteParser.CastExpressionContext ctx) { }
	@Override public void exitCastExpression(CoreRebecaCompleteParser.CastExpressionContext ctx) { }

	@Override public void enterForInit(CoreRebecaCompleteParser.ForInitContext ctx) { }
	@Override public void exitForInit(CoreRebecaCompleteParser.ForInitContext ctx) { }

	@Override public void enterLiteral(CoreRebecaCompleteParser.LiteralContext ctx) { }
	@Override public void exitLiteral(CoreRebecaCompleteParser.LiteralContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	@Override public void visitTerminal(TerminalNode node) { }
	@Override public void visitErrorNode(ErrorNode node) { }
}