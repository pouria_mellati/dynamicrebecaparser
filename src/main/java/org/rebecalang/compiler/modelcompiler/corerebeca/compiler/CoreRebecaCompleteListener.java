// Generated from CoreRebecaComplete.g4 by ANTLR 4.0

	package org.rebecalang.compiler.modelcompiler.corerebeca.compiler;
	import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.*;
	import java.util.*;
	import org.antlr.runtime.BitSet;
	import org.rebecalang.compiler.utils.TypesUtilities;

import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface CoreRebecaCompleteListener extends ParseTreeListener {
	void enterVariableDeclarator(CoreRebecaCompleteParser.VariableDeclaratorContext ctx);
	void exitVariableDeclarator(CoreRebecaCompleteParser.VariableDeclaratorContext ctx);

	void enterExpression(CoreRebecaCompleteParser.ExpressionContext ctx);
	void exitExpression(CoreRebecaCompleteParser.ExpressionContext ctx);

	void enterRelationalOp(CoreRebecaCompleteParser.RelationalOpContext ctx);
	void exitRelationalOp(CoreRebecaCompleteParser.RelationalOpContext ctx);

	void enterExpressionList(CoreRebecaCompleteParser.ExpressionListContext ctx);
	void exitExpressionList(CoreRebecaCompleteParser.ExpressionListContext ctx);

	void enterUnaryExpression(CoreRebecaCompleteParser.UnaryExpressionContext ctx);
	void exitUnaryExpression(CoreRebecaCompleteParser.UnaryExpressionContext ctx);

	void enterType(CoreRebecaCompleteParser.TypeContext ctx);
	void exitType(CoreRebecaCompleteParser.TypeContext ctx);

	void enterPrimary(CoreRebecaCompleteParser.PrimaryContext ctx);
	void exitPrimary(CoreRebecaCompleteParser.PrimaryContext ctx);

	void enterShiftExpression(CoreRebecaCompleteParser.ShiftExpressionContext ctx);
	void exitShiftExpression(CoreRebecaCompleteParser.ShiftExpressionContext ctx);

	void enterImportDeclaration(CoreRebecaCompleteParser.ImportDeclarationContext ctx);
	void exitImportDeclaration(CoreRebecaCompleteParser.ImportDeclarationContext ctx);

	void enterRebecaCode(CoreRebecaCompleteParser.RebecaCodeContext ctx);
	void exitRebecaCode(CoreRebecaCompleteParser.RebecaCodeContext ctx);

	void enterFormalParametersDeclaration(CoreRebecaCompleteParser.FormalParametersDeclarationContext ctx);
	void exitFormalParametersDeclaration(CoreRebecaCompleteParser.FormalParametersDeclarationContext ctx);

	void enterPackageDeclaration(CoreRebecaCompleteParser.PackageDeclarationContext ctx);
	void exitPackageDeclaration(CoreRebecaCompleteParser.PackageDeclarationContext ctx);

	void enterConditionalAndExpression(CoreRebecaCompleteParser.ConditionalAndExpressionContext ctx);
	void exitConditionalAndExpression(CoreRebecaCompleteParser.ConditionalAndExpressionContext ctx);

	void enterVariableDeclarators(CoreRebecaCompleteParser.VariableDeclaratorsContext ctx);
	void exitVariableDeclarators(CoreRebecaCompleteParser.VariableDeclaratorsContext ctx);

	void enterAdditiveExpression(CoreRebecaCompleteParser.AdditiveExpressionContext ctx);
	void exitAdditiveExpression(CoreRebecaCompleteParser.AdditiveExpressionContext ctx);

	void enterSwitchBlock(CoreRebecaCompleteParser.SwitchBlockContext ctx);
	void exitSwitchBlock(CoreRebecaCompleteParser.SwitchBlockContext ctx);

	void enterStatement(CoreRebecaCompleteParser.StatementContext ctx);
	void exitStatement(CoreRebecaCompleteParser.StatementContext ctx);

	void enterExclusiveOrExpression(CoreRebecaCompleteParser.ExclusiveOrExpressionContext ctx);
	void exitExclusiveOrExpression(CoreRebecaCompleteParser.ExclusiveOrExpressionContext ctx);

	void enterInstanceOfExpression(CoreRebecaCompleteParser.InstanceOfExpressionContext ctx);
	void exitInstanceOfExpression(CoreRebecaCompleteParser.InstanceOfExpressionContext ctx);

	void enterMsgsrvDeclaration(CoreRebecaCompleteParser.MsgsrvDeclarationContext ctx);
	void exitMsgsrvDeclaration(CoreRebecaCompleteParser.MsgsrvDeclarationContext ctx);

	void enterMultiplicativeExpression(CoreRebecaCompleteParser.MultiplicativeExpressionContext ctx);
	void exitMultiplicativeExpression(CoreRebecaCompleteParser.MultiplicativeExpressionContext ctx);

	void enterAssignmentOperator(CoreRebecaCompleteParser.AssignmentOperatorContext ctx);
	void exitAssignmentOperator(CoreRebecaCompleteParser.AssignmentOperatorContext ctx);

	void enterShiftOp(CoreRebecaCompleteParser.ShiftOpContext ctx);
	void exitShiftOp(CoreRebecaCompleteParser.ShiftOpContext ctx);

	void enterStatementExpression(CoreRebecaCompleteParser.StatementExpressionContext ctx);
	void exitStatementExpression(CoreRebecaCompleteParser.StatementExpressionContext ctx);

	void enterUnaryExpressionNotPlusMinus(CoreRebecaCompleteParser.UnaryExpressionNotPlusMinusContext ctx);
	void exitUnaryExpressionNotPlusMinus(CoreRebecaCompleteParser.UnaryExpressionNotPlusMinusContext ctx);

	void enterBlock(CoreRebecaCompleteParser.BlockContext ctx);
	void exitBlock(CoreRebecaCompleteParser.BlockContext ctx);

	void enterVariableInitializer(CoreRebecaCompleteParser.VariableInitializerContext ctx);
	void exitVariableInitializer(CoreRebecaCompleteParser.VariableInitializerContext ctx);

	void enterFormalParameterDeclaration(CoreRebecaCompleteParser.FormalParameterDeclarationContext ctx);
	void exitFormalParameterDeclaration(CoreRebecaCompleteParser.FormalParameterDeclarationContext ctx);

	void enterMainRebecDefinition(CoreRebecaCompleteParser.MainRebecDefinitionContext ctx);
	void exitMainRebecDefinition(CoreRebecaCompleteParser.MainRebecDefinitionContext ctx);

	void enterConditionalExpression(CoreRebecaCompleteParser.ConditionalExpressionContext ctx);
	void exitConditionalExpression(CoreRebecaCompleteParser.ConditionalExpressionContext ctx);

	void enterAndExpression(CoreRebecaCompleteParser.AndExpressionContext ctx);
	void exitAndExpression(CoreRebecaCompleteParser.AndExpressionContext ctx);

	void enterRebecaModel(CoreRebecaCompleteParser.RebecaModelContext ctx);
	void exitRebecaModel(CoreRebecaCompleteParser.RebecaModelContext ctx);

	void enterFieldDeclaration(CoreRebecaCompleteParser.FieldDeclarationContext ctx);
	void exitFieldDeclaration(CoreRebecaCompleteParser.FieldDeclarationContext ctx);

	void enterRelationalExpression(CoreRebecaCompleteParser.RelationalExpressionContext ctx);
	void exitRelationalExpression(CoreRebecaCompleteParser.RelationalExpressionContext ctx);

	void enterDimensions(CoreRebecaCompleteParser.DimensionsContext ctx);
	void exitDimensions(CoreRebecaCompleteParser.DimensionsContext ctx);

	void enterConditionalOrExpression(CoreRebecaCompleteParser.ConditionalOrExpressionContext ctx);
	void exitConditionalOrExpression(CoreRebecaCompleteParser.ConditionalOrExpressionContext ctx);

	void enterSynchMethodDeclaration(CoreRebecaCompleteParser.SynchMethodDeclarationContext ctx);
	void exitSynchMethodDeclaration(CoreRebecaCompleteParser.SynchMethodDeclarationContext ctx);

	void enterEnvironmentVariables(CoreRebecaCompleteParser.EnvironmentVariablesContext ctx);
	void exitEnvironmentVariables(CoreRebecaCompleteParser.EnvironmentVariablesContext ctx);

	void enterMethodDeclaration(CoreRebecaCompleteParser.MethodDeclarationContext ctx);
	void exitMethodDeclaration(CoreRebecaCompleteParser.MethodDeclarationContext ctx);

	void enterConstructorDeclaration(CoreRebecaCompleteParser.ConstructorDeclarationContext ctx);
	void exitConstructorDeclaration(CoreRebecaCompleteParser.ConstructorDeclarationContext ctx);

	void enterReactiveClassDeclaration(CoreRebecaCompleteParser.ReactiveClassDeclarationContext ctx);
	void exitReactiveClassDeclaration(CoreRebecaCompleteParser.ReactiveClassDeclarationContext ctx);

	void enterRecordDeclaration(CoreRebecaCompleteParser.RecordDeclarationContext ctx);
	void exitRecordDeclaration(CoreRebecaCompleteParser.RecordDeclarationContext ctx);

	void enterInclusiveOrExpression(CoreRebecaCompleteParser.InclusiveOrExpressionContext ctx);
	void exitInclusiveOrExpression(CoreRebecaCompleteParser.InclusiveOrExpressionContext ctx);

	void enterMainDeclaration(CoreRebecaCompleteParser.MainDeclarationContext ctx);
	void exitMainDeclaration(CoreRebecaCompleteParser.MainDeclarationContext ctx);

	void enterEqualityExpression(CoreRebecaCompleteParser.EqualityExpressionContext ctx);
	void exitEqualityExpression(CoreRebecaCompleteParser.EqualityExpressionContext ctx);

	void enterArrayInitializer(CoreRebecaCompleteParser.ArrayInitializerContext ctx);
	void exitArrayInitializer(CoreRebecaCompleteParser.ArrayInitializerContext ctx);

	void enterFormalParameters(CoreRebecaCompleteParser.FormalParametersContext ctx);
	void exitFormalParameters(CoreRebecaCompleteParser.FormalParametersContext ctx);

	void enterCastExpression(CoreRebecaCompleteParser.CastExpressionContext ctx);
	void exitCastExpression(CoreRebecaCompleteParser.CastExpressionContext ctx);

	void enterForInit(CoreRebecaCompleteParser.ForInitContext ctx);
	void exitForInit(CoreRebecaCompleteParser.ForInitContext ctx);

	void enterLiteral(CoreRebecaCompleteParser.LiteralContext ctx);
	void exitLiteral(CoreRebecaCompleteParser.LiteralContext ctx);
}