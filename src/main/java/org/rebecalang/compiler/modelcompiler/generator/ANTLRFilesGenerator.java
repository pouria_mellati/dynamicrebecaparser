package org.rebecalang.compiler.modelcompiler.generator;

public class ANTLRFilesGenerator {
	public static void main(String[] args) throws Exception {
		String base = "src/main/resources/org/rebecalang/compiler/modelcompiler/grammar";
		String[] antlrArgs;
		
//		antlrArgs = new String[] { "-o",
//				"src/main/java/org/rebecalang/compiler/corerebeca/compiler",
//				base + "/CoreRebecaComplete.g4" };
//		org.antlr.v4.Tool.main(antlrArgs);

//		antlrArgs = new String[] { "-o",
//				"src/main/java/org/rebecalang/compiler/probabilistictimedrebeca/compiler",
//				base + "/ProbabilisticTimedRebecaComplete.g4" };
//		org.antlr.v4.Tool.main(antlrArgs);
//
		antlrArgs = new String[] { "-o",
				"src/main/java/org/rebecalang/compiler/modelcompiler/timedrebeca/compiler",
				base + "/TimedRebecaComplete.g4" };
		org.antlr.v4.Tool.main(antlrArgs);

	}

}
