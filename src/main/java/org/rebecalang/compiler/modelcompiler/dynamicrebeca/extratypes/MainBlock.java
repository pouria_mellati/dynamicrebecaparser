package org.rebecalang.compiler.modelcompiler.dynamicrebeca.extratypes;

import java.util.ArrayList;
import java.util.List;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.*;

/* We sneak a MainBlock instead of a MainDeclaration into the AST structure of rebeca. */
public class MainBlock extends MainDeclaration
{
	protected BlockStatement block;
	
	@Override
	public List<MainRebecDefinition> getMainRebecDefinition() {
		throw new RuntimeException("A dynamic rebeca MainBlock does not support the old-style getMainRebecDefinition(). Use getBlock() instead.");
	}
	
	public BlockStatement getBlock() {
		return block;
	}
	
	public void setBlock(BlockStatement block) {
		this.block = block;
	} 
}
