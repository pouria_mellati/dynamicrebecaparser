// Generated from DynamicRebecaComplete.g4 by ANTLR 4.0

	package org.rebecalang.compiler.modelcompiler.dynamicrebeca.compiler;
	import org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel.*;
	import org.rebecalang.compiler.modelcompiler.dynamicrebeca.extratypes.*;
	import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.*;
	
	import java.util.*;
	import org.antlr.runtime.BitSet;
	import org.rebecalang.compiler.utils.TypesUtilities;

import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface DynamicRebecaCompleteListener extends ParseTreeListener {
	void enterVariableDeclarator(DynamicRebecaCompleteParser.VariableDeclaratorContext ctx);
	void exitVariableDeclarator(DynamicRebecaCompleteParser.VariableDeclaratorContext ctx);

	void enterExpression(DynamicRebecaCompleteParser.ExpressionContext ctx);
	void exitExpression(DynamicRebecaCompleteParser.ExpressionContext ctx);

	void enterRelationalOp(DynamicRebecaCompleteParser.RelationalOpContext ctx);
	void exitRelationalOp(DynamicRebecaCompleteParser.RelationalOpContext ctx);

	void enterExpressionList(DynamicRebecaCompleteParser.ExpressionListContext ctx);
	void exitExpressionList(DynamicRebecaCompleteParser.ExpressionListContext ctx);

	void enterUnaryExpression(DynamicRebecaCompleteParser.UnaryExpressionContext ctx);
	void exitUnaryExpression(DynamicRebecaCompleteParser.UnaryExpressionContext ctx);

	void enterType(DynamicRebecaCompleteParser.TypeContext ctx);
	void exitType(DynamicRebecaCompleteParser.TypeContext ctx);

	void enterTypeParametersDecl(DynamicRebecaCompleteParser.TypeParametersDeclContext ctx);
	void exitTypeParametersDecl(DynamicRebecaCompleteParser.TypeParametersDeclContext ctx);

	void enterPrimary(DynamicRebecaCompleteParser.PrimaryContext ctx);
	void exitPrimary(DynamicRebecaCompleteParser.PrimaryContext ctx);

	void enterShiftExpression(DynamicRebecaCompleteParser.ShiftExpressionContext ctx);
	void exitShiftExpression(DynamicRebecaCompleteParser.ShiftExpressionContext ctx);

	void enterImportDeclaration(DynamicRebecaCompleteParser.ImportDeclarationContext ctx);
	void exitImportDeclaration(DynamicRebecaCompleteParser.ImportDeclarationContext ctx);

	void enterRebecaCode(DynamicRebecaCompleteParser.RebecaCodeContext ctx);
	void exitRebecaCode(DynamicRebecaCompleteParser.RebecaCodeContext ctx);

	void enterFormalParametersDeclaration(DynamicRebecaCompleteParser.FormalParametersDeclarationContext ctx);
	void exitFormalParametersDeclaration(DynamicRebecaCompleteParser.FormalParametersDeclarationContext ctx);

	void enterPackageDeclaration(DynamicRebecaCompleteParser.PackageDeclarationContext ctx);
	void exitPackageDeclaration(DynamicRebecaCompleteParser.PackageDeclarationContext ctx);

	void enterConditionalAndExpression(DynamicRebecaCompleteParser.ConditionalAndExpressionContext ctx);
	void exitConditionalAndExpression(DynamicRebecaCompleteParser.ConditionalAndExpressionContext ctx);

	void enterVariableDeclarators(DynamicRebecaCompleteParser.VariableDeclaratorsContext ctx);
	void exitVariableDeclarators(DynamicRebecaCompleteParser.VariableDeclaratorsContext ctx);

	void enterAdditiveExpression(DynamicRebecaCompleteParser.AdditiveExpressionContext ctx);
	void exitAdditiveExpression(DynamicRebecaCompleteParser.AdditiveExpressionContext ctx);

	void enterSwitchBlock(DynamicRebecaCompleteParser.SwitchBlockContext ctx);
	void exitSwitchBlock(DynamicRebecaCompleteParser.SwitchBlockContext ctx);

	void enterStatement(DynamicRebecaCompleteParser.StatementContext ctx);
	void exitStatement(DynamicRebecaCompleteParser.StatementContext ctx);

	void enterExclusiveOrExpression(DynamicRebecaCompleteParser.ExclusiveOrExpressionContext ctx);
	void exitExclusiveOrExpression(DynamicRebecaCompleteParser.ExclusiveOrExpressionContext ctx);

	void enterInstanceOfExpression(DynamicRebecaCompleteParser.InstanceOfExpressionContext ctx);
	void exitInstanceOfExpression(DynamicRebecaCompleteParser.InstanceOfExpressionContext ctx);

	void enterParametrizedType(DynamicRebecaCompleteParser.ParametrizedTypeContext ctx);
	void exitParametrizedType(DynamicRebecaCompleteParser.ParametrizedTypeContext ctx);

	void enterMsgsrvDeclaration(DynamicRebecaCompleteParser.MsgsrvDeclarationContext ctx);
	void exitMsgsrvDeclaration(DynamicRebecaCompleteParser.MsgsrvDeclarationContext ctx);

	void enterMultiplicativeExpression(DynamicRebecaCompleteParser.MultiplicativeExpressionContext ctx);
	void exitMultiplicativeExpression(DynamicRebecaCompleteParser.MultiplicativeExpressionContext ctx);

	void enterAssignmentOperator(DynamicRebecaCompleteParser.AssignmentOperatorContext ctx);
	void exitAssignmentOperator(DynamicRebecaCompleteParser.AssignmentOperatorContext ctx);

	void enterShiftOp(DynamicRebecaCompleteParser.ShiftOpContext ctx);
	void exitShiftOp(DynamicRebecaCompleteParser.ShiftOpContext ctx);

	void enterStatementExpression(DynamicRebecaCompleteParser.StatementExpressionContext ctx);
	void exitStatementExpression(DynamicRebecaCompleteParser.StatementExpressionContext ctx);

	void enterUnaryExpressionNotPlusMinus(DynamicRebecaCompleteParser.UnaryExpressionNotPlusMinusContext ctx);
	void exitUnaryExpressionNotPlusMinus(DynamicRebecaCompleteParser.UnaryExpressionNotPlusMinusContext ctx);

	void enterVariableInitializer(DynamicRebecaCompleteParser.VariableInitializerContext ctx);
	void exitVariableInitializer(DynamicRebecaCompleteParser.VariableInitializerContext ctx);

	void enterBlock(DynamicRebecaCompleteParser.BlockContext ctx);
	void exitBlock(DynamicRebecaCompleteParser.BlockContext ctx);

	void enterFormalParameterDeclaration(DynamicRebecaCompleteParser.FormalParameterDeclarationContext ctx);
	void exitFormalParameterDeclaration(DynamicRebecaCompleteParser.FormalParameterDeclarationContext ctx);

	void enterMainRebecDefinition(DynamicRebecaCompleteParser.MainRebecDefinitionContext ctx);
	void exitMainRebecDefinition(DynamicRebecaCompleteParser.MainRebecDefinitionContext ctx);

	void enterConditionalExpression(DynamicRebecaCompleteParser.ConditionalExpressionContext ctx);
	void exitConditionalExpression(DynamicRebecaCompleteParser.ConditionalExpressionContext ctx);

	void enterAndExpression(DynamicRebecaCompleteParser.AndExpressionContext ctx);
	void exitAndExpression(DynamicRebecaCompleteParser.AndExpressionContext ctx);

	void enterRebecaModel(DynamicRebecaCompleteParser.RebecaModelContext ctx);
	void exitRebecaModel(DynamicRebecaCompleteParser.RebecaModelContext ctx);

	void enterFieldDeclaration(DynamicRebecaCompleteParser.FieldDeclarationContext ctx);
	void exitFieldDeclaration(DynamicRebecaCompleteParser.FieldDeclarationContext ctx);

	void enterRelationalExpression(DynamicRebecaCompleteParser.RelationalExpressionContext ctx);
	void exitRelationalExpression(DynamicRebecaCompleteParser.RelationalExpressionContext ctx);

	void enterDimensions(DynamicRebecaCompleteParser.DimensionsContext ctx);
	void exitDimensions(DynamicRebecaCompleteParser.DimensionsContext ctx);

	void enterConditionalOrExpression(DynamicRebecaCompleteParser.ConditionalOrExpressionContext ctx);
	void exitConditionalOrExpression(DynamicRebecaCompleteParser.ConditionalOrExpressionContext ctx);

	void enterSynchMethodDeclaration(DynamicRebecaCompleteParser.SynchMethodDeclarationContext ctx);
	void exitSynchMethodDeclaration(DynamicRebecaCompleteParser.SynchMethodDeclarationContext ctx);

	void enterEnvironmentVariables(DynamicRebecaCompleteParser.EnvironmentVariablesContext ctx);
	void exitEnvironmentVariables(DynamicRebecaCompleteParser.EnvironmentVariablesContext ctx);

	void enterMethodDeclaration(DynamicRebecaCompleteParser.MethodDeclarationContext ctx);
	void exitMethodDeclaration(DynamicRebecaCompleteParser.MethodDeclarationContext ctx);

	void enterConstructorDeclaration(DynamicRebecaCompleteParser.ConstructorDeclarationContext ctx);
	void exitConstructorDeclaration(DynamicRebecaCompleteParser.ConstructorDeclarationContext ctx);

	void enterReactiveClassDeclaration(DynamicRebecaCompleteParser.ReactiveClassDeclarationContext ctx);
	void exitReactiveClassDeclaration(DynamicRebecaCompleteParser.ReactiveClassDeclarationContext ctx);

	void enterRecordDeclaration(DynamicRebecaCompleteParser.RecordDeclarationContext ctx);
	void exitRecordDeclaration(DynamicRebecaCompleteParser.RecordDeclarationContext ctx);

	void enterInclusiveOrExpression(DynamicRebecaCompleteParser.InclusiveOrExpressionContext ctx);
	void exitInclusiveOrExpression(DynamicRebecaCompleteParser.InclusiveOrExpressionContext ctx);

	void enterMainDeclaration(DynamicRebecaCompleteParser.MainDeclarationContext ctx);
	void exitMainDeclaration(DynamicRebecaCompleteParser.MainDeclarationContext ctx);

	void enterEqualityExpression(DynamicRebecaCompleteParser.EqualityExpressionContext ctx);
	void exitEqualityExpression(DynamicRebecaCompleteParser.EqualityExpressionContext ctx);

	void enterArrayInitializer(DynamicRebecaCompleteParser.ArrayInitializerContext ctx);
	void exitArrayInitializer(DynamicRebecaCompleteParser.ArrayInitializerContext ctx);

	void enterFormalParameters(DynamicRebecaCompleteParser.FormalParametersContext ctx);
	void exitFormalParameters(DynamicRebecaCompleteParser.FormalParametersContext ctx);

	void enterCastExpression(DynamicRebecaCompleteParser.CastExpressionContext ctx);
	void exitCastExpression(DynamicRebecaCompleteParser.CastExpressionContext ctx);

	void enterForInit(DynamicRebecaCompleteParser.ForInitContext ctx);
	void exitForInit(DynamicRebecaCompleteParser.ForInitContext ctx);

	void enterLiteral(DynamicRebecaCompleteParser.LiteralContext ctx);
	void exitLiteral(DynamicRebecaCompleteParser.LiteralContext ctx);
}