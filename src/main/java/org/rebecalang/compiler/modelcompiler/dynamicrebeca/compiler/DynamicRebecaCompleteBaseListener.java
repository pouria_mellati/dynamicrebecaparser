// Generated from DynamicRebecaComplete.g4 by ANTLR 4.0

	package org.rebecalang.compiler.modelcompiler.dynamicrebeca.compiler;
	import org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel.*;
	import org.rebecalang.compiler.modelcompiler.dynamicrebeca.extratypes.*;
	import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.*;
	
	import java.util.*;
	import org.antlr.runtime.BitSet;
	import org.rebecalang.compiler.utils.TypesUtilities;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;

public class DynamicRebecaCompleteBaseListener implements DynamicRebecaCompleteListener {
	@Override public void enterVariableDeclarator(DynamicRebecaCompleteParser.VariableDeclaratorContext ctx) { }
	@Override public void exitVariableDeclarator(DynamicRebecaCompleteParser.VariableDeclaratorContext ctx) { }

	@Override public void enterExpression(DynamicRebecaCompleteParser.ExpressionContext ctx) { }
	@Override public void exitExpression(DynamicRebecaCompleteParser.ExpressionContext ctx) { }

	@Override public void enterRelationalOp(DynamicRebecaCompleteParser.RelationalOpContext ctx) { }
	@Override public void exitRelationalOp(DynamicRebecaCompleteParser.RelationalOpContext ctx) { }

	@Override public void enterExpressionList(DynamicRebecaCompleteParser.ExpressionListContext ctx) { }
	@Override public void exitExpressionList(DynamicRebecaCompleteParser.ExpressionListContext ctx) { }

	@Override public void enterUnaryExpression(DynamicRebecaCompleteParser.UnaryExpressionContext ctx) { }
	@Override public void exitUnaryExpression(DynamicRebecaCompleteParser.UnaryExpressionContext ctx) { }

	@Override public void enterType(DynamicRebecaCompleteParser.TypeContext ctx) { }
	@Override public void exitType(DynamicRebecaCompleteParser.TypeContext ctx) { }

	@Override public void enterTypeParametersDecl(DynamicRebecaCompleteParser.TypeParametersDeclContext ctx) { }
	@Override public void exitTypeParametersDecl(DynamicRebecaCompleteParser.TypeParametersDeclContext ctx) { }

	@Override public void enterPrimary(DynamicRebecaCompleteParser.PrimaryContext ctx) { }
	@Override public void exitPrimary(DynamicRebecaCompleteParser.PrimaryContext ctx) { }

	@Override public void enterShiftExpression(DynamicRebecaCompleteParser.ShiftExpressionContext ctx) { }
	@Override public void exitShiftExpression(DynamicRebecaCompleteParser.ShiftExpressionContext ctx) { }

	@Override public void enterImportDeclaration(DynamicRebecaCompleteParser.ImportDeclarationContext ctx) { }
	@Override public void exitImportDeclaration(DynamicRebecaCompleteParser.ImportDeclarationContext ctx) { }

	@Override public void enterRebecaCode(DynamicRebecaCompleteParser.RebecaCodeContext ctx) { }
	@Override public void exitRebecaCode(DynamicRebecaCompleteParser.RebecaCodeContext ctx) { }

	@Override public void enterFormalParametersDeclaration(DynamicRebecaCompleteParser.FormalParametersDeclarationContext ctx) { }
	@Override public void exitFormalParametersDeclaration(DynamicRebecaCompleteParser.FormalParametersDeclarationContext ctx) { }

	@Override public void enterPackageDeclaration(DynamicRebecaCompleteParser.PackageDeclarationContext ctx) { }
	@Override public void exitPackageDeclaration(DynamicRebecaCompleteParser.PackageDeclarationContext ctx) { }

	@Override public void enterConditionalAndExpression(DynamicRebecaCompleteParser.ConditionalAndExpressionContext ctx) { }
	@Override public void exitConditionalAndExpression(DynamicRebecaCompleteParser.ConditionalAndExpressionContext ctx) { }

	@Override public void enterVariableDeclarators(DynamicRebecaCompleteParser.VariableDeclaratorsContext ctx) { }
	@Override public void exitVariableDeclarators(DynamicRebecaCompleteParser.VariableDeclaratorsContext ctx) { }

	@Override public void enterAdditiveExpression(DynamicRebecaCompleteParser.AdditiveExpressionContext ctx) { }
	@Override public void exitAdditiveExpression(DynamicRebecaCompleteParser.AdditiveExpressionContext ctx) { }

	@Override public void enterSwitchBlock(DynamicRebecaCompleteParser.SwitchBlockContext ctx) { }
	@Override public void exitSwitchBlock(DynamicRebecaCompleteParser.SwitchBlockContext ctx) { }

	@Override public void enterStatement(DynamicRebecaCompleteParser.StatementContext ctx) { }
	@Override public void exitStatement(DynamicRebecaCompleteParser.StatementContext ctx) { }

	@Override public void enterExclusiveOrExpression(DynamicRebecaCompleteParser.ExclusiveOrExpressionContext ctx) { }
	@Override public void exitExclusiveOrExpression(DynamicRebecaCompleteParser.ExclusiveOrExpressionContext ctx) { }

	@Override public void enterInstanceOfExpression(DynamicRebecaCompleteParser.InstanceOfExpressionContext ctx) { }
	@Override public void exitInstanceOfExpression(DynamicRebecaCompleteParser.InstanceOfExpressionContext ctx) { }

	@Override public void enterParametrizedType(DynamicRebecaCompleteParser.ParametrizedTypeContext ctx) { }
	@Override public void exitParametrizedType(DynamicRebecaCompleteParser.ParametrizedTypeContext ctx) { }

	@Override public void enterMsgsrvDeclaration(DynamicRebecaCompleteParser.MsgsrvDeclarationContext ctx) { }
	@Override public void exitMsgsrvDeclaration(DynamicRebecaCompleteParser.MsgsrvDeclarationContext ctx) { }

	@Override public void enterMultiplicativeExpression(DynamicRebecaCompleteParser.MultiplicativeExpressionContext ctx) { }
	@Override public void exitMultiplicativeExpression(DynamicRebecaCompleteParser.MultiplicativeExpressionContext ctx) { }

	@Override public void enterAssignmentOperator(DynamicRebecaCompleteParser.AssignmentOperatorContext ctx) { }
	@Override public void exitAssignmentOperator(DynamicRebecaCompleteParser.AssignmentOperatorContext ctx) { }

	@Override public void enterShiftOp(DynamicRebecaCompleteParser.ShiftOpContext ctx) { }
	@Override public void exitShiftOp(DynamicRebecaCompleteParser.ShiftOpContext ctx) { }

	@Override public void enterStatementExpression(DynamicRebecaCompleteParser.StatementExpressionContext ctx) { }
	@Override public void exitStatementExpression(DynamicRebecaCompleteParser.StatementExpressionContext ctx) { }

	@Override public void enterUnaryExpressionNotPlusMinus(DynamicRebecaCompleteParser.UnaryExpressionNotPlusMinusContext ctx) { }
	@Override public void exitUnaryExpressionNotPlusMinus(DynamicRebecaCompleteParser.UnaryExpressionNotPlusMinusContext ctx) { }

	@Override public void enterVariableInitializer(DynamicRebecaCompleteParser.VariableInitializerContext ctx) { }
	@Override public void exitVariableInitializer(DynamicRebecaCompleteParser.VariableInitializerContext ctx) { }

	@Override public void enterBlock(DynamicRebecaCompleteParser.BlockContext ctx) { }
	@Override public void exitBlock(DynamicRebecaCompleteParser.BlockContext ctx) { }

	@Override public void enterFormalParameterDeclaration(DynamicRebecaCompleteParser.FormalParameterDeclarationContext ctx) { }
	@Override public void exitFormalParameterDeclaration(DynamicRebecaCompleteParser.FormalParameterDeclarationContext ctx) { }

	@Override public void enterMainRebecDefinition(DynamicRebecaCompleteParser.MainRebecDefinitionContext ctx) { }
	@Override public void exitMainRebecDefinition(DynamicRebecaCompleteParser.MainRebecDefinitionContext ctx) { }

	@Override public void enterConditionalExpression(DynamicRebecaCompleteParser.ConditionalExpressionContext ctx) { }
	@Override public void exitConditionalExpression(DynamicRebecaCompleteParser.ConditionalExpressionContext ctx) { }

	@Override public void enterAndExpression(DynamicRebecaCompleteParser.AndExpressionContext ctx) { }
	@Override public void exitAndExpression(DynamicRebecaCompleteParser.AndExpressionContext ctx) { }

	@Override public void enterRebecaModel(DynamicRebecaCompleteParser.RebecaModelContext ctx) { }
	@Override public void exitRebecaModel(DynamicRebecaCompleteParser.RebecaModelContext ctx) { }

	@Override public void enterFieldDeclaration(DynamicRebecaCompleteParser.FieldDeclarationContext ctx) { }
	@Override public void exitFieldDeclaration(DynamicRebecaCompleteParser.FieldDeclarationContext ctx) { }

	@Override public void enterRelationalExpression(DynamicRebecaCompleteParser.RelationalExpressionContext ctx) { }
	@Override public void exitRelationalExpression(DynamicRebecaCompleteParser.RelationalExpressionContext ctx) { }

	@Override public void enterDimensions(DynamicRebecaCompleteParser.DimensionsContext ctx) { }
	@Override public void exitDimensions(DynamicRebecaCompleteParser.DimensionsContext ctx) { }

	@Override public void enterConditionalOrExpression(DynamicRebecaCompleteParser.ConditionalOrExpressionContext ctx) { }
	@Override public void exitConditionalOrExpression(DynamicRebecaCompleteParser.ConditionalOrExpressionContext ctx) { }

	@Override public void enterSynchMethodDeclaration(DynamicRebecaCompleteParser.SynchMethodDeclarationContext ctx) { }
	@Override public void exitSynchMethodDeclaration(DynamicRebecaCompleteParser.SynchMethodDeclarationContext ctx) { }

	@Override public void enterEnvironmentVariables(DynamicRebecaCompleteParser.EnvironmentVariablesContext ctx) { }
	@Override public void exitEnvironmentVariables(DynamicRebecaCompleteParser.EnvironmentVariablesContext ctx) { }

	@Override public void enterMethodDeclaration(DynamicRebecaCompleteParser.MethodDeclarationContext ctx) { }
	@Override public void exitMethodDeclaration(DynamicRebecaCompleteParser.MethodDeclarationContext ctx) { }

	@Override public void enterConstructorDeclaration(DynamicRebecaCompleteParser.ConstructorDeclarationContext ctx) { }
	@Override public void exitConstructorDeclaration(DynamicRebecaCompleteParser.ConstructorDeclarationContext ctx) { }

	@Override public void enterReactiveClassDeclaration(DynamicRebecaCompleteParser.ReactiveClassDeclarationContext ctx) { }
	@Override public void exitReactiveClassDeclaration(DynamicRebecaCompleteParser.ReactiveClassDeclarationContext ctx) { }

	@Override public void enterRecordDeclaration(DynamicRebecaCompleteParser.RecordDeclarationContext ctx) { }
	@Override public void exitRecordDeclaration(DynamicRebecaCompleteParser.RecordDeclarationContext ctx) { }

	@Override public void enterInclusiveOrExpression(DynamicRebecaCompleteParser.InclusiveOrExpressionContext ctx) { }
	@Override public void exitInclusiveOrExpression(DynamicRebecaCompleteParser.InclusiveOrExpressionContext ctx) { }

	@Override public void enterMainDeclaration(DynamicRebecaCompleteParser.MainDeclarationContext ctx) { }
	@Override public void exitMainDeclaration(DynamicRebecaCompleteParser.MainDeclarationContext ctx) { }

	@Override public void enterEqualityExpression(DynamicRebecaCompleteParser.EqualityExpressionContext ctx) { }
	@Override public void exitEqualityExpression(DynamicRebecaCompleteParser.EqualityExpressionContext ctx) { }

	@Override public void enterArrayInitializer(DynamicRebecaCompleteParser.ArrayInitializerContext ctx) { }
	@Override public void exitArrayInitializer(DynamicRebecaCompleteParser.ArrayInitializerContext ctx) { }

	@Override public void enterFormalParameters(DynamicRebecaCompleteParser.FormalParametersContext ctx) { }
	@Override public void exitFormalParameters(DynamicRebecaCompleteParser.FormalParametersContext ctx) { }

	@Override public void enterCastExpression(DynamicRebecaCompleteParser.CastExpressionContext ctx) { }
	@Override public void exitCastExpression(DynamicRebecaCompleteParser.CastExpressionContext ctx) { }

	@Override public void enterForInit(DynamicRebecaCompleteParser.ForInitContext ctx) { }
	@Override public void exitForInit(DynamicRebecaCompleteParser.ForInitContext ctx) { }

	@Override public void enterLiteral(DynamicRebecaCompleteParser.LiteralContext ctx) { }
	@Override public void exitLiteral(DynamicRebecaCompleteParser.LiteralContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	@Override public void visitTerminal(TerminalNode node) { }
	@Override public void visitErrorNode(ErrorNode node) { }
}