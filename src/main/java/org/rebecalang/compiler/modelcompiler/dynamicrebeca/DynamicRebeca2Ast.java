package org.rebecalang.compiler.modelcompiler.dynamicrebeca;

import java.io.FileInputStream;
import java.io.IOException;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.RebecaModel;
import org.rebecalang.compiler.modelcompiler.dynamicrebeca.compiler.DynamicRebecaCompleteLexer;
import org.rebecalang.compiler.modelcompiler.dynamicrebeca.compiler.DynamicRebecaCompleteParser;

public class DynamicRebeca2Ast {
	public static RebecaModel fromRebecaFileAt(String pathToRebecaFile) throws IOException{
		DynamicRebecaCompleteLexer lexer = new DynamicRebecaCompleteLexer(new ANTLRInputStream(new FileInputStream(pathToRebecaFile)));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		DynamicRebecaCompleteParser parser = new DynamicRebecaCompleteParser(tokens);
		return parser.rebecaModel().r;
	}
}
