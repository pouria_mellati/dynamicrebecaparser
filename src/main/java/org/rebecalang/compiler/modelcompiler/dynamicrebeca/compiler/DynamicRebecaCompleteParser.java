// Generated from DynamicRebecaComplete.g4 by ANTLR 4.0

	package org.rebecalang.compiler.modelcompiler.dynamicrebeca.compiler;
	import org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel.*;
	import org.rebecalang.compiler.modelcompiler.dynamicrebeca.extratypes.*;
	import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.*;
	
	import java.util.*;
	import org.antlr.runtime.BitSet;
	import org.rebecalang.compiler.utils.TypesUtilities;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class DynamicRebecaCompleteParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LIST=1, NEW=2, INTLITERAL=3, FLOATLITERAL=4, DOUBLELITERAL=5, CHARLITERAL=6, 
		STRINGLITERAL=7, WS=8, COMMENT=9, LINE_COMMENT=10, BREAK=11, CASE=12, 
		CLASS=13, CONTINUE=14, DEFAULT=15, ELSE=16, FOR=17, GOTO=18, IF=19, IMPORT=20, 
		PACKAGE=21, PRIVATE=22, PROTECTED=23, PUBLIC=24, RECORD=25, RETURN=26, 
		SWITCH=27, WHILE=28, ENV=29, REACTIVECLASS=30, MSGSRV=31, MAIN=32, STATEVARS=33, 
		KNOWNREBECS=34, THIS=35, TRUE=36, FALSE=37, NULL=38, LPAREN=39, RPAREN=40, 
		LBRACE=41, RBRACE=42, LBRACKET=43, RBRACKET=44, SEMI=45, COMMA=46, DOT=47, 
		EQ=48, BANG=49, TILDA=50, QUES=51, COLON=52, EQEQ=53, AMPAMP=54, BARBAR=55, 
		PLUSPLUS=56, SUBSUB=57, SUPER=58, PLUS=59, SUB=60, STAR=61, SLASH=62, 
		AMP=63, BAR=64, CARET=65, PERCENT=66, PLUSEQ=67, SUBEQ=68, STAREQ=69, 
		SLASHEQ=70, AMPEQ=71, BAREQ=72, CARETEQ=73, TILDAEQ=74, PERCENTEQ=75, 
		MONKEYS_AT=76, BANGEQ=77, GT=78, LT=79, LTLT=80, LTLTEQ=81, GTGT=82, GTGTEQ=83, 
		IDENTIFIER=84;
	public static final String[] tokenNames = {
		"<INVALID>", "'list'", "'new'", "INTLITERAL", "FLOATLITERAL", "DOUBLELITERAL", 
		"CHARLITERAL", "STRINGLITERAL", "WS", "COMMENT", "LINE_COMMENT", "'break'", 
		"'case'", "'class'", "'continue'", "'default'", "'else'", "'for'", "'goto'", 
		"'if'", "'import'", "'package'", "'private'", "'protected'", "'public'", 
		"'record'", "'return'", "'switch'", "'while'", "'env'", "'reactiveclass'", 
		"'msgsrv'", "'main'", "'statevars'", "'knownrebecs'", "'this'", "'true'", 
		"'false'", "'null'", "'('", "')'", "'{'", "'}'", "'['", "']'", "';'", 
		"','", "'.'", "'='", "'!'", "'~'", "'?'", "':'", "'=='", "'&&'", "'||'", 
		"'++'", "'--'", "'super'", "'+'", "'-'", "'*'", "'/'", "'&'", "'|'", "'^'", 
		"'%'", "'+='", "'-='", "'*='", "'/='", "'&='", "'|='", "'^='", "'~='", 
		"'%='", "'@'", "'!='", "'>'", "'<'", "'<<'", "'<<='", "'>>'", "'>>='", 
		"IDENTIFIER"
	};
	public static final int
		RULE_statement = 0, RULE_type = 1, RULE_parametrizedType = 2, RULE_typeParametersDecl = 3, 
		RULE_literal = 4, RULE_unaryExpressionNotPlusMinus = 5, RULE_mainDeclaration = 6, 
		RULE_rebecaModel = 7, RULE_packageDeclaration = 8, RULE_importDeclaration = 9, 
		RULE_rebecaCode = 10, RULE_recordDeclaration = 11, RULE_mainRebecDefinition = 12, 
		RULE_environmentVariables = 13, RULE_fieldDeclaration = 14, RULE_variableDeclarators = 15, 
		RULE_variableDeclarator = 16, RULE_variableInitializer = 17, RULE_arrayInitializer = 18, 
		RULE_reactiveClassDeclaration = 19, RULE_methodDeclaration = 20, RULE_constructorDeclaration = 21, 
		RULE_msgsrvDeclaration = 22, RULE_synchMethodDeclaration = 23, RULE_formalParameters = 24, 
		RULE_formalParametersDeclaration = 25, RULE_formalParameterDeclaration = 26, 
		RULE_block = 27, RULE_forInit = 28, RULE_switchBlock = 29, RULE_statementExpression = 30, 
		RULE_dimensions = 31, RULE_expression = 32, RULE_assignmentOperator = 33, 
		RULE_conditionalExpression = 34, RULE_conditionalOrExpression = 35, RULE_conditionalAndExpression = 36, 
		RULE_inclusiveOrExpression = 37, RULE_exclusiveOrExpression = 38, RULE_andExpression = 39, 
		RULE_equalityExpression = 40, RULE_instanceOfExpression = 41, RULE_relationalExpression = 42, 
		RULE_relationalOp = 43, RULE_shiftExpression = 44, RULE_shiftOp = 45, 
		RULE_additiveExpression = 46, RULE_multiplicativeExpression = 47, RULE_unaryExpression = 48, 
		RULE_castExpression = 49, RULE_primary = 50, RULE_expressionList = 51;
	public static final String[] ruleNames = {
		"statement", "type", "parametrizedType", "typeParametersDecl", "literal", 
		"unaryExpressionNotPlusMinus", "mainDeclaration", "rebecaModel", "packageDeclaration", 
		"importDeclaration", "rebecaCode", "recordDeclaration", "mainRebecDefinition", 
		"environmentVariables", "fieldDeclaration", "variableDeclarators", "variableDeclarator", 
		"variableInitializer", "arrayInitializer", "reactiveClassDeclaration", 
		"methodDeclaration", "constructorDeclaration", "msgsrvDeclaration", "synchMethodDeclaration", 
		"formalParameters", "formalParametersDeclaration", "formalParameterDeclaration", 
		"block", "forInit", "switchBlock", "statementExpression", "dimensions", 
		"expression", "assignmentOperator", "conditionalExpression", "conditionalOrExpression", 
		"conditionalAndExpression", "inclusiveOrExpression", "exclusiveOrExpression", 
		"andExpression", "equalityExpression", "instanceOfExpression", "relationalExpression", 
		"relationalOp", "shiftExpression", "shiftOp", "additiveExpression", "multiplicativeExpression", 
		"unaryExpression", "castExpression", "primary", "expressionList"
	};

	@Override
	public String getGrammarFileName() { return "DynamicRebecaComplete.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public DynamicRebecaCompleteParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StatementContext extends ParserRuleContext {
		public Statement s;
		public ExpressionContext recipient;
		public Token BANG;
		public Token msgName;
		public ExpressionListContext params;
		public FieldDeclarationContext fd;
		public BlockContext b;
		public Token IF;
		public ExpressionContext e;
		public StatementContext st;
		public StatementContext est;
		public Token WHILE;
		public Token FOR;
		public ForInitContext fi;
		public ExpressionListContext el;
		public TypeContext t;
		public Token varName;
		public ExpressionContext range;
		public Token SWITCH;
		public SwitchBlockContext sb;
		public Token RETURN;
		public Token BREAK;
		public Token CONTINUE;
		public StatementExpressionContext se;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(DynamicRebecaCompleteParser.RBRACE, 0); }
		public StatementExpressionContext statementExpression() {
			return getRuleContext(StatementExpressionContext.class,0);
		}
		public TerminalNode WHILE() { return getToken(DynamicRebecaCompleteParser.WHILE, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode SWITCH() { return getToken(DynamicRebecaCompleteParser.SWITCH, 0); }
		public TerminalNode ELSE() { return getToken(DynamicRebecaCompleteParser.ELSE, 0); }
		public TerminalNode LBRACE() { return getToken(DynamicRebecaCompleteParser.LBRACE, 0); }
		public TerminalNode FOR() { return getToken(DynamicRebecaCompleteParser.FOR, 0); }
		public TerminalNode BANG() { return getToken(DynamicRebecaCompleteParser.BANG, 0); }
		public FieldDeclarationContext fieldDeclaration() {
			return getRuleContext(FieldDeclarationContext.class,0);
		}
		public List<TerminalNode> SEMI() { return getTokens(DynamicRebecaCompleteParser.SEMI); }
		public TerminalNode BREAK() { return getToken(DynamicRebecaCompleteParser.BREAK, 0); }
		public TerminalNode LPAREN() { return getToken(DynamicRebecaCompleteParser.LPAREN, 0); }
		public TerminalNode IF() { return getToken(DynamicRebecaCompleteParser.IF, 0); }
		public TerminalNode COLON() { return getToken(DynamicRebecaCompleteParser.COLON, 0); }
		public TerminalNode RPAREN() { return getToken(DynamicRebecaCompleteParser.RPAREN, 0); }
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public TerminalNode CONTINUE() { return getToken(DynamicRebecaCompleteParser.CONTINUE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(DynamicRebecaCompleteParser.IDENTIFIER, 0); }
		public SwitchBlockContext switchBlock() {
			return getRuleContext(SwitchBlockContext.class,0);
		}
		public TerminalNode RETURN() { return getToken(DynamicRebecaCompleteParser.RETURN, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public ForInitContext forInit() {
			return getRuleContext(ForInitContext.class,0);
		}
		public TerminalNode SEMI(int i) {
			return getToken(DynamicRebecaCompleteParser.SEMI, i);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_statement);
		int _la;
		try {
			setState(210);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(104); ((StatementContext)_localctx).recipient = expression();
				setState(105); ((StatementContext)_localctx).BANG = match(BANG);
				MsgSendStatement mss = new MsgSendStatement(); ((StatementContext)_localctx).s =  mss; mss.setLineNumber(((StatementContext)_localctx).BANG.getLine()); mss.setCharacter(((StatementContext)_localctx).BANG.getCharPositionInLine());
				setState(107); ((StatementContext)_localctx).msgName = match(IDENTIFIER);
				setState(115);
				_la = _input.LA(1);
				if (_la==LPAREN) {
					{
					setState(108); match(LPAREN);
					setState(112);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
						{
						setState(109); ((StatementContext)_localctx).params = expressionList();
						mss.getParameters().addAll(((StatementContext)_localctx).params.el);
						}
					}

					setState(114); match(RPAREN);
					}
				}

				setState(117); match(SEMI);
				mss.setRecipient(((StatementContext)_localctx).recipient.e); mss.setMessageName((((StatementContext)_localctx).msgName!=null?((StatementContext)_localctx).msgName.getText():null));
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(120); ((StatementContext)_localctx).fd = fieldDeclaration();
				((StatementContext)_localctx).s =  ((StatementContext)_localctx).fd.fd;
				setState(122); match(SEMI);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(124); ((StatementContext)_localctx).b = block();
				((StatementContext)_localctx).s =  ((StatementContext)_localctx).b.bs;
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(127); ((StatementContext)_localctx).IF = match(IF);
				((StatementContext)_localctx).s =  new ConditionalStatement(); _localctx.s.setLineNumber(((StatementContext)_localctx).IF.getLine());_localctx.s.setCharacter(((StatementContext)_localctx).IF.getCharPositionInLine());
				setState(129); match(LPAREN);
				setState(130); ((StatementContext)_localctx).e = expression();
				setState(131); match(RPAREN);
				setState(132); ((StatementContext)_localctx).st = statement();
				((ConditionalStatement)_localctx.s).setCondition(((StatementContext)_localctx).e.e); ((ConditionalStatement)_localctx.s).setStatement(((StatementContext)_localctx).st.s);
				setState(138);
				switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
				case 1:
					{
					setState(134); match(ELSE);
					setState(135); ((StatementContext)_localctx).est = statement();
					((ConditionalStatement)_localctx.s).setElseStatement(((StatementContext)_localctx).est.s);
					}
					break;
				}
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(140); ((StatementContext)_localctx).WHILE = match(WHILE);
				((StatementContext)_localctx).s =  new WhileStatement(); _localctx.s.setLineNumber(((StatementContext)_localctx).WHILE.getLine());_localctx.s.setCharacter(((StatementContext)_localctx).WHILE.getCharPositionInLine());
				setState(142); match(LPAREN);
				setState(143); ((StatementContext)_localctx).e = expression();
				setState(144); match(RPAREN);
				setState(145); ((StatementContext)_localctx).st = statement();
				((WhileStatement)_localctx.s).setCondition(((StatementContext)_localctx).e.e); ((WhileStatement)_localctx.s).setStatement(((StatementContext)_localctx).st.s);
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(148); ((StatementContext)_localctx).FOR = match(FOR);
				((StatementContext)_localctx).s =  new ForStatement(); _localctx.s.setLineNumber(((StatementContext)_localctx).FOR.getLine());_localctx.s.setCharacter(((StatementContext)_localctx).FOR.getCharPositionInLine());
				setState(150); match(LPAREN);
				setState(154);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
					{
					setState(151); ((StatementContext)_localctx).fi = forInit();
					((ForStatement)_localctx.s).setForInitializer(((StatementContext)_localctx).fi.fi);
					}
				}

				setState(156); match(SEMI);
				setState(160);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
					{
					setState(157); ((StatementContext)_localctx).e = expression();
					((ForStatement)_localctx.s).setCondition(((StatementContext)_localctx).e.e);
					}
				}

				setState(162); match(SEMI);
				setState(166);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
					{
					setState(163); ((StatementContext)_localctx).el = expressionList();
					((ForStatement)_localctx.s).getForIncrement().addAll(((StatementContext)_localctx).el.el); 
					}
				}

				setState(168); match(RPAREN);
				setState(169); ((StatementContext)_localctx).st = statement();
				((ForStatement)_localctx.s).setStatement(((StatementContext)_localctx).st.s);
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(172); ((StatementContext)_localctx).FOR = match(FOR);
				((StatementContext)_localctx).s =  new ForeachStatement(); _localctx.s.setLineNumber(((StatementContext)_localctx).FOR.getLine()); _localctx.s.setCharacter(((StatementContext)_localctx).FOR.getCharPositionInLine());
				setState(174); match(LPAREN);
				setState(175); ((StatementContext)_localctx).t = type();
				setState(176); ((StatementContext)_localctx).varName = match(IDENTIFIER);
				setState(177); match(COLON);
				setState(178); ((StatementContext)_localctx).range = expression();
				setState(179); match(RPAREN);
				setState(180); ((StatementContext)_localctx).st = statement();
				ForeachStatement fe = (ForeachStatement)_localctx.s;
				    	fe.setVarType(((StatementContext)_localctx).t.t); fe.setVarName((((StatementContext)_localctx).varName!=null?((StatementContext)_localctx).varName.getText():null)); fe.setRange(((StatementContext)_localctx).range.e); fe.setStatement(((StatementContext)_localctx).st.s);
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(183); ((StatementContext)_localctx).SWITCH = match(SWITCH);
				setState(184); match(LPAREN);
				setState(185); ((StatementContext)_localctx).e = expression();
				setState(186); match(RPAREN);
				setState(187); match(LBRACE);
				setState(188); ((StatementContext)_localctx).sb = switchBlock();
				setState(189); match(RBRACE);
				((StatementContext)_localctx).s =  ((StatementContext)_localctx).sb.ss; ((SwitchStatement)_localctx.s).setExpression(((StatementContext)_localctx).e.e); _localctx.s.setLineNumber(((StatementContext)_localctx).SWITCH.getLine()); _localctx.s.setCharacter(((StatementContext)_localctx).SWITCH.getCharPositionInLine());
				}
				break;

			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(192); ((StatementContext)_localctx).RETURN = match(RETURN);
				setState(194);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
					{
					setState(193); ((StatementContext)_localctx).e = expression();
					}
				}

				setState(196); match(SEMI);
				((StatementContext)_localctx).s =  new ReturnStatement(); ((ReturnStatement)_localctx.s).setExpression(((StatementContext)_localctx).e.e); _localctx.s.setLineNumber(((StatementContext)_localctx).RETURN.getLine());_localctx.s.setCharacter(((StatementContext)_localctx).RETURN.getCharPositionInLine());
				}
				break;

			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(198); ((StatementContext)_localctx).BREAK = match(BREAK);
				setState(199); match(SEMI);
				((StatementContext)_localctx).s =  new BreakStatement(); _localctx.s.setLineNumber(((StatementContext)_localctx).BREAK.getLine());_localctx.s.setCharacter(((StatementContext)_localctx).BREAK.getCharPositionInLine());
				}
				break;

			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(201); ((StatementContext)_localctx).CONTINUE = match(CONTINUE);
				setState(202); match(SEMI);
				((StatementContext)_localctx).s =  new BreakStatement(); _localctx.s.setLineNumber(((StatementContext)_localctx).CONTINUE.getLine());_localctx.s.setCharacter(((StatementContext)_localctx).CONTINUE.getCharPositionInLine());
				}
				break;

			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(204); match(SEMI);
				((StatementContext)_localctx).s =  new Statement();
				}
				break;

			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(206); ((StatementContext)_localctx).se = statementExpression();
				((StatementContext)_localctx).s =  ((StatementContext)_localctx).se.se;
				setState(208); match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Type t;
		public Token typeName;
		public DimensionsContext ds;
		public ParametrizedTypeContext pt;
		public ParametrizedTypeContext parametrizedType() {
			return getRuleContext(ParametrizedTypeContext.class,0);
		}
		public DimensionsContext dimensions() {
			return getRuleContext(DimensionsContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(DynamicRebecaCompleteParser.IDENTIFIER, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_type);
		int _la;
		try {
			setState(224);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				((TypeContext)_localctx).t =  new PrimitiveType();
				setState(213); ((TypeContext)_localctx).typeName = match(IDENTIFIER);
				((TypeContext)_localctx).t =  new OrdinaryPrimitiveType(); ((OrdinaryPrimitiveType)_localctx.t).setName((((TypeContext)_localctx).typeName!=null?((TypeContext)_localctx).typeName.getText():null));
					 	 _localctx.t.setLineNumber(((TypeContext)_localctx).typeName.getLine());_localctx.t.setCharacter(((TypeContext)_localctx).typeName.getCharPositionInLine());
				setState(218);
				_la = _input.LA(1);
				if (_la==LBRACKET) {
					{
					setState(215); ((TypeContext)_localctx).ds = dimensions();
					PrimitiveType newpt = (PrimitiveType)_localctx.t; ((TypeContext)_localctx).t =  new ArrayType(); ((ArrayType)_localctx.t).setPrimitiveType(newpt); ((ArrayType)_localctx.t).getDimensions().addAll(((TypeContext)_localctx).ds.ds);
					}
				}

				}
				break;
			case LIST:
				enterOuterAlt(_localctx, 2);
				{
				((TypeContext)_localctx).t =  new ParametrizedType();
				setState(221); ((TypeContext)_localctx).pt = parametrizedType();
				((TypeContext)_localctx).t =  ((TypeContext)_localctx).pt.t;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametrizedTypeContext extends ParserRuleContext {
		public ParametrizedType t;
		public Token parametrizedTypeName;
		public TypeParametersDeclContext typeParams;
		public TypeParametersDeclContext typeParametersDecl() {
			return getRuleContext(TypeParametersDeclContext.class,0);
		}
		public TerminalNode LIST() { return getToken(DynamicRebecaCompleteParser.LIST, 0); }
		public ParametrizedTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parametrizedType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterParametrizedType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitParametrizedType(this);
		}
	}

	public final ParametrizedTypeContext parametrizedType() throws RecognitionException {
		ParametrizedTypeContext _localctx = new ParametrizedTypeContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_parametrizedType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			((ParametrizedTypeContext)_localctx).t =  new ParametrizedType();
			setState(227); ((ParametrizedTypeContext)_localctx).parametrizedTypeName = match(LIST);
			setState(228); ((ParametrizedTypeContext)_localctx).typeParams = typeParametersDecl();
			ParametrizedType pt = (ParametrizedType)_localctx.t; pt.setName((((ParametrizedTypeContext)_localctx).parametrizedTypeName!=null?((ParametrizedTypeContext)_localctx).parametrizedTypeName.getText():null)); pt.getParameters().addAll(((ParametrizedTypeContext)_localctx).typeParams.params);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeParametersDeclContext extends ParserRuleContext {
		public List<Type> params;
		public TypeContext firstTypeParam;
		public TypeContext typeParam;
		public TerminalNode GT() { return getToken(DynamicRebecaCompleteParser.GT, 0); }
		public TerminalNode LT() { return getToken(DynamicRebecaCompleteParser.LT, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(DynamicRebecaCompleteParser.COMMA, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(DynamicRebecaCompleteParser.COMMA); }
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeParametersDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeParametersDecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterTypeParametersDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitTypeParametersDecl(this);
		}
	}

	public final TypeParametersDeclContext typeParametersDecl() throws RecognitionException {
		TypeParametersDeclContext _localctx = new TypeParametersDeclContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_typeParametersDecl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((TypeParametersDeclContext)_localctx).params =  new LinkedList<Type>();
			setState(232); match(LT);
			setState(236);
			_la = _input.LA(1);
			if (_la==LIST || _la==IDENTIFIER) {
				{
				setState(233); ((TypeParametersDeclContext)_localctx).firstTypeParam = type();
				_localctx.params.add(((TypeParametersDeclContext)_localctx).firstTypeParam.t);
				}
			}

			setState(244);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(238); match(COMMA);
				setState(239); ((TypeParametersDeclContext)_localctx).typeParam = type();
				_localctx.params.add(((TypeParametersDeclContext)_localctx).typeParam.t);
				}
				}
				setState(246);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(247); match(GT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public Literal l;
		public Token INTLITERAL;
		public Token FLOATLITERAL;
		public Token DOUBLELITERAL;
		public Token CHARLITERAL;
		public Token STRINGLITERAL;
		public Token TRUE;
		public Token FALSE;
		public Token NULL;
		public ParametrizedTypeContext collType;
		public ExpressionListContext elems;
		public TerminalNode RPAREN() { return getToken(DynamicRebecaCompleteParser.RPAREN, 0); }
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public ParametrizedTypeContext parametrizedType() {
			return getRuleContext(ParametrizedTypeContext.class,0);
		}
		public TerminalNode INTLITERAL() { return getToken(DynamicRebecaCompleteParser.INTLITERAL, 0); }
		public TerminalNode FALSE() { return getToken(DynamicRebecaCompleteParser.FALSE, 0); }
		public TerminalNode TRUE() { return getToken(DynamicRebecaCompleteParser.TRUE, 0); }
		public TerminalNode FLOATLITERAL() { return getToken(DynamicRebecaCompleteParser.FLOATLITERAL, 0); }
		public TerminalNode STRINGLITERAL() { return getToken(DynamicRebecaCompleteParser.STRINGLITERAL, 0); }
		public TerminalNode LPAREN() { return getToken(DynamicRebecaCompleteParser.LPAREN, 0); }
		public TerminalNode NULL() { return getToken(DynamicRebecaCompleteParser.NULL, 0); }
		public TerminalNode CHARLITERAL() { return getToken(DynamicRebecaCompleteParser.CHARLITERAL, 0); }
		public TerminalNode DOUBLELITERAL() { return getToken(DynamicRebecaCompleteParser.DOUBLELITERAL, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitLiteral(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_literal);
		int _la;
		try {
			setState(276);
			switch (_input.LA(1)) {
			case INTLITERAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(249); ((LiteralContext)_localctx).INTLITERAL = match(INTLITERAL);
				((LiteralContext)_localctx).l =  new Literal();_localctx.l.setLiteralValue((((LiteralContext)_localctx).INTLITERAL!=null?((LiteralContext)_localctx).INTLITERAL.getText():null));
				    		_localctx.l.setType(TypesUtilities.INT_TYPE);
				    		_localctx.l.setLineNumber(((LiteralContext)_localctx).INTLITERAL.getLine());_localctx.l.setCharacter(((LiteralContext)_localctx).INTLITERAL.getCharPositionInLine());
				}
				break;
			case FLOATLITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(251); ((LiteralContext)_localctx).FLOATLITERAL = match(FLOATLITERAL);
				((LiteralContext)_localctx).l =  new Literal();_localctx.l.setLiteralValue((((LiteralContext)_localctx).FLOATLITERAL!=null?((LiteralContext)_localctx).FLOATLITERAL.getText():null));
				    	_localctx.l.setType(TypesUtilities.FLOAT_TYPE);
				    	_localctx.l.setLineNumber(((LiteralContext)_localctx).FLOATLITERAL.getLine());_localctx.l.setCharacter(((LiteralContext)_localctx).FLOATLITERAL.getCharPositionInLine());
				}
				break;
			case DOUBLELITERAL:
				enterOuterAlt(_localctx, 3);
				{
				setState(253); ((LiteralContext)_localctx).DOUBLELITERAL = match(DOUBLELITERAL);
				((LiteralContext)_localctx).l =  new Literal();_localctx.l.setLiteralValue((((LiteralContext)_localctx).DOUBLELITERAL!=null?((LiteralContext)_localctx).DOUBLELITERAL.getText():null));
				    	_localctx.l.setType(TypesUtilities.DOUBLE_TYPE);
				    	_localctx.l.setLineNumber(((LiteralContext)_localctx).DOUBLELITERAL.getLine());_localctx.l.setCharacter(((LiteralContext)_localctx).DOUBLELITERAL.getCharPositionInLine());
				}
				break;
			case CHARLITERAL:
				enterOuterAlt(_localctx, 4);
				{
				setState(255); ((LiteralContext)_localctx).CHARLITERAL = match(CHARLITERAL);
				((LiteralContext)_localctx).l =  new Literal();_localctx.l.setLiteralValue((((LiteralContext)_localctx).CHARLITERAL!=null?((LiteralContext)_localctx).CHARLITERAL.getText():null));
				    	_localctx.l.setType(TypesUtilities.CHAR_TYPE);
				    	_localctx.l.setLineNumber(((LiteralContext)_localctx).CHARLITERAL.getLine());_localctx.l.setCharacter(((LiteralContext)_localctx).CHARLITERAL.getCharPositionInLine());
				}
				break;
			case STRINGLITERAL:
				enterOuterAlt(_localctx, 5);
				{
				setState(257); ((LiteralContext)_localctx).STRINGLITERAL = match(STRINGLITERAL);
				((LiteralContext)_localctx).l =  new Literal();_localctx.l.setLiteralValue((((LiteralContext)_localctx).STRINGLITERAL!=null?((LiteralContext)_localctx).STRINGLITERAL.getText():null));
				    	_localctx.l.setType(TypesUtilities.STRING_TYPE);
				    	_localctx.l.setLineNumber(((LiteralContext)_localctx).STRINGLITERAL.getLine());_localctx.l.setCharacter(((LiteralContext)_localctx).STRINGLITERAL.getCharPositionInLine());
				}
				break;
			case TRUE:
				enterOuterAlt(_localctx, 6);
				{
				setState(259); ((LiteralContext)_localctx).TRUE = match(TRUE);
				((LiteralContext)_localctx).l =  new Literal();_localctx.l.setLiteralValue("true");
				    	_localctx.l.setType(TypesUtilities.BOOLEAN_TYPE);
				    	_localctx.l.setLineNumber(((LiteralContext)_localctx).TRUE.getLine());_localctx.l.setCharacter(((LiteralContext)_localctx).TRUE.getCharPositionInLine());
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 7);
				{
				setState(261); ((LiteralContext)_localctx).FALSE = match(FALSE);
				((LiteralContext)_localctx).l =  new Literal();_localctx.l.setLiteralValue("false");
				    	_localctx.l.setType(TypesUtilities.BOOLEAN_TYPE);
				    	_localctx.l.setLineNumber(((LiteralContext)_localctx).FALSE.getLine());_localctx.l.setCharacter(((LiteralContext)_localctx).FALSE.getCharPositionInLine());
				}
				break;
			case NULL:
				enterOuterAlt(_localctx, 8);
				{
				setState(263); ((LiteralContext)_localctx).NULL = match(NULL);
				((LiteralContext)_localctx).l =  new Literal();_localctx.l.setLiteralValue("null");
				    	_localctx.l.setType(TypesUtilities.NULL_TYPE);
				    	_localctx.l.setLineNumber(((LiteralContext)_localctx).NULL.getLine());_localctx.l.setCharacter(((LiteralContext)_localctx).NULL.getCharPositionInLine());
				}
				break;
			case LIST:
				enterOuterAlt(_localctx, 9);
				{
				CollectionLiteral cl = new CollectionLiteral(); ((LiteralContext)_localctx).l =  cl;
				setState(266); ((LiteralContext)_localctx).collType = parametrizedType();
				cl.setType(((LiteralContext)_localctx).collType.t);
				setState(268); match(LPAREN);
				setState(272);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
					{
					setState(269); ((LiteralContext)_localctx).elems = expressionList();
					cl.getElemExprs().addAll(((LiteralContext)_localctx).elems.el);
					}
				}

				setState(274); match(RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryExpressionNotPlusMinusContext extends ParserRuleContext {
		public Expression e;
		public Token NEW;
		public TypeContext t;
		public ExpressionListContext params;
		public Token TILDA;
		public UnaryExpressionContext e1;
		public Token BANG;
		public CastExpressionContext ec;
		public ExpressionContext ep;
		public PrimaryContext p;
		public LiteralContext l;
		public Token QUES;
		public ExpressionListContext el;
		public Token DOT;
		public PrimaryContext p2;
		public Token PLUSPLUS;
		public Token SUBSUB;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(DynamicRebecaCompleteParser.RPAREN, 0); }
		public TerminalNode DOT(int i) {
			return getToken(DynamicRebecaCompleteParser.DOT, i);
		}
		public TerminalNode TILDA() { return getToken(DynamicRebecaCompleteParser.TILDA, 0); }
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public TerminalNode SUBSUB() { return getToken(DynamicRebecaCompleteParser.SUBSUB, 0); }
		public TerminalNode PLUSPLUS() { return getToken(DynamicRebecaCompleteParser.PLUSPLUS, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode NEW() { return getToken(DynamicRebecaCompleteParser.NEW, 0); }
		public TerminalNode BANG() { return getToken(DynamicRebecaCompleteParser.BANG, 0); }
		public PrimaryContext primary(int i) {
			return getRuleContext(PrimaryContext.class,i);
		}
		public CastExpressionContext castExpression() {
			return getRuleContext(CastExpressionContext.class,0);
		}
		public List<PrimaryContext> primary() {
			return getRuleContexts(PrimaryContext.class);
		}
		public List<TerminalNode> DOT() { return getTokens(DynamicRebecaCompleteParser.DOT); }
		public TerminalNode LPAREN() { return getToken(DynamicRebecaCompleteParser.LPAREN, 0); }
		public TerminalNode QUES() { return getToken(DynamicRebecaCompleteParser.QUES, 0); }
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public UnaryExpressionNotPlusMinusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryExpressionNotPlusMinus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterUnaryExpressionNotPlusMinus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitUnaryExpressionNotPlusMinus(this);
		}
	}

	public final UnaryExpressionNotPlusMinusContext unaryExpressionNotPlusMinus() throws RecognitionException {
		UnaryExpressionNotPlusMinusContext _localctx = new UnaryExpressionNotPlusMinusContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_unaryExpressionNotPlusMinus);
		int _la;
		try {
			int _alt;
			setState(334);
			switch (_input.LA(1)) {
			case NEW:
				enterOuterAlt(_localctx, 1);
				{
				setState(278); ((UnaryExpressionNotPlusMinusContext)_localctx).NEW = match(NEW);
				setState(279); ((UnaryExpressionNotPlusMinusContext)_localctx).t = type();
				NewExpression ne = new NewExpression(); ((UnaryExpressionNotPlusMinusContext)_localctx).e =  ne; ne.setType(((UnaryExpressionNotPlusMinusContext)_localctx).t.t); _localctx.e.setLineNumber(((UnaryExpressionNotPlusMinusContext)_localctx).NEW.getLine()); _localctx.e.setCharacter(((UnaryExpressionNotPlusMinusContext)_localctx).NEW.getCharPositionInLine());
				setState(281); match(LPAREN);
				setState(285);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
					{
					setState(282); ((UnaryExpressionNotPlusMinusContext)_localctx).params = expressionList();
					ne.getParamExprs().addAll(((UnaryExpressionNotPlusMinusContext)_localctx).params.el);
					}
				}

				setState(287); match(RPAREN);
				}
				break;
			case TILDA:
				enterOuterAlt(_localctx, 2);
				{
				setState(289); ((UnaryExpressionNotPlusMinusContext)_localctx).TILDA = match(TILDA);
				setState(290); ((UnaryExpressionNotPlusMinusContext)_localctx).e1 = unaryExpression();
				((UnaryExpressionNotPlusMinusContext)_localctx).e =  new UnaryExpression(); ((UnaryExpression)_localctx.e).setOperator((((UnaryExpressionNotPlusMinusContext)_localctx).TILDA!=null?((UnaryExpressionNotPlusMinusContext)_localctx).TILDA.getText():null)); ((UnaryExpression)_localctx.e).setExpression(((UnaryExpressionNotPlusMinusContext)_localctx).e1.e); _localctx.e.setLineNumber(((UnaryExpressionNotPlusMinusContext)_localctx).e1.e.getLineNumber()); _localctx.e.setCharacter(((UnaryExpressionNotPlusMinusContext)_localctx).e1.e.getCharacter());
				}
				break;
			case BANG:
				enterOuterAlt(_localctx, 3);
				{
				setState(293); ((UnaryExpressionNotPlusMinusContext)_localctx).BANG = match(BANG);
				setState(294); ((UnaryExpressionNotPlusMinusContext)_localctx).e1 = unaryExpression();
				((UnaryExpressionNotPlusMinusContext)_localctx).e =  new UnaryExpression(); ((UnaryExpression)_localctx.e).setOperator((((UnaryExpressionNotPlusMinusContext)_localctx).BANG!=null?((UnaryExpressionNotPlusMinusContext)_localctx).BANG.getText():null)); ((UnaryExpression)_localctx.e).setExpression(((UnaryExpressionNotPlusMinusContext)_localctx).e1.e); _localctx.e.setLineNumber(((UnaryExpressionNotPlusMinusContext)_localctx).e1.e.getLineNumber()); _localctx.e.setCharacter(((UnaryExpressionNotPlusMinusContext)_localctx).e1.e.getCharacter());
				}
				break;
			case LIST:
			case INTLITERAL:
			case FLOATLITERAL:
			case DOUBLELITERAL:
			case CHARLITERAL:
			case STRINGLITERAL:
			case TRUE:
			case FALSE:
			case NULL:
			case LPAREN:
			case QUES:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 4);
				{
				setState(317);
				switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
				case 1:
					{
					setState(297); ((UnaryExpressionNotPlusMinusContext)_localctx).ec = castExpression();
					((UnaryExpressionNotPlusMinusContext)_localctx).e =  ((UnaryExpressionNotPlusMinusContext)_localctx).ec.e;
					}
					break;

				case 2:
					{
					setState(300); match(LPAREN);
					setState(301); ((UnaryExpressionNotPlusMinusContext)_localctx).ep = expression();
					((UnaryExpressionNotPlusMinusContext)_localctx).e =  ((UnaryExpressionNotPlusMinusContext)_localctx).ep.e;
					setState(303); match(RPAREN);
					}
					break;

				case 3:
					{
					setState(305); ((UnaryExpressionNotPlusMinusContext)_localctx).p = primary();
					((UnaryExpressionNotPlusMinusContext)_localctx).e =  ((UnaryExpressionNotPlusMinusContext)_localctx).p.tp;
					}
					break;

				case 4:
					{
					setState(308); ((UnaryExpressionNotPlusMinusContext)_localctx).l = literal();
					((UnaryExpressionNotPlusMinusContext)_localctx).e =  ((UnaryExpressionNotPlusMinusContext)_localctx).l.l;
					}
					break;

				case 5:
					{
					setState(311); ((UnaryExpressionNotPlusMinusContext)_localctx).QUES = match(QUES);
					setState(312); match(LPAREN);
					setState(313); ((UnaryExpressionNotPlusMinusContext)_localctx).el = expressionList();
					setState(314); match(RPAREN);
					((UnaryExpressionNotPlusMinusContext)_localctx).e =  new NonDetExpression(); ((NonDetExpression)_localctx.e).getChoices().addAll(((UnaryExpressionNotPlusMinusContext)_localctx).el.el);
						    	_localctx.e.setLineNumber(((UnaryExpressionNotPlusMinusContext)_localctx).QUES.getLine()); _localctx.e.setCharacter(((UnaryExpressionNotPlusMinusContext)_localctx).QUES.getCharPositionInLine());
					}
					break;
				}
				setState(325);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
				while ( _alt!=2 && _alt!=-1 ) {
					if ( _alt==1 ) {
						{
						{
						setState(319); ((UnaryExpressionNotPlusMinusContext)_localctx).DOT = match(DOT);
						setState(320); ((UnaryExpressionNotPlusMinusContext)_localctx).p2 = primary();

						        	DotPrimary de = new DotPrimary(); de.setLineNumber(((UnaryExpressionNotPlusMinusContext)_localctx).DOT.getLine()); de.setCharacter(((UnaryExpressionNotPlusMinusContext)_localctx).DOT.getCharPositionInLine());
						        	if (_localctx.e instanceof DotPrimary) {
						        		DotPrimary temp = (DotPrimary)_localctx.e;
						        		while(temp.getRight() instanceof DotPrimary)
						        			temp = (DotPrimary)temp.getRight();
						        		de.setLeft(temp.getRight());
						        		temp.setRight(de);
						        		de.setRight(((UnaryExpressionNotPlusMinusContext)_localctx).p2.tp);
						        	} else {
						        		de.setLeft(_localctx.e); de.setRight(((UnaryExpressionNotPlusMinusContext)_localctx).p2.tp);
							        	((UnaryExpressionNotPlusMinusContext)_localctx).e =  de;
						        	}
						        	
						}
						} 
					}
					setState(327);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
				}
				setState(332);
				switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
				case 1:
					{
					setState(328); ((UnaryExpressionNotPlusMinusContext)_localctx).PLUSPLUS = match(PLUSPLUS);
					PlusSubExpression pse = new PlusSubExpression(); pse.setValue(_localctx.e); pse.setOperator("++");((UnaryExpressionNotPlusMinusContext)_localctx).e = pse;
					        		pse.setLineNumber(((UnaryExpressionNotPlusMinusContext)_localctx).PLUSPLUS.getLine()); pse.setCharacter(((UnaryExpressionNotPlusMinusContext)_localctx).PLUSPLUS.getCharPositionInLine());
					}
					break;

				case 2:
					{
					setState(330); ((UnaryExpressionNotPlusMinusContext)_localctx).SUBSUB = match(SUBSUB);
					PlusSubExpression pse = new PlusSubExpression(); pse.setValue(_localctx.e); pse.setOperator("--");((UnaryExpressionNotPlusMinusContext)_localctx).e = pse;
					        		pse.setLineNumber(((UnaryExpressionNotPlusMinusContext)_localctx).SUBSUB.getLine()); pse.setCharacter(((UnaryExpressionNotPlusMinusContext)_localctx).SUBSUB.getCharPositionInLine());
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainDeclarationContext extends ParserRuleContext {
		public MainDeclaration md;
		public Token MAIN;
		public BlockContext b;
		public TerminalNode MAIN() { return getToken(DynamicRebecaCompleteParser.MAIN, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public MainDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mainDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterMainDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitMainDeclaration(this);
		}
	}

	public final MainDeclarationContext mainDeclaration() throws RecognitionException {
		MainDeclarationContext _localctx = new MainDeclarationContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_mainDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			((MainDeclarationContext)_localctx).md =  new MainBlock();
			setState(337); ((MainDeclarationContext)_localctx).MAIN = match(MAIN);
			_localctx.md.setLineNumber(((MainDeclarationContext)_localctx).MAIN.getLine());_localctx.md.setCharacter(((MainDeclarationContext)_localctx).MAIN.getCharPositionInLine());
			setState(339); ((MainDeclarationContext)_localctx).b = block();
			((MainBlock)_localctx.md).setBlock(((MainDeclarationContext)_localctx).b.bs);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RebecaModelContext extends ParserRuleContext {
		public RebecaModel r;
		public PackageDeclarationContext p;
		public ImportDeclarationContext i;
		public RebecaCodeContext t1;
		public PackageDeclarationContext packageDeclaration() {
			return getRuleContext(PackageDeclarationContext.class,0);
		}
		public ImportDeclarationContext importDeclaration(int i) {
			return getRuleContext(ImportDeclarationContext.class,i);
		}
		public List<ImportDeclarationContext> importDeclaration() {
			return getRuleContexts(ImportDeclarationContext.class);
		}
		public RebecaCodeContext rebecaCode() {
			return getRuleContext(RebecaCodeContext.class,0);
		}
		public RebecaModelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rebecaModel; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterRebecaModel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitRebecaModel(this);
		}
	}

	public final RebecaModelContext rebecaModel() throws RecognitionException {
		RebecaModelContext _localctx = new RebecaModelContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_rebecaModel);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((RebecaModelContext)_localctx).r =  new RebecaModel();
			        
			setState(346);
			_la = _input.LA(1);
			if (_la==PACKAGE) {
				{
				setState(343); ((RebecaModelContext)_localctx).p = packageDeclaration();
				_localctx.r.setPackageDeclaration(((RebecaModelContext)_localctx).p.p);
				}
			}

			setState(353);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==IMPORT) {
				{
				{
				setState(348); ((RebecaModelContext)_localctx).i = importDeclaration();
				_localctx.r.getImportDeclaration().add(((RebecaModelContext)_localctx).i.i);
				}
				}
				setState(355);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			{
			setState(356); ((RebecaModelContext)_localctx).t1 = rebecaCode();
			_localctx.r.setRebecaCode(((RebecaModelContext)_localctx).t1.rc);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PackageDeclarationContext extends ParserRuleContext {
		public PackageDeclaration p;
		public TerminalNode PACKAGE() { return getToken(DynamicRebecaCompleteParser.PACKAGE, 0); }
		public PackageDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_packageDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterPackageDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitPackageDeclaration(this);
		}
	}

	public final PackageDeclarationContext packageDeclaration() throws RecognitionException {
		PackageDeclarationContext _localctx = new PackageDeclarationContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_packageDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(359); match(PACKAGE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportDeclarationContext extends ParserRuleContext {
		public ImportDeclaration i;
		public TerminalNode IMPORT() { return getToken(DynamicRebecaCompleteParser.IMPORT, 0); }
		public ImportDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterImportDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitImportDeclaration(this);
		}
	}

	public final ImportDeclarationContext importDeclaration() throws RecognitionException {
		ImportDeclarationContext _localctx = new ImportDeclarationContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_importDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(361); match(IMPORT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RebecaCodeContext extends ParserRuleContext {
		public RebecaCode rc;
		public RecordDeclarationContext rd;
		public EnvironmentVariablesContext e;
		public ReactiveClassDeclarationContext rcd;
		public MainDeclarationContext md;
		public MainDeclarationContext mainDeclaration() {
			return getRuleContext(MainDeclarationContext.class,0);
		}
		public ReactiveClassDeclarationContext reactiveClassDeclaration(int i) {
			return getRuleContext(ReactiveClassDeclarationContext.class,i);
		}
		public RecordDeclarationContext recordDeclaration(int i) {
			return getRuleContext(RecordDeclarationContext.class,i);
		}
		public List<ReactiveClassDeclarationContext> reactiveClassDeclaration() {
			return getRuleContexts(ReactiveClassDeclarationContext.class);
		}
		public List<RecordDeclarationContext> recordDeclaration() {
			return getRuleContexts(RecordDeclarationContext.class);
		}
		public EnvironmentVariablesContext environmentVariables() {
			return getRuleContext(EnvironmentVariablesContext.class,0);
		}
		public RebecaCodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rebecaCode; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterRebecaCode(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitRebecaCode(this);
		}
	}

	public final RebecaCodeContext rebecaCode() throws RecognitionException {
		RebecaCodeContext _localctx = new RebecaCodeContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_rebecaCode);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((RebecaCodeContext)_localctx).rc =  new RebecaCode();
			setState(369);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==RECORD) {
				{
				{
				setState(364); ((RebecaCodeContext)_localctx).rd = recordDeclaration();
				_localctx.rc.getRecordDeclaration().add(((RebecaCodeContext)_localctx).rd.rd);
				}
				}
				setState(371);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			{
			setState(372); ((RebecaCodeContext)_localctx).e = environmentVariables();
			_localctx.rc.getEnvironmentVariables().addAll(((RebecaCodeContext)_localctx).e.fds);
			}
			setState(377); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(374); ((RebecaCodeContext)_localctx).rcd = reactiveClassDeclaration();
				_localctx.rc.getReactiveClassDeclaration().add(((RebecaCodeContext)_localctx).rcd.rcd);
				}
				}
				setState(379); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==REACTIVECLASS );
			setState(381); ((RebecaCodeContext)_localctx).md = mainDeclaration();
			_localctx.rc.setMainDeclaration(((RebecaCodeContext)_localctx).md.md);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RecordDeclarationContext extends ParserRuleContext {
		public RecordDeclaration rd;
		public TerminalNode RECORD() { return getToken(DynamicRebecaCompleteParser.RECORD, 0); }
		public RecordDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_recordDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterRecordDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitRecordDeclaration(this);
		}
	}

	public final RecordDeclarationContext recordDeclaration() throws RecognitionException {
		RecordDeclarationContext _localctx = new RecordDeclarationContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_recordDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(384); match(RECORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainRebecDefinitionContext extends ParserRuleContext {
		public MainRebecDefinition mrd;
		public TypeContext t;
		public Token rebecName;
		public ExpressionListContext el;
		public TerminalNode COLON() { return getToken(DynamicRebecaCompleteParser.COLON, 0); }
		public List<TerminalNode> RPAREN() { return getTokens(DynamicRebecaCompleteParser.RPAREN); }
		public TerminalNode RPAREN(int i) {
			return getToken(DynamicRebecaCompleteParser.RPAREN, i);
		}
		public List<ExpressionListContext> expressionList() {
			return getRuleContexts(ExpressionListContext.class);
		}
		public TerminalNode LPAREN(int i) {
			return getToken(DynamicRebecaCompleteParser.LPAREN, i);
		}
		public ExpressionListContext expressionList(int i) {
			return getRuleContext(ExpressionListContext.class,i);
		}
		public TerminalNode SEMI() { return getToken(DynamicRebecaCompleteParser.SEMI, 0); }
		public TerminalNode IDENTIFIER() { return getToken(DynamicRebecaCompleteParser.IDENTIFIER, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<TerminalNode> LPAREN() { return getTokens(DynamicRebecaCompleteParser.LPAREN); }
		public MainRebecDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mainRebecDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterMainRebecDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitMainRebecDefinition(this);
		}
	}

	public final MainRebecDefinitionContext mainRebecDefinition() throws RecognitionException {
		MainRebecDefinitionContext _localctx = new MainRebecDefinitionContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_mainRebecDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((MainRebecDefinitionContext)_localctx).mrd =  new MainRebecDefinition();
			setState(387); ((MainRebecDefinitionContext)_localctx).t = type();
			setState(388); ((MainRebecDefinitionContext)_localctx).rebecName = match(IDENTIFIER);
			_localctx.mrd.setType(((MainRebecDefinitionContext)_localctx).t.t);_localctx.mrd.setName((((MainRebecDefinitionContext)_localctx).rebecName!=null?((MainRebecDefinitionContext)_localctx).rebecName.getText():null));
						_localctx.mrd.setLineNumber(((MainRebecDefinitionContext)_localctx).rebecName.getLine()); _localctx.mrd.setCharacter(((MainRebecDefinitionContext)_localctx).rebecName.getCharPositionInLine());
			setState(390); match(LPAREN);
			setState(394);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
				{
				setState(391); ((MainRebecDefinitionContext)_localctx).el = expressionList();
				_localctx.mrd.getBindings().addAll(((MainRebecDefinitionContext)_localctx).el.el);
				}
			}

			setState(396); match(RPAREN);
			setState(397); match(COLON);
			setState(398); match(LPAREN);
			setState(402);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
				{
				setState(399); ((MainRebecDefinitionContext)_localctx).el = expressionList();
				_localctx.mrd.getArguments().addAll(((MainRebecDefinitionContext)_localctx).el.el);
				}
			}

			setState(404); match(RPAREN);
			setState(405); match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EnvironmentVariablesContext extends ParserRuleContext {
		public List<FieldDeclaration> fds;
		public FieldDeclarationContext fd;
		public TerminalNode ENV(int i) {
			return getToken(DynamicRebecaCompleteParser.ENV, i);
		}
		public List<FieldDeclarationContext> fieldDeclaration() {
			return getRuleContexts(FieldDeclarationContext.class);
		}
		public List<TerminalNode> ENV() { return getTokens(DynamicRebecaCompleteParser.ENV); }
		public List<TerminalNode> SEMI() { return getTokens(DynamicRebecaCompleteParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(DynamicRebecaCompleteParser.SEMI, i);
		}
		public FieldDeclarationContext fieldDeclaration(int i) {
			return getRuleContext(FieldDeclarationContext.class,i);
		}
		public EnvironmentVariablesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_environmentVariables; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterEnvironmentVariables(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitEnvironmentVariables(this);
		}
	}

	public final EnvironmentVariablesContext environmentVariables() throws RecognitionException {
		EnvironmentVariablesContext _localctx = new EnvironmentVariablesContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_environmentVariables);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((EnvironmentVariablesContext)_localctx).fds =  new ArrayList<FieldDeclaration>();
			setState(415);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ENV) {
				{
				{
				setState(408); match(ENV);
				setState(409); ((EnvironmentVariablesContext)_localctx).fd = fieldDeclaration();
				_localctx.fds.add(((EnvironmentVariablesContext)_localctx).fd.fd);
				setState(411); match(SEMI);
				}
				}
				setState(417);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldDeclarationContext extends ParserRuleContext {
		public FieldDeclaration fd;
		public TypeContext t;
		public VariableDeclaratorsContext vds;
		public VariableDeclaratorsContext variableDeclarators() {
			return getRuleContext(VariableDeclaratorsContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public FieldDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterFieldDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitFieldDeclaration(this);
		}
	}

	public final FieldDeclarationContext fieldDeclaration() throws RecognitionException {
		FieldDeclarationContext _localctx = new FieldDeclarationContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_fieldDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			((FieldDeclarationContext)_localctx).fd =  new FieldDeclaration();
			setState(419); ((FieldDeclarationContext)_localctx).t = type();
			setState(420); ((FieldDeclarationContext)_localctx).vds = variableDeclarators();
			_localctx.fd.setType(((FieldDeclarationContext)_localctx).t.t); _localctx.fd.getVariableDeclarators().addAll(((FieldDeclarationContext)_localctx).vds.vds);
			    		_localctx.fd.setCharacter(((FieldDeclarationContext)_localctx).t.t.getCharacter()); _localctx.fd.setLineNumber(((FieldDeclarationContext)_localctx).t.t.getLineNumber());
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclaratorsContext extends ParserRuleContext {
		public List<VariableDeclarator> vds;
		public VariableDeclaratorContext vd;
		public List<VariableDeclaratorContext> variableDeclarator() {
			return getRuleContexts(VariableDeclaratorContext.class);
		}
		public TerminalNode COMMA(int i) {
			return getToken(DynamicRebecaCompleteParser.COMMA, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(DynamicRebecaCompleteParser.COMMA); }
		public VariableDeclaratorContext variableDeclarator(int i) {
			return getRuleContext(VariableDeclaratorContext.class,i);
		}
		public VariableDeclaratorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclarators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterVariableDeclarators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitVariableDeclarators(this);
		}
	}

	public final VariableDeclaratorsContext variableDeclarators() throws RecognitionException {
		VariableDeclaratorsContext _localctx = new VariableDeclaratorsContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_variableDeclarators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((VariableDeclaratorsContext)_localctx).vds =  new LinkedList<VariableDeclarator>();
			setState(424); ((VariableDeclaratorsContext)_localctx).vd = variableDeclarator();
			_localctx.vds.add(((VariableDeclaratorsContext)_localctx).vd.vd);
			setState(432);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(426); match(COMMA);
				setState(427); ((VariableDeclaratorsContext)_localctx).vd = variableDeclarator();
				_localctx.vds.add(((VariableDeclaratorsContext)_localctx).vd.vd);
				}
				}
				setState(434);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclaratorContext extends ParserRuleContext {
		public VariableDeclarator vd;
		public Token name;
		public VariableInitializerContext vi;
		public TerminalNode EQ() { return getToken(DynamicRebecaCompleteParser.EQ, 0); }
		public VariableInitializerContext variableInitializer() {
			return getRuleContext(VariableInitializerContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(DynamicRebecaCompleteParser.IDENTIFIER, 0); }
		public VariableDeclaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterVariableDeclarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitVariableDeclarator(this);
		}
	}

	public final VariableDeclaratorContext variableDeclarator() throws RecognitionException {
		VariableDeclaratorContext _localctx = new VariableDeclaratorContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_variableDeclarator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((VariableDeclaratorContext)_localctx).vd =  new VariableDeclarator();
			setState(436); ((VariableDeclaratorContext)_localctx).name = match(IDENTIFIER);
			_localctx.vd.setVariableName((((VariableDeclaratorContext)_localctx).name!=null?((VariableDeclaratorContext)_localctx).name.getText():null)); _localctx.vd.setLineNumber(((VariableDeclaratorContext)_localctx).name.getLine());_localctx.vd.setCharacter(((VariableDeclaratorContext)_localctx).name.getCharPositionInLine());
			setState(442);
			_la = _input.LA(1);
			if (_la==EQ) {
				{
				setState(438); match(EQ);
				setState(439); ((VariableDeclaratorContext)_localctx).vi = variableInitializer();
				_localctx.vd.setVariableInitializer(((VariableDeclaratorContext)_localctx).vi.vi);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableInitializerContext extends ParserRuleContext {
		public VariableInitializer vi;
		public ArrayInitializerContext ai;
		public ExpressionContext e;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ArrayInitializerContext arrayInitializer() {
			return getRuleContext(ArrayInitializerContext.class,0);
		}
		public VariableInitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableInitializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterVariableInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitVariableInitializer(this);
		}
	}

	public final VariableInitializerContext variableInitializer() throws RecognitionException {
		VariableInitializerContext _localctx = new VariableInitializerContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_variableInitializer);
		try {
			setState(450);
			switch (_input.LA(1)) {
			case LBRACE:
				enterOuterAlt(_localctx, 1);
				{
				setState(444); ((VariableInitializerContext)_localctx).ai = arrayInitializer();
				((VariableInitializerContext)_localctx).vi =  ((VariableInitializerContext)_localctx).ai.avi;
				}
				break;
			case LIST:
			case NEW:
			case INTLITERAL:
			case FLOATLITERAL:
			case DOUBLELITERAL:
			case CHARLITERAL:
			case STRINGLITERAL:
			case TRUE:
			case FALSE:
			case NULL:
			case LPAREN:
			case BANG:
			case TILDA:
			case QUES:
			case PLUSPLUS:
			case SUBSUB:
			case PLUS:
			case SUB:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 2);
				{
				setState(447); ((VariableInitializerContext)_localctx).e = expression();
				((VariableInitializerContext)_localctx).vi =  new OrdinaryVariableInitializer(); ((OrdinaryVariableInitializer)_localctx.vi).setValue(((VariableInitializerContext)_localctx).e.e);
				    		_localctx.vi.setLineNumber(((VariableInitializerContext)_localctx).e.e.getLineNumber());_localctx.vi.setCharacter(((VariableInitializerContext)_localctx).e.e.getCharacter());
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayInitializerContext extends ParserRuleContext {
		public ArrayVariableInitializer avi;
		public Token LBRACE;
		public VariableInitializerContext vi;
		public TerminalNode COMMA(int i) {
			return getToken(DynamicRebecaCompleteParser.COMMA, i);
		}
		public TerminalNode RBRACE() { return getToken(DynamicRebecaCompleteParser.RBRACE, 0); }
		public VariableInitializerContext variableInitializer(int i) {
			return getRuleContext(VariableInitializerContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(DynamicRebecaCompleteParser.COMMA); }
		public List<VariableInitializerContext> variableInitializer() {
			return getRuleContexts(VariableInitializerContext.class);
		}
		public TerminalNode LBRACE() { return getToken(DynamicRebecaCompleteParser.LBRACE, 0); }
		public ArrayInitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayInitializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterArrayInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitArrayInitializer(this);
		}
	}

	public final ArrayInitializerContext arrayInitializer() throws RecognitionException {
		ArrayInitializerContext _localctx = new ArrayInitializerContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_arrayInitializer);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((ArrayInitializerContext)_localctx).avi =  new ArrayVariableInitializer();
			setState(453); ((ArrayInitializerContext)_localctx).LBRACE = match(LBRACE);
			setState(465);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << LBRACE) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
				{
				setState(454); ((ArrayInitializerContext)_localctx).vi = variableInitializer();
				_localctx.avi.setCharacter(((ArrayInitializerContext)_localctx).LBRACE.getCharPositionInLine()); _localctx.avi.setLineNumber(((ArrayInitializerContext)_localctx).LBRACE.getLine());
				    		_localctx.avi.getValues().add(((ArrayInitializerContext)_localctx).vi.vi);
				setState(462);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(456); match(COMMA);
					setState(457); ((ArrayInitializerContext)_localctx).vi = variableInitializer();
					_localctx.avi.getValues().add(((ArrayInitializerContext)_localctx).vi.vi);
					}
					}
					setState(464);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(467); match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReactiveClassDeclarationContext extends ParserRuleContext {
		public ReactiveClassDeclaration rcd;
		public Token reactiveClassName;
		public Token queueSize;
		public FieldDeclarationContext fd;
		public ConstructorDeclarationContext cd;
		public MsgsrvDeclarationContext md;
		public SynchMethodDeclarationContext smd;
		public TerminalNode RPAREN() { return getToken(DynamicRebecaCompleteParser.RPAREN, 0); }
		public TerminalNode LBRACE(int i) {
			return getToken(DynamicRebecaCompleteParser.LBRACE, i);
		}
		public List<ConstructorDeclarationContext> constructorDeclaration() {
			return getRuleContexts(ConstructorDeclarationContext.class);
		}
		public ConstructorDeclarationContext constructorDeclaration(int i) {
			return getRuleContext(ConstructorDeclarationContext.class,i);
		}
		public List<TerminalNode> RBRACE() { return getTokens(DynamicRebecaCompleteParser.RBRACE); }
		public TerminalNode REACTIVECLASS() { return getToken(DynamicRebecaCompleteParser.REACTIVECLASS, 0); }
		public TerminalNode IDENTIFIER() { return getToken(DynamicRebecaCompleteParser.IDENTIFIER, 0); }
		public FieldDeclarationContext fieldDeclaration(int i) {
			return getRuleContext(FieldDeclarationContext.class,i);
		}
		public List<TerminalNode> LBRACE() { return getTokens(DynamicRebecaCompleteParser.LBRACE); }
		public TerminalNode STATEVARS() { return getToken(DynamicRebecaCompleteParser.STATEVARS, 0); }
		public TerminalNode RBRACE(int i) {
			return getToken(DynamicRebecaCompleteParser.RBRACE, i);
		}
		public List<FieldDeclarationContext> fieldDeclaration() {
			return getRuleContexts(FieldDeclarationContext.class);
		}
		public SynchMethodDeclarationContext synchMethodDeclaration(int i) {
			return getRuleContext(SynchMethodDeclarationContext.class,i);
		}
		public TerminalNode KNOWNREBECS() { return getToken(DynamicRebecaCompleteParser.KNOWNREBECS, 0); }
		public TerminalNode INTLITERAL() { return getToken(DynamicRebecaCompleteParser.INTLITERAL, 0); }
		public List<TerminalNode> SEMI() { return getTokens(DynamicRebecaCompleteParser.SEMI); }
		public List<SynchMethodDeclarationContext> synchMethodDeclaration() {
			return getRuleContexts(SynchMethodDeclarationContext.class);
		}
		public List<MsgsrvDeclarationContext> msgsrvDeclaration() {
			return getRuleContexts(MsgsrvDeclarationContext.class);
		}
		public TerminalNode SEMI(int i) {
			return getToken(DynamicRebecaCompleteParser.SEMI, i);
		}
		public MsgsrvDeclarationContext msgsrvDeclaration(int i) {
			return getRuleContext(MsgsrvDeclarationContext.class,i);
		}
		public TerminalNode LPAREN() { return getToken(DynamicRebecaCompleteParser.LPAREN, 0); }
		public ReactiveClassDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reactiveClassDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterReactiveClassDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitReactiveClassDeclaration(this);
		}
	}

	public final ReactiveClassDeclarationContext reactiveClassDeclaration() throws RecognitionException {
		ReactiveClassDeclarationContext _localctx = new ReactiveClassDeclarationContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_reactiveClassDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((ReactiveClassDeclarationContext)_localctx).rcd =  new ReactiveClassDeclaration();
			setState(470); match(REACTIVECLASS);
			setState(471); ((ReactiveClassDeclarationContext)_localctx).reactiveClassName = match(IDENTIFIER);
				_localctx.rcd.setName((((ReactiveClassDeclarationContext)_localctx).reactiveClassName!=null?((ReactiveClassDeclarationContext)_localctx).reactiveClassName.getText():null)); 
			        		_localctx.rcd.setLineNumber(((ReactiveClassDeclarationContext)_localctx).reactiveClassName.getLine()); _localctx.rcd.setCharacter(((ReactiveClassDeclarationContext)_localctx).reactiveClassName.getCharPositionInLine());
			        	
			setState(473); match(LPAREN);
			setState(474); ((ReactiveClassDeclarationContext)_localctx).queueSize = match(INTLITERAL);
			_localctx.rcd.setQueueSize(Integer.parseInt((((ReactiveClassDeclarationContext)_localctx).queueSize!=null?((ReactiveClassDeclarationContext)_localctx).queueSize.getText():null)));
			setState(476); match(RPAREN);
			setState(477); match(LBRACE);
			setState(490);
			_la = _input.LA(1);
			if (_la==KNOWNREBECS) {
				{
				setState(478); match(KNOWNREBECS);
				setState(479); match(LBRACE);
				setState(486);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==LIST || _la==IDENTIFIER) {
					{
					{
					setState(480); ((ReactiveClassDeclarationContext)_localctx).fd = fieldDeclaration();
					_localctx.rcd.getKnownRebecs().add(((ReactiveClassDeclarationContext)_localctx).fd.fd);
					setState(482); match(SEMI);
					}
					}
					setState(488);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(489); match(RBRACE);
				}
			}

			setState(504);
			_la = _input.LA(1);
			if (_la==STATEVARS) {
				{
				setState(492); match(STATEVARS);
				setState(493); match(LBRACE);
				setState(500);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==LIST || _la==IDENTIFIER) {
					{
					{
					setState(494); ((ReactiveClassDeclarationContext)_localctx).fd = fieldDeclaration();
					_localctx.rcd.getStatevars().add(((ReactiveClassDeclarationContext)_localctx).fd.fd);
					setState(496); match(SEMI);
					}
					}
					setState(502);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(503); match(RBRACE);
				}
			}

			setState(517);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LIST || _la==MSGSRV || _la==IDENTIFIER) {
				{
				setState(515);
				switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
				case 1:
					{
					setState(506); ((ReactiveClassDeclarationContext)_localctx).cd = constructorDeclaration();
					_localctx.rcd.getConstructors().add(((ReactiveClassDeclarationContext)_localctx).cd.cd);
					}
					break;

				case 2:
					{
					setState(509); ((ReactiveClassDeclarationContext)_localctx).md = msgsrvDeclaration();
					_localctx.rcd.getMsgsrvs().add(((ReactiveClassDeclarationContext)_localctx).md.md);
					}
					break;

				case 3:
					{
					setState(512); ((ReactiveClassDeclarationContext)_localctx).smd = synchMethodDeclaration();
					_localctx.rcd.getSynchMethods().add(((ReactiveClassDeclarationContext)_localctx).smd.smd);
					}
					break;
				}
				}
				setState(519);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(520); match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodDeclarationContext extends ParserRuleContext {
		public MethodDeclaration md;
		public Token name;
		public FormalParametersContext fps;
		public BlockContext b;
		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(DynamicRebecaCompleteParser.IDENTIFIER, 0); }
		public MethodDeclarationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public MethodDeclarationContext(ParserRuleContext parent, int invokingState, MethodDeclaration md) {
			super(parent, invokingState);
			this.md = md;
		}
		@Override public int getRuleIndex() { return RULE_methodDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterMethodDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitMethodDeclaration(this);
		}
	}

	public final MethodDeclarationContext methodDeclaration(MethodDeclaration md) throws RecognitionException {
		MethodDeclarationContext _localctx = new MethodDeclarationContext(_ctx, getState(), md);
		enterRule(_localctx, 40, RULE_methodDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(522); ((MethodDeclarationContext)_localctx).name = match(IDENTIFIER);
			_localctx.md.setName((((MethodDeclarationContext)_localctx).name!=null?((MethodDeclarationContext)_localctx).name.getText():null)); _localctx.md.setLineNumber(((MethodDeclarationContext)_localctx).name.getLine());_localctx.md.setCharacter(((MethodDeclarationContext)_localctx).name.getCharPositionInLine());
			setState(524); ((MethodDeclarationContext)_localctx).fps = formalParameters();
			_localctx.md.getFormalParameters().addAll(((MethodDeclarationContext)_localctx).fps.fps);
			setState(526); ((MethodDeclarationContext)_localctx).b = block();
			_localctx.md.setBlock(((MethodDeclarationContext)_localctx).b.bs);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorDeclarationContext extends ParserRuleContext {
		public ConstructorDeclaration cd;
		public MethodDeclarationContext methodDeclaration() {
			return getRuleContext(MethodDeclarationContext.class,0);
		}
		public ConstructorDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructorDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterConstructorDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitConstructorDeclaration(this);
		}
	}

	public final ConstructorDeclarationContext constructorDeclaration() throws RecognitionException {
		ConstructorDeclarationContext _localctx = new ConstructorDeclarationContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_constructorDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			((ConstructorDeclarationContext)_localctx).cd =  new ConstructorDeclaration();
			setState(530); methodDeclaration(_localctx.cd);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MsgsrvDeclarationContext extends ParserRuleContext {
		public MsgsrvDeclaration md;
		public TerminalNode MSGSRV() { return getToken(DynamicRebecaCompleteParser.MSGSRV, 0); }
		public MethodDeclarationContext methodDeclaration() {
			return getRuleContext(MethodDeclarationContext.class,0);
		}
		public MsgsrvDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_msgsrvDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterMsgsrvDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitMsgsrvDeclaration(this);
		}
	}

	public final MsgsrvDeclarationContext msgsrvDeclaration() throws RecognitionException {
		MsgsrvDeclarationContext _localctx = new MsgsrvDeclarationContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_msgsrvDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			((MsgsrvDeclarationContext)_localctx).md =  new MsgsrvDeclaration();
			setState(533); match(MSGSRV);
			setState(534); methodDeclaration(_localctx.md);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SynchMethodDeclarationContext extends ParserRuleContext {
		public SynchMethodDeclaration smd;
		public TypeContext t;
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public MethodDeclarationContext methodDeclaration() {
			return getRuleContext(MethodDeclarationContext.class,0);
		}
		public SynchMethodDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_synchMethodDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterSynchMethodDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitSynchMethodDeclaration(this);
		}
	}

	public final SynchMethodDeclarationContext synchMethodDeclaration() throws RecognitionException {
		SynchMethodDeclarationContext _localctx = new SynchMethodDeclarationContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_synchMethodDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			((SynchMethodDeclarationContext)_localctx).smd =  new SynchMethodDeclaration();
			setState(537); ((SynchMethodDeclarationContext)_localctx).t = type();
			_localctx.smd.setReturnType(((SynchMethodDeclarationContext)_localctx).t.t);
			setState(539); methodDeclaration(_localctx.smd);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalParametersContext extends ParserRuleContext {
		public List<FormalParameterDeclaration> fps;
		public FormalParametersDeclarationContext fpds;
		public TerminalNode RPAREN() { return getToken(DynamicRebecaCompleteParser.RPAREN, 0); }
		public TerminalNode LPAREN() { return getToken(DynamicRebecaCompleteParser.LPAREN, 0); }
		public FormalParametersDeclarationContext formalParametersDeclaration() {
			return getRuleContext(FormalParametersDeclarationContext.class,0);
		}
		public FormalParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterFormalParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitFormalParameters(this);
		}
	}

	public final FormalParametersContext formalParameters() throws RecognitionException {
		FormalParametersContext _localctx = new FormalParametersContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_formalParameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((FormalParametersContext)_localctx).fps =  new ArrayList<FormalParameterDeclaration>();
			setState(542); match(LPAREN);
			setState(546);
			_la = _input.LA(1);
			if (_la==LIST || _la==IDENTIFIER) {
				{
				setState(543); ((FormalParametersContext)_localctx).fpds = formalParametersDeclaration();
				_localctx.fps.addAll(((FormalParametersContext)_localctx).fpds.fpds);
				}
			}

			setState(548); match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalParametersDeclarationContext extends ParserRuleContext {
		public List<FormalParameterDeclaration> fpds;
		public FormalParameterDeclarationContext fpd;
		public TerminalNode COMMA(int i) {
			return getToken(DynamicRebecaCompleteParser.COMMA, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(DynamicRebecaCompleteParser.COMMA); }
		public FormalParameterDeclarationContext formalParameterDeclaration(int i) {
			return getRuleContext(FormalParameterDeclarationContext.class,i);
		}
		public List<FormalParameterDeclarationContext> formalParameterDeclaration() {
			return getRuleContexts(FormalParameterDeclarationContext.class);
		}
		public FormalParametersDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParametersDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterFormalParametersDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitFormalParametersDeclaration(this);
		}
	}

	public final FormalParametersDeclarationContext formalParametersDeclaration() throws RecognitionException {
		FormalParametersDeclarationContext _localctx = new FormalParametersDeclarationContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_formalParametersDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((FormalParametersDeclarationContext)_localctx).fpds =  new ArrayList<FormalParameterDeclaration>();
			setState(551); ((FormalParametersDeclarationContext)_localctx).fpd = formalParameterDeclaration();
			_localctx.fpds.add(((FormalParametersDeclarationContext)_localctx).fpd.fpd);
			setState(559);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(553); match(COMMA);
				setState(554); ((FormalParametersDeclarationContext)_localctx).fpd = formalParameterDeclaration();
				_localctx.fpds.add(((FormalParametersDeclarationContext)_localctx).fpd.fpd);
				}
				}
				setState(561);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalParameterDeclarationContext extends ParserRuleContext {
		public FormalParameterDeclaration fpd;
		public TypeContext t;
		public Token name;
		public TerminalNode IDENTIFIER() { return getToken(DynamicRebecaCompleteParser.IDENTIFIER, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public FormalParameterDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParameterDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterFormalParameterDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitFormalParameterDeclaration(this);
		}
	}

	public final FormalParameterDeclarationContext formalParameterDeclaration() throws RecognitionException {
		FormalParameterDeclarationContext _localctx = new FormalParameterDeclarationContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_formalParameterDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(562); ((FormalParameterDeclarationContext)_localctx).t = type();
			setState(563); ((FormalParameterDeclarationContext)_localctx).name = match(IDENTIFIER);

			            ((FormalParameterDeclarationContext)_localctx).fpd =  new FormalParameterDeclaration();
						_localctx.fpd.setLineNumber(((FormalParameterDeclarationContext)_localctx).name.getLine());
						_localctx.fpd.setCharacter(((FormalParameterDeclarationContext)_localctx).name.getCharPositionInLine());
			            _localctx.fpd.setName((((FormalParameterDeclarationContext)_localctx).name!=null?((FormalParameterDeclarationContext)_localctx).name.getText():null));
			            _localctx.fpd.setType(((FormalParameterDeclarationContext)_localctx).t.t);
			        
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public BlockStatement bs;
		public Token LBRACE;
		public StatementContext s;
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public TerminalNode RBRACE() { return getToken(DynamicRebecaCompleteParser.RBRACE, 0); }
		public TerminalNode LBRACE() { return getToken(DynamicRebecaCompleteParser.LBRACE, 0); }
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitBlock(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((BlockContext)_localctx).bs =  new BlockStatement();
			setState(567); ((BlockContext)_localctx).LBRACE = match(LBRACE);
			_localctx.bs.setLineNumber(((BlockContext)_localctx).LBRACE.getLine());_localctx.bs.setCharacter(((BlockContext)_localctx).LBRACE.getCharPositionInLine());
			setState(574);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << BREAK) | (1L << CONTINUE) | (1L << FOR) | (1L << IF) | (1L << RETURN) | (1L << SWITCH) | (1L << WHILE) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << LBRACE) | (1L << SEMI) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
				{
				{
				setState(569); ((BlockContext)_localctx).s = statement();
				_localctx.bs.getStatements().add (((BlockContext)_localctx).s.s);
				}
				}
				setState(576);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(577); match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForInitContext extends ParserRuleContext {
		public ForInitializer fi;
		public FieldDeclarationContext fd;
		public ExpressionListContext el;
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public FieldDeclarationContext fieldDeclaration() {
			return getRuleContext(FieldDeclarationContext.class,0);
		}
		public ForInitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forInit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterForInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitForInit(this);
		}
	}

	public final ForInitContext forInit() throws RecognitionException {
		ForInitContext _localctx = new ForInitContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_forInit);
		try {
			setState(585);
			switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(579); ((ForInitContext)_localctx).fd = fieldDeclaration();
				((ForInitContext)_localctx).fi =  new ForInitializer(); _localctx.fi.setFieldDeclaration(((ForInitContext)_localctx).fd.fd); 
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(582); ((ForInitContext)_localctx).el = expressionList();
				((ForInitContext)_localctx).fi =  new ForInitializer(); _localctx.fi.getExpressions().addAll(((ForInitContext)_localctx).el.el);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SwitchBlockContext extends ParserRuleContext {
		public SwitchStatement ss;
		public ExpressionContext e;
		public Token DEFAULT;
		public StatementContext st;
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public List<TerminalNode> COLON() { return getTokens(DynamicRebecaCompleteParser.COLON); }
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode COLON(int i) {
			return getToken(DynamicRebecaCompleteParser.COLON, i);
		}
		public TerminalNode DEFAULT(int i) {
			return getToken(DynamicRebecaCompleteParser.DEFAULT, i);
		}
		public List<TerminalNode> DEFAULT() { return getTokens(DynamicRebecaCompleteParser.DEFAULT); }
		public TerminalNode CASE(int i) {
			return getToken(DynamicRebecaCompleteParser.CASE, i);
		}
		public List<TerminalNode> CASE() { return getTokens(DynamicRebecaCompleteParser.CASE); }
		public SwitchBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switchBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterSwitchBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitSwitchBlock(this);
		}
	}

	public final SwitchBlockContext switchBlock() throws RecognitionException {
		SwitchBlockContext _localctx = new SwitchBlockContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_switchBlock);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((SwitchBlockContext)_localctx).ss =  new SwitchStatement();
			setState(608);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==CASE || _la==DEFAULT) {
				{
				{
				_localctx.ss.getSwitchStatementGroups().add(new SwitchStatementGroup());
				setState(595);
				switch (_input.LA(1)) {
				case CASE:
					{
					setState(589); match(CASE);
					setState(590); ((SwitchBlockContext)_localctx).e = expression();

					    			_localctx.ss.getSwitchStatementGroups().get(_localctx.ss.getSwitchStatementGroups().size() - 1).setExpression(((SwitchBlockContext)_localctx).e.e);
					    			_localctx.ss.getSwitchStatementGroups().get(_localctx.ss.getSwitchStatementGroups().size() - 1).setLineNumber(((SwitchBlockContext)_localctx).e.e.getLineNumber());
					    			_localctx.ss.getSwitchStatementGroups().get(_localctx.ss.getSwitchStatementGroups().size() - 1).setCharacter(((SwitchBlockContext)_localctx).e.e.getCharacter());
					    			
					}
					break;
				case DEFAULT:
					{
					setState(593); ((SwitchBlockContext)_localctx).DEFAULT = match(DEFAULT);

					    			_localctx.ss.getSwitchStatementGroups().get(_localctx.ss.getSwitchStatementGroups().size() - 1).setLineNumber(((SwitchBlockContext)_localctx).DEFAULT.getCharPositionInLine());
					    			_localctx.ss.getSwitchStatementGroups().get(_localctx.ss.getSwitchStatementGroups().size() - 1).setCharacter(((SwitchBlockContext)_localctx).DEFAULT.getLine());
					    			
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(597); match(COLON);
				setState(603);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << BREAK) | (1L << CONTINUE) | (1L << FOR) | (1L << IF) | (1L << RETURN) | (1L << SWITCH) | (1L << WHILE) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << LBRACE) | (1L << SEMI) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
					{
					{
					setState(598); ((SwitchBlockContext)_localctx).st = statement();
					_localctx.ss.getSwitchStatementGroups().get(_localctx.ss.getSwitchStatementGroups().size() - 1).getStatements().add(((SwitchBlockContext)_localctx).st.s);
					}
					}
					setState(605);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				}
				setState(610);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementExpressionContext extends ParserRuleContext {
		public Statement se;
		public ExpressionContext e;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StatementExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statementExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterStatementExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitStatementExpression(this);
		}
	}

	public final StatementExpressionContext statementExpression() throws RecognitionException {
		StatementExpressionContext _localctx = new StatementExpressionContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_statementExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(611); ((StatementExpressionContext)_localctx).e = expression();
			((StatementExpressionContext)_localctx).se =  ((StatementExpressionContext)_localctx).e.e;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DimensionsContext extends ParserRuleContext {
		public List<Integer> ds;
		public Token i;
		public List<TerminalNode> LBRACKET() { return getTokens(DynamicRebecaCompleteParser.LBRACKET); }
		public TerminalNode INTLITERAL(int i) {
			return getToken(DynamicRebecaCompleteParser.INTLITERAL, i);
		}
		public List<TerminalNode> RBRACKET() { return getTokens(DynamicRebecaCompleteParser.RBRACKET); }
		public List<TerminalNode> INTLITERAL() { return getTokens(DynamicRebecaCompleteParser.INTLITERAL); }
		public TerminalNode RBRACKET(int i) {
			return getToken(DynamicRebecaCompleteParser.RBRACKET, i);
		}
		public TerminalNode LBRACKET(int i) {
			return getToken(DynamicRebecaCompleteParser.LBRACKET, i);
		}
		public DimensionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dimensions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterDimensions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitDimensions(this);
		}
	}

	public final DimensionsContext dimensions() throws RecognitionException {
		DimensionsContext _localctx = new DimensionsContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_dimensions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((DimensionsContext)_localctx).ds =  new LinkedList();
			setState(619); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(615); match(LBRACKET);
				setState(616); ((DimensionsContext)_localctx).i = match(INTLITERAL);
				setState(617); match(RBRACKET);
				_localctx.ds.add(Integer.parseInt((((DimensionsContext)_localctx).i!=null?((DimensionsContext)_localctx).i.getText():null)));
				}
				}
				setState(621); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==LBRACKET );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public Expression e;
		public ConditionalExpressionContext e1;
		public AssignmentOperatorContext ao;
		public ExpressionContext e2;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AssignmentOperatorContext assignmentOperator() {
			return getRuleContext(AssignmentOperatorContext.class,0);
		}
		public ConditionalExpressionContext conditionalExpression() {
			return getRuleContext(ConditionalExpressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(623); ((ExpressionContext)_localctx).e1 = conditionalExpression();
			((ExpressionContext)_localctx).e =  ((ExpressionContext)_localctx).e1.e;
			setState(629);
			switch ( getInterpreter().adaptivePredict(_input,45,_ctx) ) {
			case 1:
				{
				setState(625); ((ExpressionContext)_localctx).ao = assignmentOperator();
				setState(626); ((ExpressionContext)_localctx).e2 = expression();
				BinaryExpression e3 = new BinaryExpression();
							e3.setOperator(((ExpressionContext)_localctx).ao.ao); e3.setLeft(((ExpressionContext)_localctx).e1.e); e3.setRight(((ExpressionContext)_localctx).e2.e);
							e3.setLineNumber(((ExpressionContext)_localctx).e1.e.getLineNumber());e3.setCharacter(((ExpressionContext)_localctx).e1.e.getCharacter()); ((ExpressionContext)_localctx).e =  e3;
							
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentOperatorContext extends ParserRuleContext {
		public String ao;
		public Token EQ;
		public Token PLUSEQ;
		public Token SUBEQ;
		public Token STAREQ;
		public Token SLASHEQ;
		public Token AMPEQ;
		public Token BAREQ;
		public Token CARETEQ;
		public Token TILDAEQ;
		public Token PERCENTEQ;
		public Token LTLTEQ;
		public Token GTGTEQ;
		public TerminalNode SUBEQ() { return getToken(DynamicRebecaCompleteParser.SUBEQ, 0); }
		public TerminalNode TILDAEQ() { return getToken(DynamicRebecaCompleteParser.TILDAEQ, 0); }
		public TerminalNode CARETEQ() { return getToken(DynamicRebecaCompleteParser.CARETEQ, 0); }
		public TerminalNode SLASHEQ() { return getToken(DynamicRebecaCompleteParser.SLASHEQ, 0); }
		public TerminalNode PERCENTEQ() { return getToken(DynamicRebecaCompleteParser.PERCENTEQ, 0); }
		public TerminalNode BAREQ() { return getToken(DynamicRebecaCompleteParser.BAREQ, 0); }
		public TerminalNode EQ() { return getToken(DynamicRebecaCompleteParser.EQ, 0); }
		public TerminalNode AMPEQ() { return getToken(DynamicRebecaCompleteParser.AMPEQ, 0); }
		public TerminalNode STAREQ() { return getToken(DynamicRebecaCompleteParser.STAREQ, 0); }
		public TerminalNode PLUSEQ() { return getToken(DynamicRebecaCompleteParser.PLUSEQ, 0); }
		public TerminalNode LTLTEQ() { return getToken(DynamicRebecaCompleteParser.LTLTEQ, 0); }
		public TerminalNode GTGTEQ() { return getToken(DynamicRebecaCompleteParser.GTGTEQ, 0); }
		public AssignmentOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterAssignmentOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitAssignmentOperator(this);
		}
	}

	public final AssignmentOperatorContext assignmentOperator() throws RecognitionException {
		AssignmentOperatorContext _localctx = new AssignmentOperatorContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_assignmentOperator);
		try {
			setState(655);
			switch (_input.LA(1)) {
			case EQ:
				enterOuterAlt(_localctx, 1);
				{
				setState(631); ((AssignmentOperatorContext)_localctx).EQ = match(EQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).EQ!=null?((AssignmentOperatorContext)_localctx).EQ.getText():null);
				}
				break;
			case PLUSEQ:
				enterOuterAlt(_localctx, 2);
				{
				setState(633); ((AssignmentOperatorContext)_localctx).PLUSEQ = match(PLUSEQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).PLUSEQ!=null?((AssignmentOperatorContext)_localctx).PLUSEQ.getText():null);
				}
				break;
			case SUBEQ:
				enterOuterAlt(_localctx, 3);
				{
				setState(635); ((AssignmentOperatorContext)_localctx).SUBEQ = match(SUBEQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).SUBEQ!=null?((AssignmentOperatorContext)_localctx).SUBEQ.getText():null);
				}
				break;
			case STAREQ:
				enterOuterAlt(_localctx, 4);
				{
				setState(637); ((AssignmentOperatorContext)_localctx).STAREQ = match(STAREQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).STAREQ!=null?((AssignmentOperatorContext)_localctx).STAREQ.getText():null);
				}
				break;
			case SLASHEQ:
				enterOuterAlt(_localctx, 5);
				{
				setState(639); ((AssignmentOperatorContext)_localctx).SLASHEQ = match(SLASHEQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).SLASHEQ!=null?((AssignmentOperatorContext)_localctx).SLASHEQ.getText():null);
				}
				break;
			case AMPEQ:
				enterOuterAlt(_localctx, 6);
				{
				setState(641); ((AssignmentOperatorContext)_localctx).AMPEQ = match(AMPEQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).AMPEQ!=null?((AssignmentOperatorContext)_localctx).AMPEQ.getText():null);
				}
				break;
			case BAREQ:
				enterOuterAlt(_localctx, 7);
				{
				setState(643); ((AssignmentOperatorContext)_localctx).BAREQ = match(BAREQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).BAREQ!=null?((AssignmentOperatorContext)_localctx).BAREQ.getText():null);
				}
				break;
			case CARETEQ:
				enterOuterAlt(_localctx, 8);
				{
				setState(645); ((AssignmentOperatorContext)_localctx).CARETEQ = match(CARETEQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).CARETEQ!=null?((AssignmentOperatorContext)_localctx).CARETEQ.getText():null);
				}
				break;
			case TILDAEQ:
				enterOuterAlt(_localctx, 9);
				{
				setState(647); ((AssignmentOperatorContext)_localctx).TILDAEQ = match(TILDAEQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).TILDAEQ!=null?((AssignmentOperatorContext)_localctx).TILDAEQ.getText():null);
				}
				break;
			case PERCENTEQ:
				enterOuterAlt(_localctx, 10);
				{
				setState(649); ((AssignmentOperatorContext)_localctx).PERCENTEQ = match(PERCENTEQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).PERCENTEQ!=null?((AssignmentOperatorContext)_localctx).PERCENTEQ.getText():null);
				}
				break;
			case LTLTEQ:
				enterOuterAlt(_localctx, 11);
				{
				setState(651); ((AssignmentOperatorContext)_localctx).LTLTEQ = match(LTLTEQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).LTLTEQ!=null?((AssignmentOperatorContext)_localctx).LTLTEQ.getText():null);
				}
				break;
			case GTGTEQ:
				enterOuterAlt(_localctx, 12);
				{
				setState(653); ((AssignmentOperatorContext)_localctx).GTGTEQ = match(GTGTEQ);
				((AssignmentOperatorContext)_localctx).ao =  (((AssignmentOperatorContext)_localctx).GTGTEQ!=null?((AssignmentOperatorContext)_localctx).GTGTEQ.getText():null);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionalExpressionContext extends ParserRuleContext {
		public Expression e;
		public ConditionalOrExpressionContext e1;
		public ExpressionContext e2;
		public ConditionalExpressionContext e3;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode COLON() { return getToken(DynamicRebecaCompleteParser.COLON, 0); }
		public ConditionalOrExpressionContext conditionalOrExpression() {
			return getRuleContext(ConditionalOrExpressionContext.class,0);
		}
		public TerminalNode QUES() { return getToken(DynamicRebecaCompleteParser.QUES, 0); }
		public ConditionalExpressionContext conditionalExpression() {
			return getRuleContext(ConditionalExpressionContext.class,0);
		}
		public ConditionalExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionalExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterConditionalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitConditionalExpression(this);
		}
	}

	public final ConditionalExpressionContext conditionalExpression() throws RecognitionException {
		ConditionalExpressionContext _localctx = new ConditionalExpressionContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_conditionalExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(657); ((ConditionalExpressionContext)_localctx).e1 = conditionalOrExpression();
			((ConditionalExpressionContext)_localctx).e =  ((ConditionalExpressionContext)_localctx).e1.e;
			setState(665);
			switch ( getInterpreter().adaptivePredict(_input,47,_ctx) ) {
			case 1:
				{
				setState(659); match(QUES);
				setState(660); ((ConditionalExpressionContext)_localctx).e2 = expression();
				setState(661); match(COLON);
				setState(662); ((ConditionalExpressionContext)_localctx).e3 = conditionalExpression();
				TernaryExpression e4 = new TernaryExpression();
							e4.setCondition(((ConditionalExpressionContext)_localctx).e1.e); e4.setLeft(((ConditionalExpressionContext)_localctx).e2.e); e4.setRight(((ConditionalExpressionContext)_localctx).e3.e);
							e4.setLineNumber(((ConditionalExpressionContext)_localctx).e1.e.getLineNumber());e4.setCharacter(((ConditionalExpressionContext)_localctx).e1.e.getCharacter()); ((ConditionalExpressionContext)_localctx).e =  e4;
							
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionalOrExpressionContext extends ParserRuleContext {
		public Expression e;
		public ConditionalAndExpressionContext e1;
		public Token BARBAR;
		public ConditionalAndExpressionContext e2;
		public List<TerminalNode> BARBAR() { return getTokens(DynamicRebecaCompleteParser.BARBAR); }
		public TerminalNode BARBAR(int i) {
			return getToken(DynamicRebecaCompleteParser.BARBAR, i);
		}
		public List<ConditionalAndExpressionContext> conditionalAndExpression() {
			return getRuleContexts(ConditionalAndExpressionContext.class);
		}
		public ConditionalAndExpressionContext conditionalAndExpression(int i) {
			return getRuleContext(ConditionalAndExpressionContext.class,i);
		}
		public ConditionalOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionalOrExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterConditionalOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitConditionalOrExpression(this);
		}
	}

	public final ConditionalOrExpressionContext conditionalOrExpression() throws RecognitionException {
		ConditionalOrExpressionContext _localctx = new ConditionalOrExpressionContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_conditionalOrExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(667); ((ConditionalOrExpressionContext)_localctx).e1 = conditionalAndExpression();
			((ConditionalOrExpressionContext)_localctx).e =  ((ConditionalOrExpressionContext)_localctx).e1.e;
			setState(675);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,48,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(669); ((ConditionalOrExpressionContext)_localctx).BARBAR = match(BARBAR);
					setState(670); ((ConditionalOrExpressionContext)_localctx).e2 = conditionalAndExpression();
					BinaryExpression e3 = new BinaryExpression();
								e3.setOperator((((ConditionalOrExpressionContext)_localctx).BARBAR!=null?((ConditionalOrExpressionContext)_localctx).BARBAR.getText():null)); e3.setLeft(((ConditionalOrExpressionContext)_localctx).e1.e); e3.setRight(((ConditionalOrExpressionContext)_localctx).e2.e);
								e3.setLineNumber(((ConditionalOrExpressionContext)_localctx).e1.e.getLineNumber());e3.setCharacter(((ConditionalOrExpressionContext)_localctx).e1.e.getCharacter()); ((ConditionalOrExpressionContext)_localctx).e =  e3;
								
					}
					} 
				}
				setState(677);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,48,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionalAndExpressionContext extends ParserRuleContext {
		public Expression e;
		public InclusiveOrExpressionContext e1;
		public Token AMPAMP;
		public InclusiveOrExpressionContext e2;
		public List<InclusiveOrExpressionContext> inclusiveOrExpression() {
			return getRuleContexts(InclusiveOrExpressionContext.class);
		}
		public List<TerminalNode> AMPAMP() { return getTokens(DynamicRebecaCompleteParser.AMPAMP); }
		public InclusiveOrExpressionContext inclusiveOrExpression(int i) {
			return getRuleContext(InclusiveOrExpressionContext.class,i);
		}
		public TerminalNode AMPAMP(int i) {
			return getToken(DynamicRebecaCompleteParser.AMPAMP, i);
		}
		public ConditionalAndExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionalAndExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterConditionalAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitConditionalAndExpression(this);
		}
	}

	public final ConditionalAndExpressionContext conditionalAndExpression() throws RecognitionException {
		ConditionalAndExpressionContext _localctx = new ConditionalAndExpressionContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_conditionalAndExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(678); ((ConditionalAndExpressionContext)_localctx).e1 = inclusiveOrExpression();
			((ConditionalAndExpressionContext)_localctx).e =  ((ConditionalAndExpressionContext)_localctx).e1.e;
			setState(686);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(680); ((ConditionalAndExpressionContext)_localctx).AMPAMP = match(AMPAMP);
					setState(681); ((ConditionalAndExpressionContext)_localctx).e2 = inclusiveOrExpression();
					BinaryExpression e3 = new BinaryExpression();
								e3.setOperator((((ConditionalAndExpressionContext)_localctx).AMPAMP!=null?((ConditionalAndExpressionContext)_localctx).AMPAMP.getText():null)); e3.setLeft(((ConditionalAndExpressionContext)_localctx).e1.e); e3.setRight(((ConditionalAndExpressionContext)_localctx).e2.e);
								e3.setLineNumber(((ConditionalAndExpressionContext)_localctx).e1.e.getLineNumber());e3.setCharacter(((ConditionalAndExpressionContext)_localctx).e1.e.getCharacter()); ((ConditionalAndExpressionContext)_localctx).e =  e3;
								
					}
					} 
				}
				setState(688);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,49,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InclusiveOrExpressionContext extends ParserRuleContext {
		public Expression e;
		public ExclusiveOrExpressionContext e1;
		public Token BAR;
		public ExclusiveOrExpressionContext e2;
		public List<ExclusiveOrExpressionContext> exclusiveOrExpression() {
			return getRuleContexts(ExclusiveOrExpressionContext.class);
		}
		public ExclusiveOrExpressionContext exclusiveOrExpression(int i) {
			return getRuleContext(ExclusiveOrExpressionContext.class,i);
		}
		public TerminalNode BAR(int i) {
			return getToken(DynamicRebecaCompleteParser.BAR, i);
		}
		public List<TerminalNode> BAR() { return getTokens(DynamicRebecaCompleteParser.BAR); }
		public InclusiveOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inclusiveOrExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterInclusiveOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitInclusiveOrExpression(this);
		}
	}

	public final InclusiveOrExpressionContext inclusiveOrExpression() throws RecognitionException {
		InclusiveOrExpressionContext _localctx = new InclusiveOrExpressionContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_inclusiveOrExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(689); ((InclusiveOrExpressionContext)_localctx).e1 = exclusiveOrExpression();
			((InclusiveOrExpressionContext)_localctx).e =  ((InclusiveOrExpressionContext)_localctx).e1.e;
			setState(697);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,50,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(691); ((InclusiveOrExpressionContext)_localctx).BAR = match(BAR);
					setState(692); ((InclusiveOrExpressionContext)_localctx).e2 = exclusiveOrExpression();
					BinaryExpression e3 = new BinaryExpression();
								e3.setOperator((((InclusiveOrExpressionContext)_localctx).BAR!=null?((InclusiveOrExpressionContext)_localctx).BAR.getText():null)); e3.setLeft(((InclusiveOrExpressionContext)_localctx).e1.e); e3.setRight(((InclusiveOrExpressionContext)_localctx).e2.e);
								e3.setLineNumber(((InclusiveOrExpressionContext)_localctx).e1.e.getLineNumber());e3.setCharacter(((InclusiveOrExpressionContext)_localctx).e1.e.getCharacter()); ((InclusiveOrExpressionContext)_localctx).e =  e3;
								
					}
					} 
				}
				setState(699);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,50,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExclusiveOrExpressionContext extends ParserRuleContext {
		public Expression e;
		public AndExpressionContext e1;
		public Token CARET;
		public AndExpressionContext e2;
		public List<AndExpressionContext> andExpression() {
			return getRuleContexts(AndExpressionContext.class);
		}
		public TerminalNode CARET(int i) {
			return getToken(DynamicRebecaCompleteParser.CARET, i);
		}
		public List<TerminalNode> CARET() { return getTokens(DynamicRebecaCompleteParser.CARET); }
		public AndExpressionContext andExpression(int i) {
			return getRuleContext(AndExpressionContext.class,i);
		}
		public ExclusiveOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exclusiveOrExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterExclusiveOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitExclusiveOrExpression(this);
		}
	}

	public final ExclusiveOrExpressionContext exclusiveOrExpression() throws RecognitionException {
		ExclusiveOrExpressionContext _localctx = new ExclusiveOrExpressionContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_exclusiveOrExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(700); ((ExclusiveOrExpressionContext)_localctx).e1 = andExpression();
			((ExclusiveOrExpressionContext)_localctx).e =  ((ExclusiveOrExpressionContext)_localctx).e1.e;
			setState(708);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,51,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(702); ((ExclusiveOrExpressionContext)_localctx).CARET = match(CARET);
					setState(703); ((ExclusiveOrExpressionContext)_localctx).e2 = andExpression();
					BinaryExpression e3 = new BinaryExpression();
								e3.setOperator((((ExclusiveOrExpressionContext)_localctx).CARET!=null?((ExclusiveOrExpressionContext)_localctx).CARET.getText():null)); e3.setLeft(((ExclusiveOrExpressionContext)_localctx).e1.e); e3.setRight(((ExclusiveOrExpressionContext)_localctx).e2.e);
								e3.setLineNumber(((ExclusiveOrExpressionContext)_localctx).e1.e.getLineNumber());e3.setCharacter(((ExclusiveOrExpressionContext)_localctx).e1.e.getCharacter()); ((ExclusiveOrExpressionContext)_localctx).e =  e3;
								
					}
					} 
				}
				setState(710);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,51,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AndExpressionContext extends ParserRuleContext {
		public Expression e;
		public EqualityExpressionContext e1;
		public Token AMP;
		public EqualityExpressionContext e2;
		public EqualityExpressionContext equalityExpression(int i) {
			return getRuleContext(EqualityExpressionContext.class,i);
		}
		public List<EqualityExpressionContext> equalityExpression() {
			return getRuleContexts(EqualityExpressionContext.class);
		}
		public TerminalNode AMP(int i) {
			return getToken(DynamicRebecaCompleteParser.AMP, i);
		}
		public List<TerminalNode> AMP() { return getTokens(DynamicRebecaCompleteParser.AMP); }
		public AndExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_andExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitAndExpression(this);
		}
	}

	public final AndExpressionContext andExpression() throws RecognitionException {
		AndExpressionContext _localctx = new AndExpressionContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_andExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(711); ((AndExpressionContext)_localctx).e1 = equalityExpression();
			((AndExpressionContext)_localctx).e =  ((AndExpressionContext)_localctx).e1.e;
			setState(719);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,52,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(713); ((AndExpressionContext)_localctx).AMP = match(AMP);
					setState(714); ((AndExpressionContext)_localctx).e2 = equalityExpression();
					BinaryExpression e3 = new BinaryExpression();
								e3.setOperator((((AndExpressionContext)_localctx).AMP!=null?((AndExpressionContext)_localctx).AMP.getText():null)); e3.setLeft(((AndExpressionContext)_localctx).e1.e); e3.setRight(((AndExpressionContext)_localctx).e2.e);
								e3.setLineNumber(((AndExpressionContext)_localctx).e1.e.getLineNumber());e3.setCharacter(((AndExpressionContext)_localctx).e1.e.getCharacter()); ((AndExpressionContext)_localctx).e =  e3;
								
					}
					} 
				}
				setState(721);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,52,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EqualityExpressionContext extends ParserRuleContext {
		public Expression e;
		public InstanceOfExpressionContext e1;
		public Token EQEQ;
		public Token BANGEQ;
		public InstanceOfExpressionContext e2;
		public List<TerminalNode> BANGEQ() { return getTokens(DynamicRebecaCompleteParser.BANGEQ); }
		public InstanceOfExpressionContext instanceOfExpression(int i) {
			return getRuleContext(InstanceOfExpressionContext.class,i);
		}
		public List<InstanceOfExpressionContext> instanceOfExpression() {
			return getRuleContexts(InstanceOfExpressionContext.class);
		}
		public TerminalNode EQEQ(int i) {
			return getToken(DynamicRebecaCompleteParser.EQEQ, i);
		}
		public TerminalNode BANGEQ(int i) {
			return getToken(DynamicRebecaCompleteParser.BANGEQ, i);
		}
		public List<TerminalNode> EQEQ() { return getTokens(DynamicRebecaCompleteParser.EQEQ); }
		public EqualityExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equalityExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterEqualityExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitEqualityExpression(this);
		}
	}

	public final EqualityExpressionContext equalityExpression() throws RecognitionException {
		EqualityExpressionContext _localctx = new EqualityExpressionContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_equalityExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(722); ((EqualityExpressionContext)_localctx).e1 = instanceOfExpression();
			((EqualityExpressionContext)_localctx).e =  ((EqualityExpressionContext)_localctx).e1.e;
			setState(736);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					((EqualityExpressionContext)_localctx).e =  new BinaryExpression(); ((BinaryExpression)_localctx.e).setLeft(((EqualityExpressionContext)_localctx).e1.e); _localctx.e.setLineNumber(((EqualityExpressionContext)_localctx).e1.e.getLineNumber()); _localctx.e.setCharacter(((EqualityExpressionContext)_localctx).e1.e.getCharacter());
					setState(729);
					switch (_input.LA(1)) {
					case EQEQ:
						{
						setState(725); ((EqualityExpressionContext)_localctx).EQEQ = match(EQEQ);
						((BinaryExpression)_localctx.e).setOperator((((EqualityExpressionContext)_localctx).EQEQ!=null?((EqualityExpressionContext)_localctx).EQEQ.getText():null));
						}
						break;
					case BANGEQ:
						{
						setState(727); ((EqualityExpressionContext)_localctx).BANGEQ = match(BANGEQ);
						((BinaryExpression)_localctx.e).setOperator((((EqualityExpressionContext)_localctx).BANGEQ!=null?((EqualityExpressionContext)_localctx).BANGEQ.getText():null));
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(731); ((EqualityExpressionContext)_localctx).e2 = instanceOfExpression();
					((BinaryExpression)_localctx.e).setRight(((EqualityExpressionContext)_localctx).e2.e);
					}
					} 
				}
				setState(738);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InstanceOfExpressionContext extends ParserRuleContext {
		public Expression e;
		public RelationalExpressionContext e1;
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public InstanceOfExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_instanceOfExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterInstanceOfExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitInstanceOfExpression(this);
		}
	}

	public final InstanceOfExpressionContext instanceOfExpression() throws RecognitionException {
		InstanceOfExpressionContext _localctx = new InstanceOfExpressionContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_instanceOfExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(739); ((InstanceOfExpressionContext)_localctx).e1 = relationalExpression();
			((InstanceOfExpressionContext)_localctx).e =  ((InstanceOfExpressionContext)_localctx).e1.e;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalExpressionContext extends ParserRuleContext {
		public Expression e;
		public ShiftExpressionContext e1;
		public RelationalOpContext ro;
		public ShiftExpressionContext e2;
		public List<RelationalOpContext> relationalOp() {
			return getRuleContexts(RelationalOpContext.class);
		}
		public RelationalOpContext relationalOp(int i) {
			return getRuleContext(RelationalOpContext.class,i);
		}
		public ShiftExpressionContext shiftExpression(int i) {
			return getRuleContext(ShiftExpressionContext.class,i);
		}
		public List<ShiftExpressionContext> shiftExpression() {
			return getRuleContexts(ShiftExpressionContext.class);
		}
		public RelationalExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterRelationalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitRelationalExpression(this);
		}
	}

	public final RelationalExpressionContext relationalExpression() throws RecognitionException {
		RelationalExpressionContext _localctx = new RelationalExpressionContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_relationalExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(742); ((RelationalExpressionContext)_localctx).e1 = shiftExpression();
			((RelationalExpressionContext)_localctx).e =  ((RelationalExpressionContext)_localctx).e1.e;
			setState(750);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,55,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(744); ((RelationalExpressionContext)_localctx).ro = relationalOp();
					setState(745); ((RelationalExpressionContext)_localctx).e2 = shiftExpression();
					BinaryExpression e3 = new BinaryExpression();
								e3.setOperator(((RelationalExpressionContext)_localctx).ro.ro); e3.setLeft(((RelationalExpressionContext)_localctx).e1.e); e3.setRight(((RelationalExpressionContext)_localctx).e2.e);
								e3.setLineNumber(((RelationalExpressionContext)_localctx).e1.e.getLineNumber());e3.setCharacter(((RelationalExpressionContext)_localctx).e1.e.getCharacter()); ((RelationalExpressionContext)_localctx).e =  e3;
								
					}
					} 
				}
				setState(752);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,55,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalOpContext extends ParserRuleContext {
		public String ro;
		public Token LT;
		public Token EQ;
		public Token GT;
		public TerminalNode GT() { return getToken(DynamicRebecaCompleteParser.GT, 0); }
		public TerminalNode LT() { return getToken(DynamicRebecaCompleteParser.LT, 0); }
		public TerminalNode EQ() { return getToken(DynamicRebecaCompleteParser.EQ, 0); }
		public RelationalOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterRelationalOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitRelationalOp(this);
		}
	}

	public final RelationalOpContext relationalOp() throws RecognitionException {
		RelationalOpContext _localctx = new RelationalOpContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_relationalOp);
		try {
			setState(763);
			switch ( getInterpreter().adaptivePredict(_input,56,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(753); ((RelationalOpContext)_localctx).LT = match(LT);
				setState(754); ((RelationalOpContext)_localctx).EQ = match(EQ);
				((RelationalOpContext)_localctx).ro =  (((RelationalOpContext)_localctx).LT!=null?((RelationalOpContext)_localctx).LT.getText():null) + (((RelationalOpContext)_localctx).EQ!=null?((RelationalOpContext)_localctx).EQ.getText():null);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(756); ((RelationalOpContext)_localctx).GT = match(GT);
				setState(757); ((RelationalOpContext)_localctx).EQ = match(EQ);
				((RelationalOpContext)_localctx).ro =  (((RelationalOpContext)_localctx).GT!=null?((RelationalOpContext)_localctx).GT.getText():null) + (((RelationalOpContext)_localctx).EQ!=null?((RelationalOpContext)_localctx).EQ.getText():null);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(759); ((RelationalOpContext)_localctx).LT = match(LT);
				((RelationalOpContext)_localctx).ro =  (((RelationalOpContext)_localctx).LT!=null?((RelationalOpContext)_localctx).LT.getText():null);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(761); ((RelationalOpContext)_localctx).GT = match(GT);
				((RelationalOpContext)_localctx).ro =  (((RelationalOpContext)_localctx).GT!=null?((RelationalOpContext)_localctx).GT.getText():null);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ShiftExpressionContext extends ParserRuleContext {
		public Expression e;
		public AdditiveExpressionContext e1;
		public ShiftOpContext so;
		public AdditiveExpressionContext e2;
		public List<ShiftOpContext> shiftOp() {
			return getRuleContexts(ShiftOpContext.class);
		}
		public ShiftOpContext shiftOp(int i) {
			return getRuleContext(ShiftOpContext.class,i);
		}
		public List<AdditiveExpressionContext> additiveExpression() {
			return getRuleContexts(AdditiveExpressionContext.class);
		}
		public AdditiveExpressionContext additiveExpression(int i) {
			return getRuleContext(AdditiveExpressionContext.class,i);
		}
		public ShiftExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shiftExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterShiftExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitShiftExpression(this);
		}
	}

	public final ShiftExpressionContext shiftExpression() throws RecognitionException {
		ShiftExpressionContext _localctx = new ShiftExpressionContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_shiftExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(765); ((ShiftExpressionContext)_localctx).e1 = additiveExpression();
			((ShiftExpressionContext)_localctx).e =  ((ShiftExpressionContext)_localctx).e1.e;
			setState(773);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,57,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(767); ((ShiftExpressionContext)_localctx).so = shiftOp();
					setState(768); ((ShiftExpressionContext)_localctx).e2 = additiveExpression();
					BinaryExpression e3 = new BinaryExpression();
								e3.setOperator(((ShiftExpressionContext)_localctx).so.so); e3.setLeft(((ShiftExpressionContext)_localctx).e1.e); e3.setRight(((ShiftExpressionContext)_localctx).e2.e);
								e3.setLineNumber(((ShiftExpressionContext)_localctx).e1.e.getLineNumber()); e3.setCharacter(((ShiftExpressionContext)_localctx).e1.e.getCharacter()); ((ShiftExpressionContext)_localctx).e =  e3;
								
					}
					} 
				}
				setState(775);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,57,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ShiftOpContext extends ParserRuleContext {
		public String so;
		public List<TerminalNode> GT() { return getTokens(DynamicRebecaCompleteParser.GT); }
		public List<TerminalNode> LT() { return getTokens(DynamicRebecaCompleteParser.LT); }
		public TerminalNode LT(int i) {
			return getToken(DynamicRebecaCompleteParser.LT, i);
		}
		public TerminalNode GT(int i) {
			return getToken(DynamicRebecaCompleteParser.GT, i);
		}
		public ShiftOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shiftOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterShiftOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitShiftOp(this);
		}
	}

	public final ShiftOpContext shiftOp() throws RecognitionException {
		ShiftOpContext _localctx = new ShiftOpContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_shiftOp);
		try {
			setState(782);
			switch (_input.LA(1)) {
			case LT:
				enterOuterAlt(_localctx, 1);
				{
				setState(776); match(LT);
				setState(777); match(LT);
				((ShiftOpContext)_localctx).so =  "<<";
				}
				break;
			case GT:
				enterOuterAlt(_localctx, 2);
				{
				setState(779); match(GT);
				setState(780); match(GT);
				((ShiftOpContext)_localctx).so =  ">>";
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AdditiveExpressionContext extends ParserRuleContext {
		public Expression e;
		public MultiplicativeExpressionContext e1;
		public Token PLUS;
		public Token SUB;
		public MultiplicativeExpressionContext e2;
		public List<TerminalNode> SUB() { return getTokens(DynamicRebecaCompleteParser.SUB); }
		public List<TerminalNode> PLUS() { return getTokens(DynamicRebecaCompleteParser.PLUS); }
		public TerminalNode SUB(int i) {
			return getToken(DynamicRebecaCompleteParser.SUB, i);
		}
		public MultiplicativeExpressionContext multiplicativeExpression(int i) {
			return getRuleContext(MultiplicativeExpressionContext.class,i);
		}
		public TerminalNode PLUS(int i) {
			return getToken(DynamicRebecaCompleteParser.PLUS, i);
		}
		public List<MultiplicativeExpressionContext> multiplicativeExpression() {
			return getRuleContexts(MultiplicativeExpressionContext.class);
		}
		public AdditiveExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additiveExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterAdditiveExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitAdditiveExpression(this);
		}
	}

	public final AdditiveExpressionContext additiveExpression() throws RecognitionException {
		AdditiveExpressionContext _localctx = new AdditiveExpressionContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_additiveExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(784); ((AdditiveExpressionContext)_localctx).e1 = multiplicativeExpression();
			((AdditiveExpressionContext)_localctx).e =  ((AdditiveExpressionContext)_localctx).e1.e;
			setState(798);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,60,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					((AdditiveExpressionContext)_localctx).e =  new BinaryExpression(); ((BinaryExpression)_localctx.e).setLeft(((AdditiveExpressionContext)_localctx).e1.e); _localctx.e.setLineNumber(((AdditiveExpressionContext)_localctx).e1.e.getLineNumber()); _localctx.e.setCharacter(((AdditiveExpressionContext)_localctx).e1.e.getCharacter());
					setState(791);
					switch (_input.LA(1)) {
					case PLUS:
						{
						setState(787); ((AdditiveExpressionContext)_localctx).PLUS = match(PLUS);
						((BinaryExpression)_localctx.e).setOperator((((AdditiveExpressionContext)_localctx).PLUS!=null?((AdditiveExpressionContext)_localctx).PLUS.getText():null));
						}
						break;
					case SUB:
						{
						setState(789); ((AdditiveExpressionContext)_localctx).SUB = match(SUB);
						((BinaryExpression)_localctx.e).setOperator((((AdditiveExpressionContext)_localctx).SUB!=null?((AdditiveExpressionContext)_localctx).SUB.getText():null));
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(793); ((AdditiveExpressionContext)_localctx).e2 = multiplicativeExpression();
					((BinaryExpression)_localctx.e).setRight(((AdditiveExpressionContext)_localctx).e2.e);
					}
					} 
				}
				setState(800);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,60,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplicativeExpressionContext extends ParserRuleContext {
		public Expression e;
		public UnaryExpressionContext e1;
		public Token STAR;
		public Token SLASH;
		public Token PERCENT;
		public UnaryExpressionContext e2;
		public List<TerminalNode> SLASH() { return getTokens(DynamicRebecaCompleteParser.SLASH); }
		public List<TerminalNode> STAR() { return getTokens(DynamicRebecaCompleteParser.STAR); }
		public TerminalNode PERCENT(int i) {
			return getToken(DynamicRebecaCompleteParser.PERCENT, i);
		}
		public List<TerminalNode> PERCENT() { return getTokens(DynamicRebecaCompleteParser.PERCENT); }
		public List<UnaryExpressionContext> unaryExpression() {
			return getRuleContexts(UnaryExpressionContext.class);
		}
		public TerminalNode SLASH(int i) {
			return getToken(DynamicRebecaCompleteParser.SLASH, i);
		}
		public UnaryExpressionContext unaryExpression(int i) {
			return getRuleContext(UnaryExpressionContext.class,i);
		}
		public TerminalNode STAR(int i) {
			return getToken(DynamicRebecaCompleteParser.STAR, i);
		}
		public MultiplicativeExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicativeExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterMultiplicativeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitMultiplicativeExpression(this);
		}
	}

	public final MultiplicativeExpressionContext multiplicativeExpression() throws RecognitionException {
		MultiplicativeExpressionContext _localctx = new MultiplicativeExpressionContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_multiplicativeExpression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(801); ((MultiplicativeExpressionContext)_localctx).e1 = unaryExpression();
			((MultiplicativeExpressionContext)_localctx).e =  ((MultiplicativeExpressionContext)_localctx).e1.e;
			setState(817);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,62,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					((MultiplicativeExpressionContext)_localctx).e =  new BinaryExpression(); ((BinaryExpression)_localctx.e).setLeft(((MultiplicativeExpressionContext)_localctx).e1.e); _localctx.e.setLineNumber(((MultiplicativeExpressionContext)_localctx).e1.e.getLineNumber()); _localctx.e.setCharacter(((MultiplicativeExpressionContext)_localctx).e1.e.getCharacter());
					setState(810);
					switch (_input.LA(1)) {
					case STAR:
						{
						setState(804); ((MultiplicativeExpressionContext)_localctx).STAR = match(STAR);
						((BinaryExpression)_localctx.e).setOperator((((MultiplicativeExpressionContext)_localctx).STAR!=null?((MultiplicativeExpressionContext)_localctx).STAR.getText():null));
						}
						break;
					case SLASH:
						{
						setState(806); ((MultiplicativeExpressionContext)_localctx).SLASH = match(SLASH);
						((BinaryExpression)_localctx.e).setOperator((((MultiplicativeExpressionContext)_localctx).SLASH!=null?((MultiplicativeExpressionContext)_localctx).SLASH.getText():null));
						}
						break;
					case PERCENT:
						{
						setState(808); ((MultiplicativeExpressionContext)_localctx).PERCENT = match(PERCENT);
						((BinaryExpression)_localctx.e).setOperator((((MultiplicativeExpressionContext)_localctx).PERCENT!=null?((MultiplicativeExpressionContext)_localctx).PERCENT.getText():null));
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(812); ((MultiplicativeExpressionContext)_localctx).e2 = unaryExpression();
					((BinaryExpression)_localctx.e).setRight(((MultiplicativeExpressionContext)_localctx).e2.e);
					}
					} 
				}
				setState(819);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,62,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryExpressionContext extends ParserRuleContext {
		public Expression e;
		public UnaryExpressionContext ep;
		public Token SUB;
		public UnaryExpressionContext es;
		public Token PLUSPLUS;
		public UnaryExpressionContext epp;
		public Token SUBSUB;
		public UnaryExpressionContext ess;
		public UnaryExpressionNotPlusMinusContext eu;
		public TerminalNode SUB() { return getToken(DynamicRebecaCompleteParser.SUB, 0); }
		public TerminalNode PLUS() { return getToken(DynamicRebecaCompleteParser.PLUS, 0); }
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public UnaryExpressionNotPlusMinusContext unaryExpressionNotPlusMinus() {
			return getRuleContext(UnaryExpressionNotPlusMinusContext.class,0);
		}
		public TerminalNode SUBSUB() { return getToken(DynamicRebecaCompleteParser.SUBSUB, 0); }
		public TerminalNode PLUSPLUS() { return getToken(DynamicRebecaCompleteParser.PLUSPLUS, 0); }
		public UnaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterUnaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitUnaryExpression(this);
		}
	}

	public final UnaryExpressionContext unaryExpression() throws RecognitionException {
		UnaryExpressionContext _localctx = new UnaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_unaryExpression);
		try {
			setState(839);
			switch (_input.LA(1)) {
			case PLUS:
				enterOuterAlt(_localctx, 1);
				{
				setState(820); match(PLUS);
				setState(821); ((UnaryExpressionContext)_localctx).ep = unaryExpression();
				((UnaryExpressionContext)_localctx).e =  ((UnaryExpressionContext)_localctx).ep.e;
				}
				break;
			case SUB:
				enterOuterAlt(_localctx, 2);
				{
				setState(824); ((UnaryExpressionContext)_localctx).SUB = match(SUB);
				setState(825); ((UnaryExpressionContext)_localctx).es = unaryExpression();
				((UnaryExpressionContext)_localctx).e =  new UnaryExpression(); ((UnaryExpression)_localctx.e).setOperator((((UnaryExpressionContext)_localctx).SUB!=null?((UnaryExpressionContext)_localctx).SUB.getText():null)); ((UnaryExpression)_localctx.e).setExpression(((UnaryExpressionContext)_localctx).es.e); ((UnaryExpression)_localctx.e).setLineNumber(((UnaryExpressionContext)_localctx).es.e.getLineNumber()); _localctx.e.setCharacter(((UnaryExpressionContext)_localctx).es.e.getCharacter());
				}
				break;
			case PLUSPLUS:
				enterOuterAlt(_localctx, 3);
				{
				setState(828); ((UnaryExpressionContext)_localctx).PLUSPLUS = match(PLUSPLUS);
				setState(829); ((UnaryExpressionContext)_localctx).epp = unaryExpression();
				((UnaryExpressionContext)_localctx).e =  new UnaryExpression(); ((UnaryExpression)_localctx.e).setOperator((((UnaryExpressionContext)_localctx).PLUSPLUS!=null?((UnaryExpressionContext)_localctx).PLUSPLUS.getText():null)); ((UnaryExpression)_localctx.e).setExpression(((UnaryExpressionContext)_localctx).epp.e); _localctx.e.setLineNumber(((UnaryExpressionContext)_localctx).epp.e.getLineNumber()); _localctx.e.setCharacter(((UnaryExpressionContext)_localctx).epp.e.getCharacter());
				}
				break;
			case SUBSUB:
				enterOuterAlt(_localctx, 4);
				{
				setState(832); ((UnaryExpressionContext)_localctx).SUBSUB = match(SUBSUB);
				setState(833); ((UnaryExpressionContext)_localctx).ess = unaryExpression();
				((UnaryExpressionContext)_localctx).e =  new UnaryExpression(); ((UnaryExpression)_localctx.e).setOperator((((UnaryExpressionContext)_localctx).SUBSUB!=null?((UnaryExpressionContext)_localctx).SUBSUB.getText():null)); ((UnaryExpression)_localctx.e).setExpression(((UnaryExpressionContext)_localctx).ess.e); _localctx.e.setLineNumber(((UnaryExpressionContext)_localctx).ess.e.getLineNumber()); _localctx.e.setCharacter(((UnaryExpressionContext)_localctx).ess.e.getCharacter());
				}
				break;
			case LIST:
			case NEW:
			case INTLITERAL:
			case FLOATLITERAL:
			case DOUBLELITERAL:
			case CHARLITERAL:
			case STRINGLITERAL:
			case TRUE:
			case FALSE:
			case NULL:
			case LPAREN:
			case BANG:
			case TILDA:
			case QUES:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 5);
				{
				setState(836); ((UnaryExpressionContext)_localctx).eu = unaryExpressionNotPlusMinus();
				((UnaryExpressionContext)_localctx).e =  ((UnaryExpressionContext)_localctx).eu.e;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CastExpressionContext extends ParserRuleContext {
		public Expression e;
		public TypeContext t;
		public ExpressionContext e1;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(DynamicRebecaCompleteParser.RPAREN, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode LPAREN() { return getToken(DynamicRebecaCompleteParser.LPAREN, 0); }
		public CastExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_castExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterCastExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitCastExpression(this);
		}
	}

	public final CastExpressionContext castExpression() throws RecognitionException {
		CastExpressionContext _localctx = new CastExpressionContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_castExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(841); match(LPAREN);
			setState(842); ((CastExpressionContext)_localctx).t = type();
			setState(843); match(RPAREN);
			setState(844); ((CastExpressionContext)_localctx).e1 = expression();
			((CastExpressionContext)_localctx).e =  new CastExpression();
			    	((CastExpression)_localctx.e).setExpression(((CastExpressionContext)_localctx).e1.e); ((CastExpression)_localctx.e).setType(((CastExpressionContext)_localctx).t.t);
					_localctx.e.setLineNumber(((CastExpressionContext)_localctx).e1.e.getLineNumber()); _localctx.e.setCharacter(((CastExpressionContext)_localctx).e1.e.getCharacter());
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryContext extends ParserRuleContext {
		public TermPrimary tp;
		public Token id1;
		public Token lp;
		public ExpressionListContext el;
		public ExpressionContext e2;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public List<TerminalNode> LBRACKET() { return getTokens(DynamicRebecaCompleteParser.LBRACKET); }
		public TerminalNode RPAREN() { return getToken(DynamicRebecaCompleteParser.RPAREN, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public List<TerminalNode> RBRACKET() { return getTokens(DynamicRebecaCompleteParser.RBRACKET); }
		public TerminalNode IDENTIFIER() { return getToken(DynamicRebecaCompleteParser.IDENTIFIER, 0); }
		public TerminalNode RBRACKET(int i) {
			return getToken(DynamicRebecaCompleteParser.RBRACKET, i);
		}
		public TerminalNode LPAREN() { return getToken(DynamicRebecaCompleteParser.LPAREN, 0); }
		public TerminalNode LBRACKET(int i) {
			return getToken(DynamicRebecaCompleteParser.LBRACKET, i);
		}
		public PrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterPrimary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitPrimary(this);
		}
	}

	public final PrimaryContext primary() throws RecognitionException {
		PrimaryContext _localctx = new PrimaryContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_primary);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(847); ((PrimaryContext)_localctx).id1 = match(IDENTIFIER);
			((PrimaryContext)_localctx).tp =  new TermPrimary(); _localctx.tp.setName((((PrimaryContext)_localctx).id1!=null?((PrimaryContext)_localctx).id1.getText():null));
								  _localctx.tp.setLineNumber(((PrimaryContext)_localctx).id1.getLine()); _localctx.tp.setCharacter(((PrimaryContext)_localctx).id1.getCharPositionInLine());
			setState(857);
			_la = _input.LA(1);
			if (_la==LPAREN) {
				{
				setState(849); ((PrimaryContext)_localctx).lp = match(LPAREN);
				ParentSuffixPrimary psp = new ParentSuffixPrimary(); 
				    	 psp.setLineNumber(((PrimaryContext)_localctx).lp.getLine()); psp.setCharacter(((PrimaryContext)_localctx).lp.getCharPositionInLine());
				    	 _localctx.tp.setParentSuffixPrimary(psp);
				setState(854);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LIST) | (1L << NEW) | (1L << INTLITERAL) | (1L << FLOATLITERAL) | (1L << DOUBLELITERAL) | (1L << CHARLITERAL) | (1L << STRINGLITERAL) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << LPAREN) | (1L << BANG) | (1L << TILDA) | (1L << QUES) | (1L << PLUSPLUS) | (1L << SUBSUB) | (1L << PLUS) | (1L << SUB))) != 0) || _la==IDENTIFIER) {
					{
					setState(851); ((PrimaryContext)_localctx).el = expressionList();
					_localctx.tp.getParentSuffixPrimary().getArguments().addAll(((PrimaryContext)_localctx).el.el);
					}
				}

				setState(856); match(RPAREN);
				}
			}

			setState(866);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LBRACKET) {
				{
				{
				setState(859); match(LBRACKET);
				setState(860); ((PrimaryContext)_localctx).e2 = expression();
				setState(861); match(RBRACKET);
				_localctx.tp.getIndices().add(((PrimaryContext)_localctx).e2.e);
				}
				}
				setState(868);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionListContext extends ParserRuleContext {
		public List<Expression> el;
		public ExpressionContext e;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(DynamicRebecaCompleteParser.COMMA, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(DynamicRebecaCompleteParser.COMMA); }
		public ExpressionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).enterExpressionList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof DynamicRebecaCompleteListener ) ((DynamicRebecaCompleteListener)listener).exitExpressionList(this);
		}
	}

	public final ExpressionListContext expressionList() throws RecognitionException {
		ExpressionListContext _localctx = new ExpressionListContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_expressionList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((ExpressionListContext)_localctx).el =  new LinkedList<Expression>();
			setState(870); ((ExpressionListContext)_localctx).e = expression();
			_localctx.el.add(((ExpressionListContext)_localctx).e.e);
			setState(878);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(872); match(COMMA);
				setState(873); ((ExpressionListContext)_localctx).e = expression();
				_localctx.el.add(((ExpressionListContext)_localctx).e.e);
				}
				}
				setState(880);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\2\3V\u0374\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4"+
		"\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20"+
		"\4\21\t\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27"+
		"\4\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36"+
		"\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4"+
		")\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62"+
		"\4\63\t\63\4\64\t\64\4\65\t\65\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2s\n"+
		"\2\3\2\5\2v\n\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2\u008d\n\2\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2\u009d\n\2\3\2\3\2\3\2\3\2\5\2\u00a3"+
		"\n\2\3\2\3\2\3\2\3\2\5\2\u00a9\n\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5"+
		"\2\u00c5\n\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5"+
		"\2\u00d5\n\2\3\3\3\3\3\3\3\3\3\3\3\3\5\3\u00dd\n\3\3\3\3\3\3\3\3\3\5\3"+
		"\u00e3\n\3\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\5\5\u00ef\n\5\3\5\3"+
		"\5\3\5\3\5\7\5\u00f5\n\5\f\5\16\5\u00f8\13\5\3\5\3\5\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\5\6\u0113\n\6\3\6\3\6\5\6\u0117\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5"+
		"\7\u0120\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u0140"+
		"\n\7\3\7\3\7\3\7\3\7\7\7\u0146\n\7\f\7\16\7\u0149\13\7\3\7\3\7\3\7\3\7"+
		"\5\7\u014f\n\7\5\7\u0151\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\5"+
		"\t\u015d\n\t\3\t\3\t\3\t\7\t\u0162\n\t\f\t\16\t\u0165\13\t\3\t\3\t\3\t"+
		"\3\n\3\n\3\13\3\13\3\f\3\f\3\f\3\f\7\f\u0172\n\f\f\f\16\f\u0175\13\f\3"+
		"\f\3\f\3\f\3\f\3\f\6\f\u017c\n\f\r\f\16\f\u017d\3\f\3\f\3\f\3\r\3\r\3"+
		"\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u018d\n\16\3\16\3\16\3\16"+
		"\3\16\3\16\3\16\5\16\u0195\n\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\7\17\u01a0\n\17\f\17\16\17\u01a3\13\17\3\20\3\20\3\20\3\20\3\20"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\7\21\u01b1\n\21\f\21\16\21\u01b4\13"+
		"\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u01bd\n\22\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\5\23\u01c5\n\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\7\24\u01cf\n\24\f\24\16\24\u01d2\13\24\5\24\u01d4\n\24\3\24\3\24\3\25"+
		"\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\7\25\u01e7\n\25\f\25\16\25\u01ea\13\25\3\25\5\25\u01ed\n\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\7\25\u01f5\n\25\f\25\16\25\u01f8\13\25\3\25\5\25"+
		"\u01fb\n\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\7\25\u0206\n"+
		"\25\f\25\16\25\u0209\13\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\32\3\32"+
		"\3\32\3\32\3\32\5\32\u0225\n\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\7\33\u0230\n\33\f\33\16\33\u0233\13\33\3\34\3\34\3\34\3\34\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\7\35\u023f\n\35\f\35\16\35\u0242\13\35\3\35"+
		"\3\35\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u024c\n\36\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\5\37\u0256\n\37\3\37\3\37\3\37\3\37\7\37\u025c\n"+
		"\37\f\37\16\37\u025f\13\37\7\37\u0261\n\37\f\37\16\37\u0264\13\37\3 \3"+
		" \3 \3!\3!\3!\3!\3!\6!\u026e\n!\r!\16!\u026f\3\"\3\"\3\"\3\"\3\"\3\"\5"+
		"\"\u0278\n\"\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#\3#"+
		"\3#\3#\3#\3#\3#\5#\u0292\n#\3$\3$\3$\3$\3$\3$\3$\3$\5$\u029c\n$\3%\3%"+
		"\3%\3%\3%\3%\7%\u02a4\n%\f%\16%\u02a7\13%\3&\3&\3&\3&\3&\3&\7&\u02af\n"+
		"&\f&\16&\u02b2\13&\3\'\3\'\3\'\3\'\3\'\3\'\7\'\u02ba\n\'\f\'\16\'\u02bd"+
		"\13\'\3(\3(\3(\3(\3(\3(\7(\u02c5\n(\f(\16(\u02c8\13(\3)\3)\3)\3)\3)\3"+
		")\7)\u02d0\n)\f)\16)\u02d3\13)\3*\3*\3*\3*\3*\3*\3*\5*\u02dc\n*\3*\3*"+
		"\3*\7*\u02e1\n*\f*\16*\u02e4\13*\3+\3+\3+\3,\3,\3,\3,\3,\3,\7,\u02ef\n"+
		",\f,\16,\u02f2\13,\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\5-\u02fe\n-\3.\3.\3."+
		"\3.\3.\3.\7.\u0306\n.\f.\16.\u0309\13.\3/\3/\3/\3/\3/\3/\5/\u0311\n/\3"+
		"\60\3\60\3\60\3\60\3\60\3\60\3\60\5\60\u031a\n\60\3\60\3\60\3\60\7\60"+
		"\u031f\n\60\f\60\16\60\u0322\13\60\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\5\61\u032d\n\61\3\61\3\61\3\61\7\61\u0332\n\61\f\61\16\61\u0335"+
		"\13\61\3\62\3\62\3\62\3\62\3\62\3\62\3\62\3\62\3\62\3\62\3\62\3\62\3\62"+
		"\3\62\3\62\3\62\3\62\3\62\3\62\5\62\u034a\n\62\3\63\3\63\3\63\3\63\3\63"+
		"\3\63\3\64\3\64\3\64\3\64\3\64\3\64\3\64\5\64\u0359\n\64\3\64\5\64\u035c"+
		"\n\64\3\64\3\64\3\64\3\64\3\64\7\64\u0363\n\64\f\64\16\64\u0366\13\64"+
		"\3\65\3\65\3\65\3\65\3\65\3\65\3\65\7\65\u036f\n\65\f\65\16\65\u0372\13"+
		"\65\3\65\2\66\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64"+
		"\668:<>@BDFHJLNPRTVXZ\\^`bdfh\2\2\u03ac\2\u00d4\3\2\2\2\4\u00e2\3\2\2"+
		"\2\6\u00e4\3\2\2\2\b\u00e9\3\2\2\2\n\u0116\3\2\2\2\f\u0150\3\2\2\2\16"+
		"\u0152\3\2\2\2\20\u0158\3\2\2\2\22\u0169\3\2\2\2\24\u016b\3\2\2\2\26\u016d"+
		"\3\2\2\2\30\u0182\3\2\2\2\32\u0184\3\2\2\2\34\u0199\3\2\2\2\36\u01a4\3"+
		"\2\2\2 \u01a9\3\2\2\2\"\u01b5\3\2\2\2$\u01c4\3\2\2\2&\u01c6\3\2\2\2(\u01d7"+
		"\3\2\2\2*\u020c\3\2\2\2,\u0213\3\2\2\2.\u0216\3\2\2\2\60\u021a\3\2\2\2"+
		"\62\u021f\3\2\2\2\64\u0228\3\2\2\2\66\u0234\3\2\2\28\u0238\3\2\2\2:\u024b"+
		"\3\2\2\2<\u024d\3\2\2\2>\u0265\3\2\2\2@\u0268\3\2\2\2B\u0271\3\2\2\2D"+
		"\u0291\3\2\2\2F\u0293\3\2\2\2H\u029d\3\2\2\2J\u02a8\3\2\2\2L\u02b3\3\2"+
		"\2\2N\u02be\3\2\2\2P\u02c9\3\2\2\2R\u02d4\3\2\2\2T\u02e5\3\2\2\2V\u02e8"+
		"\3\2\2\2X\u02fd\3\2\2\2Z\u02ff\3\2\2\2\\\u0310\3\2\2\2^\u0312\3\2\2\2"+
		"`\u0323\3\2\2\2b\u0349\3\2\2\2d\u034b\3\2\2\2f\u0351\3\2\2\2h\u0367\3"+
		"\2\2\2jk\5B\"\2kl\7\63\2\2lm\b\2\1\2mu\7V\2\2nr\7)\2\2op\5h\65\2pq\b\2"+
		"\1\2qs\3\2\2\2ro\3\2\2\2rs\3\2\2\2st\3\2\2\2tv\7*\2\2un\3\2\2\2uv\3\2"+
		"\2\2vw\3\2\2\2wx\7/\2\2xy\b\2\1\2y\u00d5\3\2\2\2z{\5\36\20\2{|\b\2\1\2"+
		"|}\7/\2\2}\u00d5\3\2\2\2~\177\58\35\2\177\u0080\b\2\1\2\u0080\u00d5\3"+
		"\2\2\2\u0081\u0082\7\25\2\2\u0082\u0083\b\2\1\2\u0083\u0084\7)\2\2\u0084"+
		"\u0085\5B\"\2\u0085\u0086\7*\2\2\u0086\u0087\5\2\2\2\u0087\u008c\b\2\1"+
		"\2\u0088\u0089\7\22\2\2\u0089\u008a\5\2\2\2\u008a\u008b\b\2\1\2\u008b"+
		"\u008d\3\2\2\2\u008c\u0088\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u00d5\3\2"+
		"\2\2\u008e\u008f\7\36\2\2\u008f\u0090\b\2\1\2\u0090\u0091\7)\2\2\u0091"+
		"\u0092\5B\"\2\u0092\u0093\7*\2\2\u0093\u0094\5\2\2\2\u0094\u0095\b\2\1"+
		"\2\u0095\u00d5\3\2\2\2\u0096\u0097\7\23\2\2\u0097\u0098\b\2\1\2\u0098"+
		"\u009c\7)\2\2\u0099\u009a\5:\36\2\u009a\u009b\b\2\1\2\u009b\u009d\3\2"+
		"\2\2\u009c\u0099\3\2\2\2\u009c\u009d\3\2\2\2\u009d\u009e\3\2\2\2\u009e"+
		"\u00a2\7/\2\2\u009f\u00a0\5B\"\2\u00a0\u00a1\b\2\1\2\u00a1\u00a3\3\2\2"+
		"\2\u00a2\u009f\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\u00a8"+
		"\7/\2\2\u00a5\u00a6\5h\65\2\u00a6\u00a7\b\2\1\2\u00a7\u00a9\3\2\2\2\u00a8"+
		"\u00a5\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ab\7*"+
		"\2\2\u00ab\u00ac\5\2\2\2\u00ac\u00ad\b\2\1\2\u00ad\u00d5\3\2\2\2\u00ae"+
		"\u00af\7\23\2\2\u00af\u00b0\b\2\1\2\u00b0\u00b1\7)\2\2\u00b1\u00b2\5\4"+
		"\3\2\u00b2\u00b3\7V\2\2\u00b3\u00b4\7\66\2\2\u00b4\u00b5\5B\"\2\u00b5"+
		"\u00b6\7*\2\2\u00b6\u00b7\5\2\2\2\u00b7\u00b8\b\2\1\2\u00b8\u00d5\3\2"+
		"\2\2\u00b9\u00ba\7\35\2\2\u00ba\u00bb\7)\2\2\u00bb\u00bc\5B\"\2\u00bc"+
		"\u00bd\7*\2\2\u00bd\u00be\7+\2\2\u00be\u00bf\5<\37\2\u00bf\u00c0\7,\2"+
		"\2\u00c0\u00c1\b\2\1\2\u00c1\u00d5\3\2\2\2\u00c2\u00c4\7\34\2\2\u00c3"+
		"\u00c5\5B\"\2\u00c4\u00c3\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\u00c6\3\2"+
		"\2\2\u00c6\u00c7\7/\2\2\u00c7\u00d5\b\2\1\2\u00c8\u00c9\7\r\2\2\u00c9"+
		"\u00ca\7/\2\2\u00ca\u00d5\b\2\1\2\u00cb\u00cc\7\20\2\2\u00cc\u00cd\7/"+
		"\2\2\u00cd\u00d5\b\2\1\2\u00ce\u00cf\7/\2\2\u00cf\u00d5\b\2\1\2\u00d0"+
		"\u00d1\5> \2\u00d1\u00d2\b\2\1\2\u00d2\u00d3\7/\2\2\u00d3\u00d5\3\2\2"+
		"\2\u00d4j\3\2\2\2\u00d4z\3\2\2\2\u00d4~\3\2\2\2\u00d4\u0081\3\2\2\2\u00d4"+
		"\u008e\3\2\2\2\u00d4\u0096\3\2\2\2\u00d4\u00ae\3\2\2\2\u00d4\u00b9\3\2"+
		"\2\2\u00d4\u00c2\3\2\2\2\u00d4\u00c8\3\2\2\2\u00d4\u00cb\3\2\2\2\u00d4"+
		"\u00ce\3\2\2\2\u00d4\u00d0\3\2\2\2\u00d5\3\3\2\2\2\u00d6\u00d7\b\3\1\2"+
		"\u00d7\u00d8\7V\2\2\u00d8\u00dc\b\3\1\2\u00d9\u00da\5@!\2\u00da\u00db"+
		"\b\3\1\2\u00db\u00dd\3\2\2\2\u00dc\u00d9\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd"+
		"\u00e3\3\2\2\2\u00de\u00df\b\3\1\2\u00df\u00e0\5\6\4\2\u00e0\u00e1\b\3"+
		"\1\2\u00e1\u00e3\3\2\2\2\u00e2\u00d6\3\2\2\2\u00e2\u00de\3\2\2\2\u00e3"+
		"\5\3\2\2\2\u00e4\u00e5\b\4\1\2\u00e5\u00e6\7\3\2\2\u00e6\u00e7\5\b\5\2"+
		"\u00e7\u00e8\b\4\1\2\u00e8\7\3\2\2\2\u00e9\u00ea\b\5\1\2\u00ea\u00ee\7"+
		"Q\2\2\u00eb\u00ec\5\4\3\2\u00ec\u00ed\b\5\1\2\u00ed\u00ef\3\2\2\2\u00ee"+
		"\u00eb\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00f6\3\2\2\2\u00f0\u00f1\7\60"+
		"\2\2\u00f1\u00f2\5\4\3\2\u00f2\u00f3\b\5\1\2\u00f3\u00f5\3\2\2\2\u00f4"+
		"\u00f0\3\2\2\2\u00f5\u00f8\3\2\2\2\u00f6\u00f4\3\2\2\2\u00f6\u00f7\3\2"+
		"\2\2\u00f7\u00f9\3\2\2\2\u00f8\u00f6\3\2\2\2\u00f9\u00fa\7P\2\2\u00fa"+
		"\t\3\2\2\2\u00fb\u00fc\7\5\2\2\u00fc\u0117\b\6\1\2\u00fd\u00fe\7\6\2\2"+
		"\u00fe\u0117\b\6\1\2\u00ff\u0100\7\7\2\2\u0100\u0117\b\6\1\2\u0101\u0102"+
		"\7\b\2\2\u0102\u0117\b\6\1\2\u0103\u0104\7\t\2\2\u0104\u0117\b\6\1\2\u0105"+
		"\u0106\7&\2\2\u0106\u0117\b\6\1\2\u0107\u0108\7\'\2\2\u0108\u0117\b\6"+
		"\1\2\u0109\u010a\7(\2\2\u010a\u0117\b\6\1\2\u010b\u010c\b\6\1\2\u010c"+
		"\u010d\5\6\4\2\u010d\u010e\b\6\1\2\u010e\u0112\7)\2\2\u010f\u0110\5h\65"+
		"\2\u0110\u0111\b\6\1\2\u0111\u0113\3\2\2\2\u0112\u010f\3\2\2\2\u0112\u0113"+
		"\3\2\2\2\u0113\u0114\3\2\2\2\u0114\u0115\7*\2\2\u0115\u0117\3\2\2\2\u0116"+
		"\u00fb\3\2\2\2\u0116\u00fd\3\2\2\2\u0116\u00ff\3\2\2\2\u0116\u0101\3\2"+
		"\2\2\u0116\u0103\3\2\2\2\u0116\u0105\3\2\2\2\u0116\u0107\3\2\2\2\u0116"+
		"\u0109\3\2\2\2\u0116\u010b\3\2\2\2\u0117\13\3\2\2\2\u0118\u0119\7\4\2"+
		"\2\u0119\u011a\5\4\3\2\u011a\u011b\b\7\1\2\u011b\u011f\7)\2\2\u011c\u011d"+
		"\5h\65\2\u011d\u011e\b\7\1\2\u011e\u0120\3\2\2\2\u011f\u011c\3\2\2\2\u011f"+
		"\u0120\3\2\2\2\u0120\u0121\3\2\2\2\u0121\u0122\7*\2\2\u0122\u0151\3\2"+
		"\2\2\u0123\u0124\7\64\2\2\u0124\u0125\5b\62\2\u0125\u0126\b\7\1\2\u0126"+
		"\u0151\3\2\2\2\u0127\u0128\7\63\2\2\u0128\u0129\5b\62\2\u0129\u012a\b"+
		"\7\1\2\u012a\u0151\3\2\2\2\u012b\u012c\5d\63\2\u012c\u012d\b\7\1\2\u012d"+
		"\u0140\3\2\2\2\u012e\u012f\7)\2\2\u012f\u0130\5B\"\2\u0130\u0131\b\7\1"+
		"\2\u0131\u0132\7*\2\2\u0132\u0140\3\2\2\2\u0133\u0134\5f\64\2\u0134\u0135"+
		"\b\7\1\2\u0135\u0140\3\2\2\2\u0136\u0137\5\n\6\2\u0137\u0138\b\7\1\2\u0138"+
		"\u0140\3\2\2\2\u0139\u013a\7\65\2\2\u013a\u013b\7)\2\2\u013b\u013c\5h"+
		"\65\2\u013c\u013d\7*\2\2\u013d\u013e\b\7\1\2\u013e\u0140\3\2\2\2\u013f"+
		"\u012b\3\2\2\2\u013f\u012e\3\2\2\2\u013f\u0133\3\2\2\2\u013f\u0136\3\2"+
		"\2\2\u013f\u0139\3\2\2\2\u0140\u0147\3\2\2\2\u0141\u0142\7\61\2\2\u0142"+
		"\u0143\5f\64\2\u0143\u0144\b\7\1\2\u0144\u0146\3\2\2\2\u0145\u0141\3\2"+
		"\2\2\u0146\u0149\3\2\2\2\u0147\u0145\3\2\2\2\u0147\u0148\3\2\2\2\u0148"+
		"\u014e\3\2\2\2\u0149\u0147\3\2\2\2\u014a\u014b\7:\2\2\u014b\u014f\b\7"+
		"\1\2\u014c\u014d\7;\2\2\u014d\u014f\b\7\1\2\u014e\u014a\3\2\2\2\u014e"+
		"\u014c\3\2\2\2\u014e\u014f\3\2\2\2\u014f\u0151\3\2\2\2\u0150\u0118\3\2"+
		"\2\2\u0150\u0123\3\2\2\2\u0150\u0127\3\2\2\2\u0150\u013f\3\2\2\2\u0151"+
		"\r\3\2\2\2\u0152\u0153\b\b\1\2\u0153\u0154\7\"\2\2\u0154\u0155\b\b\1\2"+
		"\u0155\u0156\58\35\2\u0156\u0157\b\b\1\2\u0157\17\3\2\2\2\u0158\u015c"+
		"\b\t\1\2\u0159\u015a\5\22\n\2\u015a\u015b\b\t\1\2\u015b\u015d\3\2\2\2"+
		"\u015c\u0159\3\2\2\2\u015c\u015d\3\2\2\2\u015d\u0163\3\2\2\2\u015e\u015f"+
		"\5\24\13\2\u015f\u0160\b\t\1\2\u0160\u0162\3\2\2\2\u0161\u015e\3\2\2\2"+
		"\u0162\u0165\3\2\2\2\u0163\u0161\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0166"+
		"\3\2\2\2\u0165\u0163\3\2\2\2\u0166\u0167\5\26\f\2\u0167\u0168\b\t\1\2"+
		"\u0168\21\3\2\2\2\u0169\u016a\7\27\2\2\u016a\23\3\2\2\2\u016b\u016c\7"+
		"\26\2\2\u016c\25\3\2\2\2\u016d\u0173\b\f\1\2\u016e\u016f\5\30\r\2\u016f"+
		"\u0170\b\f\1\2\u0170\u0172\3\2\2\2\u0171\u016e\3\2\2\2\u0172\u0175\3\2"+
		"\2\2\u0173\u0171\3\2\2\2\u0173\u0174\3\2\2\2\u0174\u0176\3\2\2\2\u0175"+
		"\u0173\3\2\2\2\u0176\u017b\5\34\17\2\u0177\u0201\b\f\1\2\u0178\u0179\5"+
		"(\25\2\u0179\u017a\b\f\1\2\u017a\u017c\3\2\2\2\u017b\u0178\3\2\2\2\u017c"+
		"\u017d\3\2\2\2\u017d\u017b\3\2\2\2\u017d\u017e\3\2\2\2\u017e\u017f\3\2"+
		"\2\2\u017f\u0180\5\16\b\2\u0180\u0181\b\f\1\2\u0181\27\3\2\2\2\u0182\u0183"+
		"\7\33\2\2\u0183\31\3\2\2\2\u0184\u0185\b\16\1\2\u0185\u0186\5\4\3\2\u0186"+
		"\u0187\7V\2\2\u0187\u0188\b\16\1\2\u0188\u018c\7)\2\2\u0189\u018a\5h\65"+
		"\2\u018a\u018b\b\16\1\2\u018b\u018d\3\2\2\2\u018c\u0189\3\2\2\2\u018c"+
		"\u018d\3\2\2\2\u018d\u018e\3\2\2\2\u018e\u018f\7*\2\2\u018f\u0190\7\66"+
		"\2\2\u0190\u0194\7)\2\2\u0191\u0192\5h\65\2\u0192\u0193\b\16\1\2\u0193"+
		"\u0195\3\2\2\2\u0194\u0191\3\2\2\2\u0194\u0195\3\2\2\2\u0195\u0196\3\2"+
		"\2\2\u0196\u0197\7*\2\2\u0197\u0198\7/\2\2\u0198\33\3\2\2\2\u0199\u01a1"+
		"\b\17\1\2\u019a\u019b\7\37\2\2\u019b\u019c\5\36\20\2\u019c\u019d\b\17"+
		"\1\2\u019d\u019e\7/\2\2\u019e\u01a0\3\2\2\2\u019f\u019a\3\2\2\2\u01a0"+
		"\u01a3\3\2\2\2\u01a1\u019f\3\2\2\2\u01a1\u01a2\3\2\2\2\u01a2\35\3\2\2"+
		"\2\u01a3\u01a1\3\2\2\2\u01a4\u01a5\b\20\1\2\u01a5\u01a6\5\4\3\2\u01a6"+
		"\u01a7\5 \21\2\u01a7\u01a8\b\20\1\2\u01a8\37\3\2\2\2\u01a9\u01aa\b\21"+
		"\1\2\u01aa\u01ab\5\"\22\2\u01ab\u01b2\b\21\1\2\u01ac\u01ad\7\60\2\2\u01ad"+
		"\u01ae\5\"\22\2\u01ae\u01af\b\21\1\2\u01af\u01b1\3\2\2\2\u01b0\u01ac\3"+
		"\2\2\2\u01b1\u01b4\3\2\2\2\u01b2\u01b0\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3"+
		"!\3\2\2\2\u01b4\u01b2\3\2\2\2\u01b5\u01b6\b\22\1\2\u01b6\u01b7\7V\2\2"+
		"\u01b7\u01bc\b\22\1\2\u01b8\u01b9\7\62\2\2\u01b9\u01ba\5$\23\2\u01ba\u01bb"+
		"\b\22\1\2\u01bb\u01bd\3\2\2\2\u01bc\u01b8\3\2\2\2\u01bc\u01bd\3\2\2\2"+
		"\u01bd#\3\2\2\2\u01be\u01bf\5&\24\2\u01bf\u01c0\b\23\1\2\u01c0\u01c5\3"+
		"\2\2\2\u01c1\u01c2\5B\"\2\u01c2\u01c3\b\23\1\2\u01c3\u01c5\3\2\2\2\u01c4"+
		"\u01be\3\2\2\2\u01c4\u01c1\3\2\2\2\u01c5%\3\2\2\2\u01c6\u01c7\b\24\1\2"+
		"\u01c7\u01d3\7+\2\2\u01c8\u01c9\5$\23\2\u01c9\u01d0\b\24\1\2\u01ca\u01cb"+
		"\7\60\2\2\u01cb\u01cc\5$\23\2\u01cc\u01cd\b\24\1\2\u01cd\u01cf\3\2\2\2"+
		"\u01ce\u01ca\3\2\2\2\u01cf\u01d2\3\2\2\2\u01d0\u01ce\3\2\2\2\u01d0\u01d1"+
		"\3\2\2\2\u01d1\u01d4\3\2\2\2\u01d2\u01d0\3\2\2\2\u01d3\u01c8\3\2\2\2\u01d3"+
		"\u01d4\3\2\2\2\u01d4\u01d5\3\2\2\2\u01d5\u01d6\7,\2\2\u01d6\'\3\2\2\2"+
		"\u01d7\u01d8\b\25\1\2\u01d8\u01d9\7 \2\2\u01d9\u01da\7V\2\2\u01da\u01db"+
		"\b\25\1\2\u01db\u01dc\7)\2\2\u01dc\u01dd\7\5\2\2\u01dd\u01de\b\25\1\2"+
		"\u01de\u01df\7*\2\2\u01df\u01ec\7+\2\2\u01e0\u01e1\7$\2\2\u01e1\u01e8"+
		"\7+\2\2\u01e2\u01e3\5\36\20\2\u01e3\u01e4\b\25\1\2\u01e4\u01e5\7/\2\2"+
		"\u01e5\u01e7\3\2\2\2\u01e6\u01e2\3\2\2\2\u01e7\u01ea\3\2\2\2\u01e8\u01e6"+
		"\3\2\2\2\u01e8\u01e9\3\2\2\2\u01e9\u01eb\3\2\2\2\u01ea\u01e8\3\2\2\2\u01eb"+
		"\u01ed\7,\2\2\u01ec\u01e0\3\2\2\2\u01ec\u01ed\3\2\2\2\u01ed\u01fa\3\2"+
		"\2\2\u01ee\u01ef\7#\2\2\u01ef\u01f6\7+\2\2\u01f0\u01f1\5\36\20\2\u01f1"+
		"\u01f2\b\25\1\2\u01f2\u01f3\7/\2\2\u01f3\u01f5\3\2\2\2\u01f4\u01f0\3\2"+
		"\2\2\u01f5\u01f8\3\2\2\2\u01f6\u01f4\3\2\2\2\u01f6\u01f7\3\2\2\2\u01f7"+
		"\u01f9\3\2\2\2\u01f8\u01f6\3\2\2\2\u01f9\u01fb\7,\2\2\u01fa\u01ee\3\2"+
		"\2\2\u01fa\u01fb\3\2\2\2\u01fb\u0207\3\2\2\2\u01fc\u01fd\5,\27\2\u01fd"+
		"\u01fe\b\25\1\2\u01fe\u0206\3\2\2\2\u01ff\u0200\5.\30\2\u0200\u0201\b"+
		"\25\1\2\u0201\u0206\3\2\2\2\u0202\u0203\5\60\31\2\u0203\u0204\b\25\1\2"+
		"\u0204\u0206\3\2\2\2\u0205\u01fc\3\2\2\2\u0205\u01ff\3\2\2\2\u0205\u0202"+
		"\3\2\2\2\u0206\u0209\3\2\2\2\u0207\u0205\3\2\2\2\u0207\u0208\3\2\2\2\u0208"+
		"\u020a\3\2\2\2\u0209\u0207\3\2\2\2\u020a\u020b\7,\2\2\u020b)\3\2\2\2\u020c"+
		"\u020d\7V\2\2\u020d\u020e\b\26\1\2\u020e\u020f\5\62\32\2\u020f\u0210\b"+
		"\26\1\2\u0210\u0211\58\35\2\u0211\u0212\b\26\1\2\u0212+\3\2\2\2\u0213"+
		"\u0214\b\27\1\2\u0214\u0215\5*\26\2\u0215-\3\2\2\2\u0216\u0217\b\30\1"+
		"\2\u0217\u0218\7!\2\2\u0218\u0219\5*\26\2\u0219/\3\2\2\2\u021a\u021b\b"+
		"\31\1\2\u021b\u021c\5\4\3\2\u021c\u021d\b\31\1\2\u021d\u021e\5*\26\2\u021e"+
		"\61\3\2\2\2\u021f\u0220\b\32\1\2\u0220\u0224\7)\2\2\u0221\u0222\5\64\33"+
		"\2\u0222\u0223\b\32\1\2\u0223\u0225\3\2\2\2\u0224\u0221\3\2\2\2\u0224"+
		"\u0225\3\2\2\2\u0225\u0226\3\2\2\2\u0226\u0227\7*\2\2\u0227\63\3\2\2\2"+
		"\u0228\u0229\b\33\1\2\u0229\u022a\5\66\34\2\u022a\u0231\b\33\1\2\u022b"+
		"\u022c\7\60\2\2\u022c\u022d\5\66\34\2\u022d\u022e\b\33\1\2\u022e\u0230"+
		"\3\2\2\2\u022f\u022b\3\2\2\2\u0230\u0233\3\2\2\2\u0231\u022f\3\2\2\2\u0231"+
		"\u0232\3\2\2\2\u0232\65\3\2\2\2\u0233\u0231\3\2\2\2\u0234\u0235\5\4\3"+
		"\2\u0235\u0236\7V\2\2\u0236\u0237\b\34\1\2\u0237\67\3\2\2\2\u0238\u0239"+
		"\b\35\1\2\u0239\u023a\7+\2\2\u023a\u0240\b\35\1\2\u023b\u023c\5\2\2\2"+
		"\u023c\u023d\b\35\1\2\u023d\u023f\3\2\2\2\u023e\u023b\3\2\2\2\u023f\u0242"+
		"\3\2\2\2\u0240\u023e\3\2\2\2\u0240\u0241\3\2\2\2\u0241\u0243\3\2\2\2\u0242"+
		"\u0240\3\2\2\2\u0243\u0244\7,\2\2\u02449\3\2\2\2\u0245\u0246\5\36\20\2"+
		"\u0246\u0247\b\36\1\2\u0247\u024c\3\2\2\2\u0248\u0249\5h\65\2\u0249\u024a"+
		"\b\36\1\2\u024a\u024c\3\2\2\2\u024b\u0245\3\2\2\2\u024b\u0248\3\2\2\2"+
		"\u024c;\3\2\2\2\u024d\u0262\b\37\1\2\u024e\u0255\b\37\1\2\u024f\u0250"+
		"\7\16\2\2\u0250\u0251\5B\"\2\u0251\u0252\b\37\1\2\u0252\u0256\3\2\2\2"+
		"\u0253\u0254\7\21\2\2\u0254\u0256\b\37\1\2\u0255\u024f\3\2\2\2\u0255\u0253"+
		"\3\2\2\2\u0256\u0257\3\2\2\2\u0257\u025d\7\66\2\2\u0258\u0259\5\2\2\2"+
		"\u0259\u025a\b\37\1\2\u025a\u025c\3\2\2\2\u025b\u0258\3\2\2\2\u025c\u025f"+
		"\3\2\2\2\u025d\u025b\3\2\2\2\u025d\u025e\3\2\2\2\u025e\u0261\3\2\2\2\u025f"+
		"\u025d\3\2\2\2\u0260\u024e\3\2\2\2\u0261\u0264\3\2\2\2\u0262\u0260\3\2"+
		"\2\2\u0262\u0263\3\2\2\2\u0263=\3\2\2\2\u0264\u0262\3\2\2\2\u0265\u0266"+
		"\5B\"\2\u0266\u0267\b \1\2\u0267?\3\2\2\2\u0268\u026d\b!\1\2\u0269\u026a"+
		"\7-\2\2\u026a\u026b\7\5\2\2\u026b\u026c\7.\2\2\u026c\u026e\b!\1\2\u026d"+
		"\u0269\3\2\2\2\u026e\u026f\3\2\2\2\u026f\u026d\3\2\2\2\u026f\u0270\3\2"+
		"\2\2\u0270A\3\2\2\2\u0271\u0272\5F$\2\u0272\u0277\b\"\1\2\u0273\u0274"+
		"\5D#\2\u0274\u0275\5B\"\2\u0275\u0276\b\"\1\2\u0276\u0278\3\2\2\2\u0277"+
		"\u0273\3\2\2\2\u0277\u0278\3\2\2\2\u0278C\3\2\2\2\u0279\u027a\7\62\2\2"+
		"\u027a\u0292\b#\1\2\u027b\u027c\7E\2\2\u027c\u0292\b#\1\2\u027d\u027e"+
		"\7F\2\2\u027e\u0292\b#\1\2\u027f\u0280\7G\2\2\u0280\u0292\b#\1\2\u0281"+
		"\u0282\7H\2\2\u0282\u0292\b#\1\2\u0283\u0284\7I\2\2\u0284\u0292\b#\1\2"+
		"\u0285\u0286\7J\2\2\u0286\u0292\b#\1\2\u0287\u0288\7K\2\2\u0288\u0292"+
		"\b#\1\2\u0289\u028a\7L\2\2\u028a\u0292\b#\1\2\u028b\u028c\7M\2\2\u028c"+
		"\u0292\b#\1\2\u028d\u028e\7S\2\2\u028e\u0292\b#\1\2\u028f\u0290\7U\2\2"+
		"\u0290\u0292\b#\1\2\u0291\u0279\3\2\2\2\u0291\u027b\3\2\2\2\u0291\u027d"+
		"\3\2\2\2\u0291\u027f\3\2\2\2\u0291\u0281\3\2\2\2\u0291\u0283\3\2\2\2\u0291"+
		"\u0285\3\2\2\2\u0291\u0287\3\2\2\2\u0291\u0289\3\2\2\2\u0291\u028b\3\2"+
		"\2\2\u0291\u028d\3\2\2\2\u0291\u028f\3\2\2\2\u0292E\3\2\2\2\u0293\u0294"+
		"\5H%\2\u0294\u029b\b$\1\2\u0295\u0296\7\65\2\2\u0296\u0297\5B\"\2\u0297"+
		"\u0298\7\66\2\2\u0298\u0299\5F$\2\u0299\u029a\b$\1\2\u029a\u029c\3\2\2"+
		"\2\u029b\u0295\3\2\2\2\u029b\u029c\3\2\2\2\u029cG\3\2\2\2\u029d\u029e"+
		"\5J&\2\u029e\u02a5\b%\1\2\u029f\u02a0\79\2\2\u02a0\u02a1\5J&\2\u02a1\u02a2"+
		"\b%\1\2\u02a2\u02a4\3\2\2\2\u02a3\u029f\3\2\2\2\u02a4\u02a7\3\2\2\2\u02a5"+
		"\u02a3\3\2\2\2\u02a5\u02a6\3\2\2\2\u02a6I\3\2\2\2\u02a7\u02a5\3\2\2\2"+
		"\u02a8\u02a9\5L\'\2\u02a9\u02b0\b&\1\2\u02aa\u02ab\78\2\2\u02ab\u02ac"+
		"\5L\'\2\u02ac\u02ad\b&\1\2\u02ad\u02af\3\2\2\2\u02ae\u02aa\3\2\2\2\u02af"+
		"\u02b2\3\2\2\2\u02b0\u02ae\3\2\2\2\u02b0\u02b1\3\2\2\2\u02b1K\3\2\2\2"+
		"\u02b2\u02b0\3\2\2\2\u02b3\u02b4\5N(\2\u02b4\u02bb\b\'\1\2\u02b5\u02b6"+
		"\7B\2\2\u02b6\u02b7\5N(\2\u02b7\u02b8\b\'\1\2\u02b8\u02ba\3\2\2\2\u02b9"+
		"\u02b5\3\2\2\2\u02ba\u02bd\3\2\2\2\u02bb\u02b9\3\2\2\2\u02bb\u02bc\3\2"+
		"\2\2\u02bcM\3\2\2\2\u02bd\u02bb\3\2\2\2\u02be\u02bf\5P)\2\u02bf\u02c6"+
		"\b(\1\2\u02c0\u02c1\7C\2\2\u02c1\u02c2\5P)\2\u02c2\u02c3\b(\1\2\u02c3"+
		"\u02c5\3\2\2\2\u02c4\u02c0\3\2\2\2\u02c5\u02c8\3\2\2\2\u02c6\u02c4\3\2"+
		"\2\2\u02c6\u02c7\3\2\2\2\u02c7O\3\2\2\2\u02c8\u02c6\3\2\2\2\u02c9\u02ca"+
		"\5R*\2\u02ca\u02d1\b)\1\2\u02cb\u02cc\7A\2\2\u02cc\u02cd\5R*\2\u02cd\u02ce"+
		"\b)\1\2\u02ce\u02d0\3\2\2\2\u02cf\u02cb\3\2\2\2\u02d0\u02d3\3\2\2\2\u02d1"+
		"\u02cf\3\2\2\2\u02d1\u02d2\3\2\2\2\u02d2Q\3\2\2\2\u02d3\u02d1\3\2\2\2"+
		"\u02d4\u02d5\5T+\2\u02d5\u02e2\b*\1\2\u02d6\u02db\b*\1\2\u02d7\u02d8\7"+
		"\67\2\2\u02d8\u02dc\b*\1\2\u02d9\u02da\7O\2\2\u02da\u02dc\b*\1\2\u02db"+
		"\u02d7\3\2\2\2\u02db\u02d9\3\2\2\2\u02dc\u02dd\3\2\2\2\u02dd\u02de\5T"+
		"+\2\u02de\u02df\b*\1\2\u02df\u02e1\3\2\2\2\u02e0\u02d6\3\2\2\2\u02e1\u02e4"+
		"\3\2\2\2\u02e2\u02e0\3\2\2\2\u02e2\u02e3\3\2\2\2\u02e3S\3\2\2\2\u02e4"+
		"\u02e2\3\2\2\2\u02e5\u02e6\5V,\2\u02e6\u02e7\b+\1\2\u02e7U\3\2\2\2\u02e8"+
		"\u02e9\5Z.\2\u02e9\u02f0\b,\1\2\u02ea\u02eb\5X-\2\u02eb\u02ec\5Z.\2\u02ec"+
		"\u02ed\b,\1\2\u02ed\u02ef\3\2\2\2\u02ee\u02ea\3\2\2\2\u02ef\u02f2\3\2"+
		"\2\2\u02f0\u02ee\3\2\2\2\u02f0\u02f1\3\2\2\2\u02f1W\3\2\2\2\u02f2\u02f0"+
		"\3\2\2\2\u02f3\u02f4\7Q\2\2\u02f4\u02f5\7\62\2\2\u02f5\u02fe\b-\1\2\u02f6"+
		"\u02f7\7P\2\2\u02f7\u02f8\7\62\2\2\u02f8\u02fe\b-\1\2\u02f9\u02fa\7Q\2"+
		"\2\u02fa\u02fe\b-\1\2\u02fb\u02fc\7P\2\2\u02fc\u02fe\b-\1\2\u02fd\u02f3"+
		"\3\2\2\2\u02fd\u02f6\3\2\2\2\u02fd\u02f9\3\2\2\2\u02fd\u02fb\3\2\2\2\u02fe"+
		"Y\3\2\2\2\u02ff\u0300\5^\60\2\u0300\u0307\b.\1\2\u0301\u0302\5\\/\2\u0302"+
		"\u0303\5^\60\2\u0303\u0304\b.\1\2\u0304\u0306\3\2\2\2\u0305\u0301\3\2"+
		"\2\2\u0306\u0309\3\2\2\2\u0307\u0305\3\2\2\2\u0307\u0308\3\2\2\2\u0308"+
		"[\3\2\2\2\u0309\u0307\3\2\2\2\u030a\u030b\7Q\2\2\u030b\u030c\7Q\2\2\u030c"+
		"\u0311\b/\1\2\u030d\u030e\7P\2\2\u030e\u030f\7P\2\2\u030f\u0311\b/\1\2"+
		"\u0310\u030a\3\2\2\2\u0310\u030d\3\2\2\2\u0311]\3\2\2\2\u0312\u0313\5"+
		"`\61\2\u0313\u0320\b\60\1\2\u0314\u0319\b\60\1\2\u0315\u0316\7=\2\2\u0316"+
		"\u031a\b\60\1\2\u0317\u0318\7>\2\2\u0318\u031a\b\60\1\2\u0319\u0315\3"+
		"\2\2\2\u0319\u0317\3\2\2\2\u031a\u031b\3\2\2\2\u031b\u031c\5`\61\2\u031c"+
		"\u031d\b\60\1\2\u031d\u031f\3\2\2\2\u031e\u0314\3\2\2\2\u031f\u0322\3"+
		"\2\2\2\u0320\u031e\3\2\2\2\u0320\u0321\3\2\2\2\u0321_\3\2\2\2\u0322\u0320"+
		"\3\2\2\2\u0323\u0324\5b\62\2\u0324\u0333\b\61\1\2\u0325\u032c\b\61\1\2"+
		"\u0326\u0327\7?\2\2\u0327\u032d\b\61\1\2\u0328\u0329\7@\2\2\u0329\u032d"+
		"\b\61\1\2\u032a\u032b\7D\2\2\u032b\u032d\b\61\1\2\u032c\u0326\3\2\2\2"+
		"\u032c\u0328\3\2\2\2\u032c\u032a\3\2\2\2\u032d\u032e\3\2\2\2\u032e\u032f"+
		"\5b\62\2\u032f\u0330\b\61\1\2\u0330\u0332\3\2\2\2\u0331\u0325\3\2\2\2"+
		"\u0332\u0335\3\2\2\2\u0333\u0331\3\2\2\2\u0333\u0334\3\2\2\2\u0334a\3"+
		"\2\2\2\u0335\u0333\3\2\2\2\u0336\u0337\7=\2\2\u0337\u0338\5b\62\2\u0338"+
		"\u0339\b\62\1\2\u0339\u034a\3\2\2\2\u033a\u033b\7>\2\2\u033b\u033c\5b"+
		"\62\2\u033c\u033d\b\62\1\2\u033d\u034a\3\2\2\2\u033e\u033f\7:\2\2\u033f"+
		"\u0340\5b\62\2\u0340\u0341\b\62\1\2\u0341\u034a\3\2\2\2\u0342\u0343\7"+
		";\2\2\u0343\u0344\5b\62\2\u0344\u0345\b\62\1\2\u0345\u034a\3\2\2\2\u0346"+
		"\u0347\5\f\7\2\u0347\u0348\b\62\1\2\u0348\u034a\3\2\2\2\u0349\u0336\3"+
		"\2\2\2\u0349\u033a\3\2\2\2\u0349\u033e\3\2\2\2\u0349\u0342\3\2\2\2\u0349"+
		"\u0346\3\2\2\2\u034ac\3\2\2\2\u034b\u034c\7)\2\2\u034c\u034d\5\4\3\2\u034d"+
		"\u034e\7*\2\2\u034e\u034f\5B\"\2\u034f\u0350\b\63\1\2\u0350e\3\2\2\2\u0351"+
		"\u0352\7V\2\2\u0352\u035b\b\64\1\2\u0353\u0354\7)\2\2\u0354\u0358\b\64"+
		"\1\2\u0355\u0356\5h\65\2\u0356\u0357\b\64\1\2\u0357\u0359\3\2\2\2\u0358"+
		"\u0355\3\2\2\2\u0358\u0359\3\2\2\2\u0359\u035a\3\2\2\2\u035a\u035c\7*"+
		"\2\2\u035b\u0353\3\2\2\2\u035b\u035c\3\2\2\2\u035c\u0364\3\2\2\2\u035d"+
		"\u035e\7-\2\2\u035e\u035f\5B\"\2\u035f\u0360\7.\2\2\u0360\u0361\b\64\1"+
		"\2\u0361\u0363\3\2\2\2\u0362\u035d\3\2\2\2\u0363\u0366\3\2\2\2\u0364\u0362"+
		"\3\2\2\2\u0364\u0365\3\2\2\2\u0365g\3\2\2\2\u0366\u0364\3\2\2\2\u0367"+
		"\u0368\b\65\1\2\u0368\u0369\5B\"\2\u0369\u0370\b\65\1\2\u036a\u036b\7"+
		"\60\2\2\u036b\u036c\5B\"\2\u036c\u036d\b\65\1\2\u036d\u036f\3\2\2\2\u036e"+
		"\u036a\3\2\2\2\u036f\u0372\3\2\2\2\u0370\u036e\3\2\2\2\u0370\u0371\3\2"+
		"\2\2\u0371i\3\2\2\2\u0372\u0370\3\2\2\2Fru\u008c\u009c\u00a2\u00a8\u00c4"+
		"\u00d4\u00dc\u00e2\u00ee\u00f6\u0112\u0116\u011f\u013f\u0147\u014e\u0150"+
		"\u015c\u0163\u0173\u017d\u018c\u0194\u01a1\u01b2\u01bc\u01c4\u01d0\u01d3"+
		"\u01e8\u01ec\u01f6\u01fa\u0205\u0207\u0224\u0231\u0240\u024b\u0255\u025d"+
		"\u0262\u026f\u0277\u0291\u029b\u02a5\u02b0\u02bb\u02c6\u02d1\u02db\u02e2"+
		"\u02f0\u02fd\u0307\u0310\u0319\u0320\u032c\u0333\u0349\u0358\u035b\u0364"+
		"\u0370";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
	}
}