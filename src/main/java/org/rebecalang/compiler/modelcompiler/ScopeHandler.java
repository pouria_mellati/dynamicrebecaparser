package org.rebecalang.compiler.modelcompiler;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.ArrayType;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.Expression;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.FieldDeclaration;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.FormalParameterDeclaration;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.MethodDeclaration;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.OrdinaryVariableInitializer;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.PrimitiveType;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.ReactiveClassDeclaration;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.RebecaModel;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.Type;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.VariableDeclarator;
import org.rebecalang.compiler.modelcompiler.corerebeca.objectmodel.VariableInitializer;
import org.rebecalang.compiler.utils.CodeCompilationException;
import org.rebecalang.compiler.utils.CompilerFeature;
import org.rebecalang.compiler.utils.ExceptionContainer;
import org.rebecalang.compiler.utils.ExpressionResolver;
import org.rebecalang.compiler.utils.Pair;
import org.rebecalang.compiler.utils.TypesUtilities;

public class ScopeHandler {

	public final static char METHOD_SIGNATURE_SEPERATOR = '$';

	public final static String RETURN_VALUE_KEY_IN_SCOPE = "$return$";
	public final static String SCOPE_OWNER_TYPE_KEY = "$owner-type$";

	private final static HashMap<String, Type> reservedVariables;
	static {

		reservedVariables = new HashMap<String, Type>();
		reservedVariables.put("now", TypesUtilities.INT_TYPE);
		reservedVariables.put("currentMessageArrival", TypesUtilities.INT_TYPE);
		reservedVariables
				.put("currentMessageDeadline", TypesUtilities.INT_TYPE);
	}

	Stack<HashMap<String, Pair<Type, Pair<Integer, Integer>>>> variablesScope;
	HashMap<String, LinkedList<Pair<LinkedList<Type>, Type>>> methods;
	HashMap<String, Pair<Type, Object>> compileTimeEvaluatableVariables;

	Set<CompilerFeature> compilerFeature;

	private ExceptionContainer container;

	private ReactiveClassDeclaration onTopOfStackReactiveClass;

	public ScopeHandler(RebecaModel rebecaModel,
			ExpressionResolver expressionResolver,
			Set<CompilerFeature> compilerFeature, ExceptionContainer container) {
		this.compilerFeature = compilerFeature;
		this.container = container;
		this.methods = new HashMap<String, LinkedList<Pair<LinkedList<Type>, Type>>>();
		this.compileTimeEvaluatableVariables = new HashMap<String, Pair<Type, Object>>();

		variablesScope = new Stack<HashMap<String, Pair<Type, Pair<Integer, Integer>>>>();
		variablesScope
				.push(new HashMap<String, Pair<Type, Pair<Integer, Integer>>>());

	}

	public void addVariableToCompileTimeEvaluatableVariables(
			String variableName, VariableInitializer variableInitializer,
			Type type, ExpressionResolver expressionResolver,
			Integer lineNumber, Integer character, ExceptionContainer container) {
		if (variableInitializer instanceof OrdinaryVariableInitializer) {
			OrdinaryVariableInitializer ovi = (OrdinaryVariableInitializer) variableInitializer;
			Object value = expressionResolver.evaluate(ovi.getValue(), this,
					compilerFeature, container).getSecond();
			compileTimeEvaluatableVariables.put(variableName,
					new Pair<Type, Object>(type, value));
		}
	}

	public Object retrieveCompileTimeEvaluatableVariable(String variableName) {
		Pair<Type, Object> pair = compileTimeEvaluatableVariables
				.get(variableName);
		if (pair != null)
			return pair.getSecond();
		return null;
	}

	// Initializing the scope at the beginning of each method call
	public void initializeScopeToItsBaseScope(ReactiveClassDeclaration rcd,
			List<FormalParameterDeclaration> parameters, Type returnType,
			boolean isAllowedToSendMessageToOthers) {
		onTopOfStackReactiveClass = rcd;

		// Clear the scope until reach the env and global variables scope.
		while (variablesScope.size() > 1)
			variablesScope.pop();

		variablesScope
				.push(new HashMap<String, Pair<Type, Pair<Integer, Integer>>>());

		// Adding predefined variables.
		for (Entry<String, Type> entry : reservedVariables.entrySet()) {
			try {
				addVaribaleToScope(entry.getKey(), entry.getValue(), 0, 0);
			} catch (ScopeException e) {
				e.printStackTrace();
			}
		}

		// Adding two predefined known rebecs
		try {
			addVaribaleToScope("self",
					TypesUtilities.getInstance().getType(rcd.getName()), 0, 0);
			addVaribaleToScope("sender", TypesUtilities.REACTIVE_CLASS_TYPE, 0,
					0);
		} catch (CodeCompilationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Adding the known rebecs of the reactive class
		for (FieldDeclaration fd : rcd.getKnownRebecs()) {
			for (VariableDeclarator vd : fd.getVariableDeclarators())
				try {
					addVaribaleToScope(vd.getVariableName(), fd.getType(),
							vd.getLineNumber(), vd.getCharacter());
				} catch (ScopeException se) {
					se.setColumn(vd.getCharacter());
					se.setLine(vd.getLineNumber());
					this.container.addException(se);
				}
		}
		// Adding the state variables of the reactive class
		for (FieldDeclaration fd : rcd.getStatevars()) {
			for (VariableDeclarator vd : fd.getVariableDeclarators()) {
				try {
					addVaribaleToScope(vd.getVariableName(), fd.getType(),
							vd.getLineNumber(), vd.getCharacter());
				} catch (ScopeException se) {
					se.setColumn(vd.getCharacter());
					se.setLine(vd.getLineNumber());
					this.container.addException(se);
				}
			}
		}

		// Adding the parameters of the method
		for (FormalParameterDeclaration fpd : parameters) {
			try {
				addVaribaleToScope(fpd.getName(), fpd.getType(),
						fpd.getLineNumber(), fpd.getCharacter());
			} catch (ScopeException se) {
				se.setColumn(fpd.getCharacter());
				se.setLine(fpd.getLineNumber());
				this.container.addException(se);
			}
		}

		// The type of the return value of the method is the last element of the
		// base scope which identified by "return" as its name.
		try {
			addVaribaleToScope(RETURN_VALUE_KEY_IN_SCOPE, returnType, 0, 0);
		} catch (ScopeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void pushActivationRecord() {
		variablesScope
				.push(new HashMap<String, Pair<Type, Pair<Integer, Integer>>>());
	}

	public void pushActivationRecord(Type owner) {
		pushActivationRecord();
		Pair<Type, Pair<Integer, Integer>> data = new Pair<Type, Pair<Integer, Integer>>(
				owner, new Pair<Integer, Integer>(0, 0));
		variablesScope.peek().put(SCOPE_OWNER_TYPE_KEY, data);
	}

	public void popActivationRecord() {
		variablesScope.pop();
	}

	public void addVaribaleToScope(String variableName, Type type,
			int lineNumber, int column) throws ScopeException {
		Pair<Type, Pair<Integer, Integer>> info = null;
		try {
			info = retreiveVariableCompleteInfoFromScope(variableName);
		} catch (ScopeException se) {
			Pair<Type, Pair<Integer, Integer>> data = new Pair<Type, Pair<Integer, Integer>>(
					type, new Pair<Integer, Integer>(lineNumber, column));
			variablesScope.peek().put(variableName, data);
			return;
		}
		String exceptionMessage;
		if (reservedVariables.containsKey(variableName)
				|| info.getSecond().getFirst().intValue() == 0) {
			exceptionMessage = variableName + " is a reserved word";
		} else {
			exceptionMessage = "Redeclaration of \""
					+ TypesUtilities.getTypeName(type) + " " + variableName
					+ "\", it previously declared in line "
					+ info.getSecond().getFirst() + " column "
					+ info.getSecond().getSecond();
		}
		throw new ScopeException(exceptionMessage, 0, 0);
	}

	public void addMethodIntoMethodDictionary(String reactiveClassName,
			MethodDeclaration methodDecleration, Type type,
			ExceptionContainer container) {
		String name = reactiveClassName + METHOD_SIGNATURE_SEPERATOR
				+ methodDecleration.getName();
		LinkedList<Pair<LinkedList<Type>, Type>> signatures = methods.get(name);
		if (signatures == null) {
			signatures = new LinkedList<Pair<LinkedList<Type>, Type>>();
			methods.put(name, signatures);
		}

		LinkedList<Type> newMethodArguments = new LinkedList<Type>();
		for (FormalParameterDeclaration fpd : methodDecleration
				.getFormalParameters()) {
			try {
				Type fpType = fpd.getType();
				if (fpType instanceof ArrayType) {
					((ArrayType) fpType)
							.setPrimitiveType((PrimitiveType) TypesUtilities
									.getInstance().getType(
											((ArrayType) fpType)
													.getPrimitiveType()));
				} else {
					fpType = TypesUtilities.getInstance().getType(fpType);
				}
				newMethodArguments.add(fpType);
			} catch (CodeCompilationException cce) {
				cce.setColumn(fpd.getCharacter());
				cce.setLine(fpd.getLineNumber());
				container.addException(cce);
				newMethodArguments.add(TypesUtilities.UNKNOWN_TYPE);
				continue;
			}
		}
		try {
			findExactMatch(name, newMethodArguments);
			String exceptionMessage = "Duplicate method "
					+ convertMethodArgumentsToString(newMethodArguments);
			exceptionMessage += " in reactive class " + reactiveClassName;
			container.addException(new ScopeException(exceptionMessage,
					methodDecleration.getLineNumber(), methodDecleration
							.getCharacter()));
		} catch (ScopeException e) {
			signatures.add(new Pair<LinkedList<Type>, Type>(newMethodArguments,
					type));
		}
	}

	public Type retreiveMethodReturnTypeFromScope(String reactiveClassName,
			String methodName, List<Expression> parameters)
			throws ScopeException {
		String methodCanonicalName = reactiveClassName
				+ METHOD_SIGNATURE_SEPERATOR + methodName;
		LinkedList<Type> types = new LinkedList<Type>();
		for (Expression expression : parameters) {
			types.add(expression.getType());
		}
		Pair<LinkedList<Type>, Type> match;
		try {
			match = findExactMatch(methodCanonicalName, types);
		} catch (ScopeException ce) {
			match = findCastableMatch(methodCanonicalName, types);
		}
		return match.getSecond();
	}

	public Type retreiveMethodReturnTypeFromScope(String methodName,
			List<Expression> parameters) throws ScopeException {
		try {
			return retreiveMethodReturnTypeFromScope(
					onTopOfStackReactiveClass.getName(), methodName, parameters);
		} catch (ScopeException se) {
			// The requested method could be one of the predefined methods
			return retreiveMethodReturnTypeFromScope("", methodName, parameters);
		}
	}

	// Returns the specification of method in case of the names of the methods
	// and their parameters are exactly the same
	public Pair<LinkedList<Type>, Type> findExactMatch(
			String methodCanonicalName, List<Type> lookFor)
			throws ScopeException {
		return matchFinder(methodCanonicalName, lookFor, TypesUtilities
				.getInstance().getExactComparator());
	}

	// Returns the specification of method in case of the names of the methods
	// are the same and the parameters of lookFor list can be casted to one of
	// the arguments
	public Pair<LinkedList<Type>, Type> findCastableMatch(
			String methodCanonicalName, List<Type> lookFor)
			throws ScopeException {
		return matchFinder(methodCanonicalName, lookFor, TypesUtilities
				.getInstance().getCastableComparator());
	}

	private Pair<LinkedList<Type>, Type> matchFinder(
			String methodCanonicalName, List<Type> lookFor,
			Comparator<Type> comp) throws ScopeException {
		LinkedList<Pair<LinkedList<Type>, Type>> signatures = methods
				.get(methodCanonicalName);
		String[] parts = methodCanonicalName.split("\\"
				+ METHOD_SIGNATURE_SEPERATOR);

		Pair<LinkedList<Type>, Type> foundSignature = null;
		if (signatures != null) {
			for (Pair<LinkedList<Type>, Type> signature : signatures) {
				if (TypesUtilities.areTheSame(lookFor, signature.getFirst(),
						comp)) {
					if (foundSignature == null) {
						foundSignature = signature;
					} else {
						throw new ScopeException("The method "
								+ parts[1]
								+ convertMethodArgumentsToString(lookFor)
								+ " is ambiguous"
								+ (parts[0].equals("") ? "" : " for the type "
										+ parts[0]), 0, 0);
					}
				}
			}
		}
		if (foundSignature != null)
			return foundSignature;
		throw new ScopeException("The method "
				+ parts[1]
				+ convertMethodArgumentsToString(lookFor)
				+ " is undefined"
				+ ((parts[0].equals("") || (parts[0].equals(TypesUtilities
						.getTypeName(TypesUtilities.UNKNOWN_TYPE)))) ? ""
						: " for the type " + parts[0]), 0, 0);
	}

	private Pair<Type, Pair<Integer, Integer>> retreiveVariableCompleteInfoFromScope(
			String variableName) throws ScopeException {
		for (int cnt = 0; cnt < variablesScope.size(); cnt++) {
			HashMap<String, Pair<Type, Pair<Integer, Integer>>> ar = variablesScope
					.get(cnt);
			if (ar.containsKey(variableName))
				return ar.get(variableName);
		}
		throw new ScopeException("\"" + variableName + "\" undeclared", 0, 0);
	}

	public Type retreiveVariableTypeFromScope(String variableName)
			throws ScopeException {
		return retreiveVariableCompleteInfoFromScope(variableName).getFirst();
	}

	public Type retreiveVariableTypeFromCurrentActivationRecord(
			String variableName) throws ScopeException {
		if (variablesScope.peek().containsKey(variableName))
			return variablesScope.peek().get(variableName).getFirst();
		throw new ScopeException("\"" + variableName + "\" undeclared", 0, 0);
	}

	// In some cases like "continue" in a loop, the owner of one of the scopes
	// should be loop statement. otherwise the "continue" statement is reported
	// as an invalid statement.
	public boolean ownerExistsInScope(Type owner) {
		for (int cnt = 0; cnt < variablesScope.size(); cnt++) {
			HashMap<String, Pair<Type, Pair<Integer, Integer>>> ar = variablesScope
					.get(cnt);
			if (ar.containsKey(SCOPE_OWNER_TYPE_KEY))
				if (ar.get(SCOPE_OWNER_TYPE_KEY).getFirst() == owner)
					return true;
		}
		return false;
	}

	private static String convertMethodArgumentsToString(List<Type> arguments) {
		String retValue = "(";
		for (Type type : arguments) {
			retValue += TypesUtilities.getTypeName(type) + ", ";
		}
		if (retValue.length() != 1)
			retValue = retValue.substring(0, retValue.length() - 2);
		retValue += ")";
		return retValue;
	}

	public static Pair<LinkedList<Type>, Type> createSignatureSegment(
			Type returnType, Type... arguments) {
		LinkedList<Type> argumentsList = new LinkedList<Type>();
		for (Type type : arguments)
			argumentsList.add(type);
		return new Pair<LinkedList<Type>, Type>(argumentsList, returnType);
	}

	public class ScopeException extends CodeCompilationException {

		private static final long serialVersionUID = 1L;

		public ScopeException(String message, int line, int column) {
			super(message, line, column);
		}

	}
}
