HERE=$(dirname $0)
xjc -p "org.rebecalang.compiler.modelcompiler.dynamicrebeca.objectmodel" \
-d "$HERE/src/main/java" \
-b "$HERE/src/main/resources/org/rebecalang/compiler/modelcompiler/objectmodel/DynamicRebecaObjectModel-bindings.xml" \
"$HERE/src/main/resources/org/rebecalang/compiler/modelcompiler/objectmodel/DynamicRebecaObjectModel.xsd"
